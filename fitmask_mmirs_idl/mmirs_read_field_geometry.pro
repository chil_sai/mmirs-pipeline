function mmirs_read_field_geometry,filename
tab=string(9B)

f_conf=read_asc(filename,/str,pattern=tab)
;mmirs guide regions in the passed file now refer to supplementary
;filen names in the same directory defining the guide polygons
dirname=file_dirname(filename,/mark)

guide1=where(f_conf[0,*] eq 'guide1')
readcol, dirname+f_conf[1,guide1],g1y,g1x,format='F',skipline=2,delim=string(9b),count=ng1,/silent
guide2=where(f_conf[0,*] eq 'guide2')
readcol, dirname+f_conf[1,guide2],g2y,g2x,format='F',skipline=2,delim=string(9b),count=ng2,/silent
wfs1=where(f_conf[0,*] eq 'wfs1')
readcol, dirname+f_conf[1,wfs1],w1y,w1x,format='F',skipline=2,delim=string(9b),count=nw1,/silent
wfs2=where(f_conf[0,*] eq 'wfs2')
readcol, dirname+f_conf[1,wfs2],w2y,w2x,format='F',skipline=2,delim=string(9b),count=nw2,/silent

mask_reg=where(f_conf[0,*] eq 'mask')
field_geometry={$
    mask:double(strsplit(f_conf[1,mask_reg],/extr)),$
    guide1:dblarr(2,ng1),$
    guide2:dblarr(2,ng2),$
    wfs1:dblarr(2,nw1),$
    wfs2:dblarr(2,nw2)}
field_geometry.guide1[0,*]=g1x
field_geometry.guide1[1,*]=g1y
field_geometry.guide2[0,*]=g2x
field_geometry.guide2[1,*]=g2y
field_geometry.wfs1[0,*]=w1x
field_geometry.wfs1[1,*]=w1y
field_geometry.wfs2[0,*]=w2x
field_geometry.wfs2[1,*]=w2y

return,field_geometry
end
