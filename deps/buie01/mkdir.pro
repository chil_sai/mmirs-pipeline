;+
; NAME:
;  mkdir
; PURPOSE:
;  Create a new directory
; DESCRIPTION:
;
; CATEGORY:
;  File I/O
; CALLING SEQUENCE:
;  mkdir,dir
; INPUTS:
;  dir - String containing name of directory to create.
; OPTIONAL INPUT PARAMETERS:
;
; KEYWORD INPUT PARAMETERS:
;
; OUTPUTS:
;
; KEYWORD OUTPUT PARAMETERS:
;
; COMMON BLOCKS:
;
; SIDE EFFECTS:
;
; RESTRICTIONS:
;  MacOS:
;     Unable to create a directory at the top level of a disk partition
;     unless it is currently set as the working directory.  For example:
;     if your disk is named "Internal HD", the command
;        macmkdir,"Internal HD:newdirectory"
;     will not work unless your current working directory is "Internal HD"
;
; PROCEDURE:
;
; MODIFICATION HISTORY:
;  1999 Nov 14, Written by Marc W. Buie, Lowell Observatory with help from
;      Dave Osip, MIT on the Mac related code.
;-
pro mkdir,dir

   if badpar(dir,7,0,caller='MKDIR: (dir) ') then return

   case !version.os_family OF

      'unix': begin
         if exists(dir) then return
         spawn,'mkdir '+dir
      end

      'Windows': begin
         if exists(dir) then return
         spawn,'mkdir '+dir
      end

      'MacOS': begin
         cd, current=pathd
         length=strlen(dir)
         findsep=rstrpos(dir,":")

         if findsep ge 0 then begin

            if findsep+1 eq length then dir=strmid(dir,0,findsep)

            length=strlen(dir)
            findsep=rstrpos(dir,":")

            dirnew=strmid(dir,findsep+1,length)
            pathnew=strmid(dir,0,findsep)

            ;check paths
            templist=findfile(pathd+pathnew,count=tempcount)
            if tempcount eq 1 then pathd=pathd+pathnew+":"

            templist=findfile(pathnew,count=tempcount)
            if tempcount eq 1 then pathd=pathnew+":"

            dir=dirnew

         endif

         ;test if directory exists
         if exists(pathd+dir) then return

         scriptmkdir=['tell current application to copy variable "dir" to dir2make',$
                      'set temp1 to characters 1 thru -2 of dir2make',$
                      'set temp1 to temp1 as string',$
                      'tell current application to copy variable "pathd" to path2dir',$
                      'set temp2 to characters 1 thru -2 of path2dir',$
                      'set temp2 to temp2 as string',$
                      'tell application "Finder"',$
                      'select folder temp2',$
                      'make new folder at folder temp2',$
                      'select folder "untitled folder" of folder temp2',$
                      'set name of selection to temp1',$
                      'end tell']           

         do_apple_script, scriptmkdir
      end

      'vms': begin
         message,!version.os_family+' is currently unsupported.'
      end

      else: begin
         message,!version.os_family+' is an unrecognized OS type.'
      end

   endcase

end
