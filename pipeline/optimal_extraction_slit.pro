pro optimal_extractaction_slit,logfile,exptype,offset=offset,$
    slit=slit,errdata=errdata,$
    spos=spos,shwidth=shwidth,median=median,$
    extrsum=extrsum,errextrsum=errextrsum,$
    extropt=extropt,errextropt=errextropt,$
    psfprof=prof,mingoodfrac=mingoodfrac


if(n_elements(offset) ne 1) then offset=0.0 ;;; offset along the slit in pixels
if(n_elements(slit) eq 0) then slit=[0]

wdir=def_wdir(logfile)

if(n_elements(mingoodfrac) ne 1) then mingoodfrac=0.5
s_in = size(inpdata)
s_type=s_in[s_in[0]+1]
if(s_type eq 7) then begin
    spec=readfits(inpdata,h,/silent)
    nx=sxpar(h,'NAXIS1')
    ny=sxpar(h,'NAXIS2')
endif else begin
    spec=inpdata
    nx=s_in[1]
    ny=s_in[2]
endelse
extrsum=fltarr(nx)
errextrsum=fltarr(nx)
extropt=fltarr(nx)
errextropt=fltarr(nx)

if(n_elements(errdata) eq 1) then begin
    espec=readfits(errdata)
endif else if(n_elements(errdata) eq n_elements(inpdata)) then begin
    espec=errdata
endif else espec=spec*0+1.0

if(n_elements(lrange) lt 2) then lrange = 0.05*nx+lindgen(((0.95*nx) < (nx-1))-0.05*nx+1l)

if(n_elements(shwidth) ne 1) then shwidth=0.1*ny

if(n_elements(spos) ne 1) then spos=0.5*ny

prof=(keyword_set(median))? $
    median(spec[lrange,spos-shwidth:spos+shwidth],dim=1) : $
    total(spec[lrange,spos-shwidth:spos+shwidth],1,/nan)/n_elements(lrange)

prof=prof/total(prof)

cbp_tot=0L
for i=0,nx-1 do begin
  prof_my = transpose(spec[i,spos-shwidth:spos+shwidth])
  eprof_my = transpose(espec[i,spos-shwidth:spos+shwidth])
  gp=where(finite(prof_my + eprof_my) eq 1,cgp, compl=bp, ncompl=cbp)
  if((cgp gt mingoodfrac*n_elements(prof)) and (cbp gt 0)) then begin
      k_norm = total(prof[gp]*prof_my[gp]/eprof_my[gp]^2) / total(prof[gp]^2/eprof_my[gp]^2)
      prof_my[bp]=prof[bp]*k_norm
      cbp_tot=cbp_tot+cbp
      ;print,'correcting x=',i,' cbp=',cbp,' k=',k_norm
      spec[i,spos-shwidth:spos+shwidth]=transpose(prof_my)
  endif
  extrsum[i]=total(prof_my)
  errextrsum[i]=sqrt(total(eprof_my^2,/nan))
  extropt[i]=total(prof_my*prof)
  errextropt[i]=sqrt(total((eprof_my*prof)^2,/nan))
endfor
message,/inf,'Pixels corrected: '+string(cbp_tot,format='(i8)')

if(n_params() eq 2) then writefits,outfile,spec,h

end
