function compare_atm_transmission,water_vapor,data=data,$
    atm_model_tab=atm_model_tab,$
    grism=grism,filter=filter,debug=debug,n_deg=n_deg

sm_w=11 ;;; smoothing window of 11 pixels
if(n_elements(n_deg) ne 1) then begin
    n_deg=3
    if(grism eq 'K3000') then n_deg=5
endif

if(n_elements(grism) ne 1) then grism='HK'
if(n_elements(filter) ne 1) then filter='HK'


if(filter eq 'HK' and grism eq 'HK') then begin
    trans_thr=0.9
    good_wl=where(atm_model_tab[0].transmission gt trans_thr and $
                atm_model_tab[0].wl lt 2140.0,cg_wl,$
                compl=bad_wl,nc=cb_wl)
    fit_wl=where(atm_model_tab[0].wl lt 2140.0)
endif
if(filter eq 'zJ' and grism eq 'J') then begin
    trans_thr=0.97
    good_wl=where(atm_model_tab[0].transmission gt trans_thr and $
                atm_model_tab[0].wl lt 1310.0,cg_wl,$
                compl=bad_wl,nc=cb_wl)
    fit_wl=where(atm_model_tab[0].wl lt 1310.0)
endif
if(filter eq 'H' and grism eq 'H') then begin
    trans_thr=0.97
    good_wl=where(atm_model_tab[0].transmission gt trans_thr and $
                ((atm_model_tab[0].wl gt 1500.0 and $
                  atm_model_tab[0].wl lt 1770.0)),cg_wl,$
                compl=bad_wl,nc=cb_wl)
    fit_wl=where(((atm_model_tab[0].wl gt 1500.0) and $
                  (atm_model_tab[0].wl lt 1770.0)))
endif
if(filter eq 'J' and grism eq 'J') then begin
    trans_thr=0.95
    n_deg=2
    good_wl=where(atm_model_tab[0].transmission gt trans_thr and $
                ((atm_model_tab[0].wl gt 1170.0 and $
                  atm_model_tab[0].wl lt 1250.0) or $
                 (atm_model_tab[0].wl gt 1280.0 and $
                  atm_model_tab[0].wl lt 1330.0)),cg_wl,$
                compl=bad_wl,nc=cb_wl)
    fit_wl=where(((atm_model_tab[0].wl gt 1170.0) and $
                  (atm_model_tab[0].wl lt 1250.0)) or $
                 ((atm_model_tab[0].wl gt 1280.0) and $
                  (atm_model_tab[0].wl lt 1330.0)))
endif
if(filter eq 'H' and grism eq 'H3000') then begin
    trans_thr=0.97
    good_wl=where(atm_model_tab[0].transmission gt trans_thr and $
                ((atm_model_tab[0].wl gt 1500.0 and $
                  atm_model_tab[0].wl lt 1770.0)),cg_wl,$
                compl=bad_wl,nc=cb_wl)
    fit_wl=where(((atm_model_tab[0].wl gt 1500.0) and $
                  (atm_model_tab[0].wl lt 1770.0)))
endif
if(filter eq 'K' and grism eq 'K3000') then begin
    trans_thr=0.7 ;0.93
    good_wl=where(atm_model_tab[0].transmission gt trans_thr and $
                ((atm_model_tab[0].wl gt 1950.0) and $
                 (atm_model_tab[0].wl lt 2380.0)),cg_wl,$ ;;; 2350.0
                compl=bad_wl,nc=cb_wl)
    fit_wl=where((atm_model_tab[0].wl gt 1950.0) and $
                 (atm_model_tab[0].wl lt 2380.0)) ;;; 2350.0
endif



n_val=n_elements(water_vapor)
cmp_arr=dblarr(n_val)

for i=0,n_val-1 do begin
    vec1=data[good_wl]
    atm_model_int=interp_telluric_model(atm_model_tab,water_vapor=water_vapor[i])
    vec2=atm_model_int.transmission[good_wl]

    vratio=median(vec1,sm_w)/median(vec2,sm_w)
    g_vr=where(finite(vratio) eq 1,cg_vr)
    c_cont = robust_poly_fit(atm_model_tab[0].wl[good_wl[g_vr]],vratio[g_vr],n_deg)
    cont_ratio = poly(atm_model_tab[0].wl,c_cont)
;;;    c_cont = svdfit(atm_model_tab[0].wl[good_wl[g_vr]],vratio[g_vr],n_deg+1,/legendre)
;;;    cont_ratio = poly(atm_model_tab[0].wl,c_cont)

    diff_trans = (data/cont_ratio) - atm_model_int.transmission
    cmp_arr[i]=total(diff_trans[fit_wl]^2,/nan)
    if(keyword_set(debug)) then begin
        plot,atm_model_tab[0].wl,data,xs=1,ys=1,yr=[0,3.5]
        oplot,atm_model_tab[0].wl,cont_ratio,col=128
        oplot,atm_model_int.wl,atm_model_int.transmission*cont_ratio,col=80
        oplot,atm_model_tab[0].wl[good_wl],data[good_wl],psym=4,syms=0.2,col=254
        oplot,atm_model_int.wl,data/(atm_model_int.transmission*cont_ratio),col=220
        ttt=''
        read,ttt
    endif
endfor

if(n_val eq 1) then cmp_arr=total(cmp_arr)
return,cmp_arr

end
