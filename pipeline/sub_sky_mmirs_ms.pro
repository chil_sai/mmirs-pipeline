pro sub_sky_mmirs_ms,logfile,image_type,adj=adj

wdir=def_wdir(LOGFILE)
grism=def_grism(LOGFILE)
filename=(keyword_set(adj))? 'disper_table_adj.fits' : 'disper_table.fits'
wl_data=mrdfits(wdir+filename,1,/silent)

Ny=n_elements(wl_data)
gg=where(finite(wl_data.y_mask) eq 1)

ndeg=n_elements(wl_data[0].wl_sol)-1

wl_new = wl_data

mask=read_mask_mmirs_ms(wdir+'mask_mos.txt',logfile=logfile)

nslit=n_elements(mask)

inpfile=wdir+image_type+'_slits.fits'
skyfile=wdir+'sky_'+image_type+'_2d_bspl_slits.fits'
outfile=wdir+image_type+'-sky_slits.fits'

hpri=headfits(inpfile)
writefits,outfile,0,hpri

for i=0,nslit-1 do begin
    img=mrdfits(inpfile,i+1,himg,/silent)
    sky=mrdfits(skyfile,i+1,/silent)
    mwrfits,img-sky,outfile,himg,/silent
endfor


end
