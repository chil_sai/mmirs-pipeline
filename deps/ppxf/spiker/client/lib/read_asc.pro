; function for ASCII file reading
function READ_ASC,file,head,hdr=hdr,str=str,pattern=pattern,num=num,regex=regex,preserve_null=preserve_null
s=''
;if(n_elements(pattern) ne 1) then pattern=' '

if n_params() eq 1 then head=0L
n=long(file_lines(file))-long(head)
if(n_elements(num) eq 1) then n = n < long(num)
; determination of the column count
if (n le 0) then begin
  message,/inf,'input file is empty'
  return,-23456
endif
openr,u,file,/get_lun
s=''
for i=1L,head do readf,u,s
readf,u,s
if(n_elements(pattern) ne 1) then $
    nc=n_elements(strsplit(s,/extract)) $
    else $
    nc=n_elements(strsplit(s,pattern,/extract,regex=regex,preserve_null=preserve_null))
close,u

if(head gt 0) then hdr=strarr(head)
datastr=strarr(n)
;read file
openr,u,file
if(head gt 0) then readf,u,hdr
readf,u,datastr
close,u
free_lun,u
if(keyword_set(str)) then d=strarr(nc,n) else d=dblarr(nc,n)
for i=1L,n do begin
	if(keyword_set(str)) then begin
            if(n_elements(pattern) ne 1) then $
		dt=strsplit(datastr[i-1],/extract) $
                else $
		dt=strsplit(datastr[i-1],pattern,/extract,regex=regex,preserve_null=preserve_null)
	endif else begin
            if(n_elements(pattern) ne 1) then $
		dt=double(strsplit(datastr[i-1],/extract)) $
                else $
		dt=double(strsplit(datastr[i-1],pattern,/extract,regex=regex,preserve_null=preserve_null))
	endelse
        if(n_elements(dt) eq nc) then begin
	    d[*,i-1]=dt
	endif else begin 
	    print,'Error in format in line',i,':',datastr[i-1],$
                form='(a,i10,2a)'
        endelse
endfor
return,d
END
