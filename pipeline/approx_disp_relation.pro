;****************************************
;approximation of the dispersion relation

function approx_disp_relation,image,lambda,pos_lines,lines,mean_line,N_deg,w,$
    FIT=fit,intens=intens,fwhm=fwhm,crossfit=crossfit,$
    ycrd=ycrd,slitthr=slitthr,badflag=badflag,$
    instrument=instrument,bad_slitthr=bad_slitthr,$
    NdegY=NdegY,inpbadflag=inpbadflag,pos_lines_orig=pos_lines_orig,smy=smy,err_pos_lines=err_pos_lines

;image - 2D arc image (file after extraction *_s.fits)
;lambda - wavelength scale (initial approximation): float vector (Nx)
;F - coefficient of the previous wavelength approximation: 2D-float array (Ny,3)
;pos_lines - arc/OH line positions from the previous approximation
;lines - wavelength
;mean_lines - mean calculated wavelength lines along slit
;N_deg - polynomial degree for the wavelength approximation
;w - box width for the analysis
;intens - intensities of arc/OH lines

if(n_elements(NdegY) ne 1) then NdegY=5
minslitthr= 0.45 ;;; minimal acceptable slit throughput
if(n_elements(intens) ne n_elements(lines)) then intens=1.0+dblarr(n_elements(lines))
if(n_elements(fwhm) ne 1) then fwhm=7.0
sw_f=0
if keyword_set(fit) then sw_f=1
a=size(image) & Nx=a(1) & Ny=a(2) & N_line=N_elements(lines)
if(n_elements(inpbadflag) ne n_line) then inpbadflag=intarr(n_line)
disper_par=dblarr(N_deg+2,Ny)
x=findgen(Nx)
mean_line=dblarr(Ny,N_line)

image_cur=image

if(keyword_set(crossfit)) then begin
    if(n_elements(smy) ne 1) then smy=0.0
endif

err_pos_lines = pos_lines*0.0
for Y=0,Ny-1 do begin
    vector=abs(image_cur(*,Y))
    k_zz=1.0

    ;cntrd_1d,vector,double(pos_lines[y,*]),xpk_cnt,FWHM*k_zz,/silent,oversample=1
    ;gcntrd_1d,vector,double(pos_lines[y,*]),xpk_cnt,FWHM*k_zz,/silent
    cntrd_1d_simple,vector,double(pos_lines[y,*]),xpk_cnt,FWHM*k_zz*2.0,/silent,oversample=1,errxcen=errxcen
    pos_lines[y,*]=xpk_cnt
    err_pos_lines[y,*]=errxcen
endfor

if(keyword_set(crossfit)) then begin
    print,'Correcting line positions (fit along the slit)'
    pos_lines_orig=pos_lines
    if(n_elements(ycrd) ne Ny) then ycrd=dindgen(Ny)
    posarr3 = dblarr(3,long(N_line)*long(Ny))
    np3=0L
    for k=0,N_line-1 do begin
        good_pos_lines=where(finite(pos_lines[*,k]) eq 1 and pos_lines[*,k] gt 0, cgpl)
        if(cgpl gt NdegY+1) then begin ;;;; added 2010/Oct/24
            f=robust_poly_fit(ycrd[good_pos_lines],DOUBLE(pos_lines(good_pos_lines,k)),NdegY,posfit_t) ;;; NdegY order
            Yfit_v=poly(ycrd,f)
            pos_lines[*,k]=Yfit_v
            if(inpbadflag[k] eq 0) then begin
                posarr3[0,np3:np3+cgpl-1]=transpose(posfit_t)
                ;;;posarr3[0,np3:np3+cgpl-1]=transpose(double(pos_lines[good_pos_lines,k]))
                posarr3[1,np3:np3+cgpl-1]=transpose(ycrd[good_pos_lines])
                posarr3[2,np3:np3+cgpl-1]=double(lines[k])
                np3=np3+cgpl
            endif
        endif else begin
            inpbadflag[k]=1
        endelse
    endfor
endif

badflag_arr=bytarr(n_elements(inpbadflag),Ny)
for Y=0,Ny-1 do begin
    ;polynomial approximation dispersion curve
    badflag=inpbadflag ;; intarr(n_elements(lines))
    maxint=max(intens)
    if(maxint le 0.0) then maxint=1.0
    err_pos=maxint/sqrt(intens)
    bad_err=where(finite(err_pos) ne 1,cbad_err)
    if(cbad_err gt 0) then err_pos[bad_err]=1.0
    thr_sig=3.0 ;;; was 3.0
    P0=goodpoly_err(double(pos_lines(y,*)),double(lines),N_deg,thr_sig,Yfit,good_pos,good_wl,bad=badflag,errors=(err_pos_lines[y,*]))
    badflag_arr[*,Y]=badflag
    g_lines=where(badflag eq 0, cg_lines)
    if(cg_lines gt 0) then begin
        robomean,lines[g_lines]-Yfit,2,0.5,average,rms
        P=poly_fit(double(pos_lines[y,g_lines]),double(lines[g_lines]),N_deg,Yfit=Yfit,measure_errors=err_pos_lines[y,g_lines])
        robomean,lines[g_lines]-Yfit,2,0.5,average,rms
        disper_par[0:N_deg,y]=P[*]
        disper_par[N_deg+1,y]=rms
        mean_line[y,*]=0.0
        mean_line[y,*]=mean_line[y,*]+poly(pos_lines[y,*],P[0:N_deg])
    endif else begin
        print,'Y=',y,' RETURNING NAN'
        disper_par[*,y]=!values.f_nan
        mean_line[y,*]=!values.f_nan
    endelse
endfor
badflag=median(badflag_arr,dim=2)

return,disper_par
end
