function def_inst,logfile
;+
; NAME:
;	DEF_INST
; PURPOSE:
;	Determination of the instrument name, default is 'MMIRS' 
; DESCRIPTION: 
;	
; CALLING SEQUENCE:
;	Result=DEF_INST(logfile)
;
; CATEGORY:
;	CfA MMIRS pipeline
;
; INPUTS:
;	LOGFILE = the pipeline control file
;	
;
; OUTPUTS:
;	Result = string scalar name of the instrument
;		
; OPTIONAL OUTPUT:
;	no
;
; OPTIONAL INPUT KEYWORDS:
;	no	
;
; RESTRICTIONS:
;	no
;
; NOTES:
;	no
;
; PROCEDURES USED:
;	SXPAR
;
; MODIFICATION HISTORY:
;       Written by Igor Chilingarian, Observatoire de Paris, Oct 2007
;       Modified by Igor Chilingarian, CfA, Oct 2011
;-

on_error,2

log=readlog(logfile)

result=strcompress(sxpar(log,'INSTRUME',count=nmatches),/remove_all)
if(nmatches ne 1) then result='MMIRS'

return, result
end
