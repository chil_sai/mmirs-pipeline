function interp_telluric_model,atm_model_tab,airmass=airmass,$
    water_vapor=water_vapor

    n_wl_m=n_elements(atm_model_tab[0].wl)
    if((n_elements(water_vapor) eq 1) and $
       (n_elements(airmass) eq 0) and $
       (n_elements(atm_model_tab) eq 6)) then begin
        atm_model_int = atm_model_tab[0]
        water_vapor_val = min(atm_model_tab.water_vapor) > water_vapor < max(atm_model_tab.water_vapor)
        atm_model_int.water_vapor=water_vapor_val
        for i=0,n_wl_m-1 do begin
            atm_model_int.transmission[i]=$
                exp(interpol(alog(atm_model_tab.transmission[i]),$
                atm_model_tab.water_vapor,[water_vapor],/spl))
        endfor
    endif else begin
        mod_airmass=reform(atm_model_tab[[0,6,12]].airmass,3)
        mod_wv=reform(atm_model_tab[0:5].water_vapor,6)
        atm_model_int = atm_model_tab[0:5]
        airmass_val = min(mod_airmass) > airmass < max(mod_airmass)
        atm_model_int.airmass=airmass_val

        for j=0,5 do begin
            for i=0,n_wl_m-1 do begin
                atm_model_int[j].transmission[i]=$
                    exp(interpol(alog(atm_model_tab[j+[0,6,12]].transmission[i]),$
                        mod_airmass,[airmass_val],/quadratic))
            endfor
        endfor
    endelse

    return,atm_model_int
end
