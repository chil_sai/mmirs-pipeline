;;; based on the C code from here:
;;; http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html -- dead link
;;; https://wrf.ecse.rpi.edu//Research/Short_Notes/pnpoly.html

function pnpoly,vertx,verty,testx,testy

nvert=n_elements(vertx)

c=0*fix(testx)

j=nvert-1
for i=0,nvert-1 do begin
    if ( ((verty[i] gt testy) ne (verty[j] gt testy)) and $
	 (testx lt (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) ) then $
       c = 1-c;
    j=i
endfor

return,c

end
