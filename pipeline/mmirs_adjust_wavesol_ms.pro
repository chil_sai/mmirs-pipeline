pro mmirs_adjust_wavesol_ms,logfile,slit=slit,wlndeg=ndeg,ndeg_3d_x=ndeg_3d_x,ndeg_3d_y=ndeg_3d_y,suff=suff,flag_3d_a=flag_3d_a

wdir=def_wdir(logfile)
grism=def_grism(logfile,filter=filter)
if(n_elements(suff) ne 1) then suff=''

h0=headfits(wdir+'wl_data_3d.fits')
;np_a=sxpar(h0,'NP_A')

disper_all_a=mrdfits(wdir+'disper.fits',0,/silent)
pix_mask_data_a=mrdfits(wdir+'disper_table.fits',1,/silent)

data_3d_a=mrdfits(wdir+'wl_data_3d.fits',1,/silent)
np_a=n_elements(data_3d_a[0,*])
errdata_a=mrdfits(wdir+'wl_data_3d.fits',2,/silent)

if(n_elements(ndeg_3d_x) ne 1) then ndeg_3d_x=(grism eq 'HK')? 3 : 3
if(n_elements(ndeg_3d_y) ne 1) then ndeg_3d_y=(grism eq 'HK')? 3 : 3

dist_map_a=mrdfits(wdir+'dist_map.fits',1)

h1=headfits(wdir+'arc_dark.fits',ext=1)
Nx=sxpar(h1,'NAXIS1')
Ny_a=sxpar(h1,'NAXIS2')
mask_a=read_mask_mmirs_ms(wdir+'mask_mos.txt',logfile=logfile)

nxvec=dindgen(Nx)

x_img_a=nxvec # (dblarr(Ny_a)+1d)
y_img_a=(dblarr(Nx)+1d) # dindgen(Ny_a)
xmask_img_a=(dblarr(Nx)+1d) # transpose(pix_mask_data_a.x_mask)
wl_img_a=dblarr(Nx,Ny_a)
for y=0,Ny_a-1 do wl_img_a[*,y]=poly(nxvec,pix_mask_data_a[y].wl_sol)
x_img_a_r= rotate(poly_2d(x_img_a,dist_map_a.kxwrap_inv,dist_map_a.kywrap_inv,1),5)
y_img_a_r= rotate(poly_2d(y_img_a,dist_map_a.kxwrap_inv,dist_map_a.kywrap_inv,1),5)
xmask_img_a_r= rotate(poly_2d(xmask_img_a,dist_map_a.kxwrap_inv,dist_map_a.kywrap_inv,1),5)
wl_img_a_r=rotate(poly_2d(wl_img_a,dist_map_a.kxwrap_inv,dist_map_a.kywrap_inv,1),5)

y_scl_a=30.2101d
if(tag_exist(dist_map_a,'mask_y_scl')) then y_scl_a=dist_map_a.mask_y_scl
zz1=1d
zz2=1d
mmpix_a=1d/y_scl_a

n_slits_a=n_elements(mask_a)
if(n_elements(slit) eq 0) then slit=findgen(n_slits_a)
n_slits=n_slits_a

y_slits_a=get_slit_region(mask_a,Nx=Nx,Ny=Ny_a,ratio=1.0,dist_map=dist_map_a,slit_geom=slit_geom_a)

pix_mask_str={y_pix:0,x_mask:!values.f_nan,y_mask:!values.f_nan,$
    w_pix:!values.f_nan,h_pix:!values.d_nan,slit:-1,$
    wl_sol:dblarr(Ndeg+1)+!values.d_nan,wl_s_err:!values.d_nan}
pix_mask_data_a=replicate(pix_mask_str,Ny_a)

tags=tag_names(slit_geom_a[0])
for i=0,n_elements(tags)-1 do pix_mask_data_a.(i)=slit_geom_a.(i)

nx_h2d=4 ;; 10 for Bino
ny_h2d=4 ;; 10 for Bino

if(flag_3d_a eq 1) and (np_a gt 2000) then begin
    ;;; applying distortion correction to ypix
    pixcrd_2d_a=long(data_3d_a[0,*]) + Nx*long(data_3d_a[2,*])
;    data_3d_a[0,*]-=(x_img_a_r[pixcrd_2d_a]- x_img_a[pixcrd_2d_a])
;    data_3d_a[2,*]-=(y_img_a_r[pixcrd_2d_a]- y_img_a[pixcrd_2d_a])
;    data_3d_a[3,*]-=(wl_img_a_r[pixcrd_2d_a]-wl_img_a[pixcrd_2d_a])

    gdata=where(finite(total(data_3d_a,1)+errdata_a) eq 1 and errdata_a gt 0 and data_3d_a[0,*] gt 3 and data_3d_a[0,*] lt Nx-1-4,np_a)
    data_3d_a=data_3d_a[*,gdata]
    errdata_a=errdata_a[gdata]

    ;;; increasing weights of bright lines
    g_bright=where(errdata_a lt 5*1.0/500.0^0.25,cg_bright) ;; difference with Bino
    if(cg_bright gt 0) then errdata_a[g_bright]/=5.0
    ;;; weighting by slit density
    mask_xsize=mask_a[0].corners[2]-mask_a[0].corners[0]
    mask_ysize=2048.0-2*4.0 ;;mask_a[0].corners[3]-mask_a[0].corners[1]
    hist_slits=hist_2d(data_3d_a[1,*],data_3d_a[2,*],$
        min1=mask_a[0].corners[0],max1=mask_a[0].corners[2],$
        min2=4.0, max2=2048.0-4.0,$ ;; min2=mask_a[0].corners[1],max2=mask_a[0].corners[3],$
        bin1=mask_xsize/nx_h2d*1.0001,bin2=mask_ysize/ny_h2d*1.0001)
    max_hist_slits=max(hist_slits)
    for x=0,nx_h2d-1 do begin
        for y=0,ny_h2d-1 do begin
            if(hist_slits[x,y] gt 1) then begin
                h2d_cell=where((data_3d_a[1,*] ge mask_a[0].corners[0]+mask_xsize/nx_h2d*x) and $
                               (data_3d_a[1,*] lt mask_a[0].corners[0]+mask_xsize/nx_h2d*(x+1)) and $
;                               (data_3d_a[2,*] ge mask_a[0].corners[1]+mask_ysize/ny_h2d*y) and $
;                               (data_3d_a[2,*] lt mask_a[0].corners[1]+mask_ysize/ny_h2d*(y+1)),c_h2d_cell)
                               (data_3d_a[2,*] ge 180.0+mask_ysize/ny_h2d*y) and $
                               (data_3d_a[2,*] lt 180.0+mask_ysize/ny_h2d*(y+1)),c_h2d_cell)
;                if(c_h2d_cell gt 0) then errdata_a[h2d_cell]*=double(hist_slits[x,y])^1.0
            endif
        endfor
    endfor
    fit_3d=vfit_3deg_err(data_3d_a,err=errdata_a,ndeg,ndeg_3d_x,ndeg_3d_y,kx=kwl3d,/irreg,/max_deg)
    gdata_fit=where(abs(data_3d_a[3,*]-fit_3d) lt abs(kwl3d[1,0,0])*0.5, cgdata_fit)
    fit_3d_i1=vfit_3deg_err(data_3d_a[*,gdata_fit],err=errdata_a[gdata_fit],ndeg,ndeg_3d_x,ndeg_3d_y+2*0,kx=kwl3d_i1,/irreg,max_deg=0)
    if(cg_bright gt 0) then errdata_a[g_bright]*=5.0
    disper_all_a_old=disper_all_a
    disper_all_a=dblarr(Ndeg+2,Ny_a)
    for j=0,n_slits_a-1 do begin
        i=j ;;;slit[j]
        hdr_slit=headfits(wdir+'arc_slits.fits',ext=i+1)
        ymin_cur = y_slits_a[0,i]
        ymax_cur = y_slits_a[1,i]
        ys_pos=ymax_cur-ymin_cur+1

        xpos=mask_a[i].x ;;sxpar(hdr_slit,'SLITX')
        slit_tilt = (sxpar(hdr_slit,'SLITTHET')-sxpar(hdr_slit,'MASKPA')) mod 180.0
        if(slit_tilt ne 0.) then print,'j,slit_tile=',j,slit_tilt

        ;;; evaluation of the 3D coefficients to be added here
;        dx_mm = -(0*sxpar(hdr_slit,'SLITHEIG')/2.0+(dindgen(ys_pos)-(ys_pos-1d)/2d)*mmpix_a)*tan(slit_tilt/!radeg)*zz2
;        pos_xmask=xpos + dblarr(ys_pos) + dx_mm
        pos_xmask = pix_mask_data_a[ymin_cur:ymax_cur].x_mask
        ypix=ymin_cur+dindgen(ys_pos)

        wlmap_slit=reform(poly3d(nxvec # (dblarr(ys_pos)+1d),(dblarr(Nx)+1d) # pos_xmask,(dblarr(Nx)+1d) # ypix,kwl3d_i1,/irreg),Nx,ys_pos)
        wlmap_slit_r=wlmap_slit ;;mmirs_rectify_slit(wlmap_slit,dist_map_a,ymin_cur,edge=(slit_tilt eq 0.))
        dtmp=dblarr(Ndeg+1,ys_pos) ;+!values.d_nan
        for y=ymin_cur,ymax_cur do begin
            goodwl=where(finite(wlmap_slit_r[*,y-ymin_cur]) eq 1,cgoodwl)
            if(cgoodwl gt 10) then dtmp[*,y-ymin_cur]=transpose(poly_fit(double(goodwl),wlmap_slit_r[goodwl,y-ymin_cur],Ndeg))
        endfor
;if j eq 10 then stop

;        for k=0,Ndeg do begin
;            for l=0,n_elements(kwl3d_i1[0,*,0])-1 do begin
;                for m=0,n_elements(kwl3d_i1[0,0,*])-1 do begin
;                    dtmp[k,*]+=transpose(kwl3d_i1[k,l,m]*pos_xmask^l*ypix^m)
;                endfor
;            endfor
;        endfor

        disper_all_a[0:Ndeg,ymin_cur:ymax_cur]=dtmp[*,*]
        disper_all_a[Ndeg+1,ymin_cur:ymax_cur]=0.01
        pix_mask_data_a[ymin_cur:ymax_cur].wl_sol=dtmp[0:Ndeg,*]
        pix_mask_data_a[ymin_cur:ymax_cur].wl_s_err=0.01
    endfor
endif

writefits,wdir+'disper'+suff+'.fits',disper_all_a
writefits,wdir+'disper_table'+suff+'.fits',0
mwrfits,pix_mask_data_a,wdir+'disper_table'+suff+'.fits'

end
