function average_img,img_list,img_hdr=img_hdr,$
    median=median,normmed=normmed,dark=dark
    n_img=n_elements(img_list)
    img=float(mrdfits(img_list[0],1,img_hdr,/silent))
    s_img=size(img)
    if(n_elements(dark) eq n_elements(img)) then img=img-float(dark)
    if(keyword_set(median)) then begin
        img_cube=fltarr(s_img[1],s_img[2],n_img)
        img_cube[*,*,0]=img
    endif
    for i=1,n_img-1 do begin
        t_img=float(mrdfits(img_list[i],1,/silent))
        if(n_elements(dark) eq n_elements(t_img)) then t_img=t_img-float(dark)
        if(keyword_set(median)) then begin
            if(keyword_set(normmed)) then t_img=t_img/median(t_img)
            img_cube[*,*,i]=t_img
        endif else img=img+t_img
    endfor
    img=(keyword_set(median) and (n_img gt 2))? median(img_cube,dim=3) : img/float(n_img)
    return,img
end

