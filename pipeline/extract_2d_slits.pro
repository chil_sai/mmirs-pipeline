pro extract_2d_slits,logfile,image_type,unproc=unproc,nflat=nflat,out_img_type=out_img_type

if(n_params() eq 1) then image_type='obj'
wdir=def_wdir(logfile)
instrument=def_inst(logfile)
if(keyword_set(nflat)) then begin
    image_type='flatn'
    img=readfits(wdir+'flat_norm.fits',h)
endif else $
    img=(keyword_set(unproc))? rotate(transpose(mrdfits(wdir+image_type[0]+'_dark.fits',1,h)),5) : readfits(wdir+image_type[0]+'_ff.fits',h)
nx=sxpar(h,'NAXIS1')
ny=sxpar(h,'NAXIS2')

mask=read_mask_mmirs_ms(wdir+'mask_mos.txt',logfile=logfile)

dist_map=mrdfits(wdir+'dist_map.fits',1,/silent)
slit_reg=get_slit_region(mask,nx=nx,ny=ny,dist_map=dist_map,slit_trace=slit_trace)

n_slits=n_elements(mask)

if(n_elements(out_img_type) ne 1) then out_img_type=image_type[0]
f_out=wdir+out_img_type+'_slits.fits'

sxdelpar,h,'NAXIS1'
sxdelpar,h,'NAXIS2'
sxaddpar,h,'NAXIS',0

writefits,f_out,0,h

for i=0,n_slits-1 do begin
    if((i gt 0) and (n_elements(image_type) eq n_slits)) then begin
        if(image_type[i] ne image_type[i-1]) then begin
            img=(keyword_set(unproc))? rotate(transpose(mrdfits(wdir+image_type[i]+'_dark.fits',1)),5) : readfits(wdir+image_type[i]+'_ff.fits',h)
        endif
    endif
    ny_img=n_elements(img[0,*])

    print,'Processing ',i+1,'/',n_slits
    cur_y_tr=slit_trace[i].y_trace
    slit_h_cur=fix(slit_trace[i].slit_h/2.0)
    ymean=fix(cur_y_tr[nx/2-1])
    yhsize=max(ceil(abs(cur_y_tr-ymean)))+slit_h_cur
    slit_img=fltarr(nx,2*yhsize+1)+!values.f_nan
    y_off = ymean-yhsize

    for x=0,nx-1 do begin
        y_cur=cur_y_tr[x]
        ymin_cur=(y_cur-slit_h_cur) ;;; > 0
        ymax_cur=(y_cur+slit_h_cur) ;;; < (ny-1)
        dymin = (ymin_cur lt 0)?  -ymin_cur : 0.0
        dymax = (ymax_cur ge ny)? ymax_cur-ny-1 : 0.0
        slit_img_y0=fix(1e-5+y_cur-y_off-slit_h_cur+dymin)
        slit_img_y1=fix(1e-5+y_cur-y_off+slit_h_cur-dymax)
        img_y0=fix(1e-5+ymin_cur+dymin)
        img_y1=fix(1e-5+ymax_cur-dymax)

        safe_array_bounds,2*yhsize+1,ny_img,slit_img_y0,slit_img_y1,img_y0,img_y1

;;        print,'logfile:',logfile,size(slit_img)
;;        print,'slit_img',x,fix(y_cur-y_off-slit_h_cur+dymin),fix(y_cur-y_off+slit_h_cur-dymax)
;;        print,'img     ',x,fix(ymin_cur+dymin),fix(ymax_cur-dymax)
;        print,'logfile:',logfile,size(slit_img)
;        print,'slit_img',x,slit_img_y0,slit_img_y1
;        print,'img     ',x,img_y0,img_y1

        if((slit_img_y0 le slit_img_y1) and (img_y0 le img_y1) and $
           (slit_img_y1 lt 2*yhsize+1) and (img_y1 lt ny_img)) then slit_img[x,slit_img_y0:slit_img_y1]=img[x,img_y0:img_y1]
    endfor
    mkhdr,hdr_out,slit_img,/image
    maskentry=mask[i]
    sxaddpar,hdr_out,'EXTNAME',strupcase(maskentry.type)+string(i+1,format='(i2.2)')
    sxaddpar,hdr_out,'YOFFSET',y_off 
    sxaddpar,hdr_out,'AIRMASS',sxpar(h,'AIRMASS')
    sxaddpar,hdr_out,'SLITID',maskentry.slit,' slit id'
    sxaddpar,hdr_out,'SLITRA',maskentry.ra,' ra slit coordinate'
    sxaddpar,hdr_out,'SLITDEC',maskentry.dec,' dec slit coordinate'
    sxaddpar,hdr_out,'SLITX',total(maskentry.bbox[[0,2,4,6]])/4.0,' x slit coordinate'
    sxaddpar,hdr_out,'SLITY',total(maskentry.bbox[[1,3,5,7]])/4.0,' y slit coordinate'
    sxaddpar,hdr_out,'SLITTARG',maskentry.target,' target'
    sxaddpar,hdr_out,'SLITOBJ',maskentry.object,' object'
    sxaddpar,hdr_out,'SLITTYPE',maskentry.type,' type'
    sxaddpar,hdr_out,'SLITHEIG',(maskentry.bbox[5]-maskentry.bbox[1]),' slit height'
    sxaddpar,hdr_out,'SLITWIDT',maskentry.width,' slit width'
    sxaddpar,hdr_out,'SLITOFFS',maskentry.offset,' slit offset'
    sxaddpar,hdr_out,'SLITTHET',maskentry.theta,' slit PA (theta)'
    sxaddpar,hdr_out,'MASKID',maskentry.mask_id,' Mask ID from the database'
    sxaddpar,hdr_out,'MASKNAME',maskentry.mask_name,' Mask name from the database'
    sxaddpar,hdr_out,'MASKRA',maskentry.mask_ra,' RA of the field center'
    sxaddpar,hdr_out,'MASKDEC',maskentry.mask_dec,' Dec of the field center'
    sxaddpar,hdr_out,'MASKPA',maskentry.mask_pa,' PA of the mask'
    mwrfits,slit_img,f_out,hdr_out,/silent
endfor

end
