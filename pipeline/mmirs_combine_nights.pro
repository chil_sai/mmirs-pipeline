; mmirs_combine_nights
; Wrapper to set up task files and sum directories to combine multiple nights
; of mmirs reduced pairs.
; INPUTS:
;   dirs: string array of directories containing task files for the
;     individual nights to be combined. The pipeline reduction should have been
;     run on each night individually before running this routine
;    nocopy: set nocopy=1 to skip copying all task files to a single Coadd
;    directory, under the first directory listed in 'dirs'
;   [pass through all coadd-related options to combine_2d_pairs.pro.]
; OUTPUTS:
;   Creates a sum_Coadd directory beneath the top level of the 1st directory
;   listed in 'dirs', where the combined result will be written
pro mmirs_combine_nights,dirs,nocopy=nocopy,nmin=nmin,nmax=nmax,box=box,target=target,anyslit=anyslit,telluric_dir_idx=telluric_dir_idx,num_tel=num_tel,verbose=verbose

if n_elements(dirs) le 1 then begin
  print, 'Please pass an array with more than one night directory'
  return
endif
if keyword_set(nocopy) then begin
  for i=0, n_elements(dirs)-1 do begin
    loglist1=file_search(dirs[i]+'/*.txt')
    if n_elements(loglist1) eq 1 then if loglist1 eq '' then begin
      print, 'No files found for directory ',dirs[i]
      if i eq 0 then loglist=!NULL
      continue
    endif
    if i eq 0 then loglist=loglist1 else loglist=[loglist,loglist1]
  endfor
endif else begin
  combdir=dirs[0]+'Coadd/'
  spawn, 'mkdir -p '+combdir
  for i=0, n_elements(dirs)-1 do begin
    loglist=file_search(dirs[i]+'/*.txt')
    if n_elements(loglist) eq 1 then if loglist eq '' then begin
      print, 'No files found for directory ',dirs[i]
      continue
    endif
    for j=0, n_elements(loglist)-1 do spawn, 'cp '+loglist[j]+' '+combdir+'N'+string(i, format='(I2.2)')+'_'+file_basename(loglist[j])
  endfor

  loglist=file_search(combdir+'/*.txt')
endelse

if n_elements(loglist) eq 1 then if loglist eq '' then begin
  print, 'No files to combine'
  return
endif

n_l=n_elements(loglist)

if(n_elements(nmin) ne 1) then nmin=0
if(n_elements(nmax) ne 1) then nmax=n_l-1
if(n_elements(box) eq 0) then box=0

nmin_corr = (nmin>0)
nmax_corr = (nmax<(n_l-1))

log0=readlog(loglist[nmin_corr])
w_dir=sxpar(log0,'W_DIR')
w_dir=strmid(w_dir,0,strlen(w_dir)-4)
fitprofile=sxpar(log0,'FITPROF')

combine_2d_pairs,loglist[nmin_corr:nmax_corr],w_dir+'/sum_Coadd/',box=box,target=target,verbose=verbose, $
    anyslit=anyslit,fitprofile=fitprofile
combine_2d_pairs,loglist[nmin_corr:nmax_corr],w_dir+'/sum_Coadd/',box=box,/corr,fitprofile=fitprofile,$
    telluric_dir_idx=telluric_dir_idx,num_tel=num_tel,target=target,verbose=verbose,anyslit=anyslit
end
