pro cntrd_1d, img_orig, x_ori, xcen, fwhm_ori, SILENT= silent, DEBUG=debug, oversample=oversample
;+
;  NAME: 
;       CNTRD
;  PURPOSE:
;       Compute the centroid coordinates of an emission line
; EXPLANATION:
;       CNTRD_1D is a 1D version of the DAOPHOT "FIND" centroid algorithm locating the
;       position where the X derivative goes to zero.   This is usually a 
;       more "robust"  determination than a "center of mass" or fitting a 1d 
;       Gaussian  if the wings in one direction are affected by the presence
;       of a neighboring line .
;
;  CALLING SEQUENCE: 
;       CNTRD, img, x, xcen, [ fwhm , /SILENT, /DEBUG]
;
;  INPUTS:     
;       IMG - one dimensional vector array
;       X - Scalar or vector integers giving approximate peak center
;
;  OPTIONAL INPUT:
;       FWHM - floating scalar; Centroid is computed using a box of half
;               width equal to 1.5 sigma = 0.637* FWHM.  CNTRD will prompt
;               for FWHM if not supplied
;
;  OUTPUTS:   
;       XCEN - the computed X centroid position, same number of points as X
;
;       Values for XCEN will not be computed if the computed
;       centroid falls outside of the box, or if the computed derivatives
;       are non-decreasing.   If the centroid cannot be computed, then a 
;       message is displayed and XCEN are set to -1.
;
;  OPTIONAL OUTPUT KEYWORDS:
;       /SILENT - Normally CNTRD prints an error message if it is unable
;               to compute the centroid.   Set /SILENT to suppress this.
;       /DEBUG - If this keyword is set, then CNTRD will display the subarray
;               it is using to compute the centroid.
;
;  PROCEDURE: 
;       Maximum pixel within distance from input pixel X  determined 
;       from FHWM is found and used as the center of a square, within 
;       which the centroid is computed as the value (XCEN) at which 
;       the derivatives of the partial sums of the input image over (x)
;       with respect to (x) = 0.
;
;  MODIFICATION HISTORY:
;       Written 2/25/86, by J. K. Hill, S.A.S.C., following
;       algorithm used by P. Stetson in DAOPHOT.
;       Allowed input vectors        G. Hennessy       April,  1992
;       Fixed to prevent wrong answer if floating pt. X & Y supplied
;               W. Landsman        March, 1993
;       Convert byte, integer subimages to float  W. Landsman  May 1995
;       Converted to IDL V5.0   W. Landsman   September 1997
;       Better checking of edge of frame David Hogg October 2000
;       Avoid integer wraparound for unsigned arrays W.Landsman January 2001
;       Handle case where more than 1 pixel has maximum value W.L. July 2002
;       Converted into 1d case by Igor Chilingarian (chil@sai.msu.su) Aug 2005
;-      
 On_error,2                          ;Return to caller

 if N_params() LT 3 then begin
        print,'Syntax: CNTRD, img, x, xcen, [ fwhm, /SILENT, /DEBUG ]'
        PRINT,'img - Input image vector'
        PRINT,'x - Input scalars giving approximate X position'
        PRINT,'xcen - Output scalars giving centroided X position'
        return
 endif else if N_elements(fwhm_ori) NE 1 then $
      read,'Enter approximate FWHM of image in pixels: ',fwhm_ori

 if(n_elements(oversample) ne 1) then oversample=1
 img = (oversample ge 2)? rebin(img_orig,fix(oversample)*n_elements(img_orig)) : img_orig
 x = (oversample ge 2)? x_ori*double(fix(oversample)) : x_ori
 FWHM = (oversample ge 2)? FWHM_ORI*double(fix(oversample)) : FWHM_ORI
 sz_image = size(img)
 if sz_image[0] NE 1 then message, $
   'ERROR - Vector (first parameter) must be 1 dimensional'

 xsize = sz_image[1]
 dtype = sz_image[2]              ;Datatype

;   Compute size of box needed to compute centroid

 nhalf =  fix(0.637*fwhm) > 2  ;
 nbox = 2*nhalf+1             ;Width of box to be used to compute centroid
 nhalfbig = nhalf +3
 nbig = nbox + 6        ;Extend box 3 pixels on each side to search for max pixel value
 npts = N_elements(x) 
 xcentroid = fltarr(npts)
 xcen = float(x)
 ix = fix( x + 0.5 )          ;Central X pixel        ;Added 3/93

 for i = 0,npts-1 do begin        ;Loop over X vector

 pos = strtrim(x[i],2)

 if ( (ix[i] LT nhalfbig) or ((ix[i] + nhalfbig) GT xsize-1)) then begin
     if not keyword_set(SILENT) then message,/INF, $
           'Position '+ pos + ' too near edge of image'
     xcen[i] = -1
     goto, DONE
 endif

 bigbox = img[ix[i]-nhalfbig : ix[i]+nhalfbig]

;  Locate maximum pixel in 'NBIG' sized subimage 

 mx = max( bigbox)     ;Maximum pixel value in BIGBOX
 mx_pos = where(bigbox EQ mx, Nmax) ;How many pixels have maximum value?
 idx = mx_pos mod nbig          ;X coordinate of Max pixel
 if NMax GT 1 then begin        ;More than 1 pixel at maximum?
     idx = round(total(idx)/Nmax)
 endif else begin
     idx = idx[0]
 endelse

 xmax = ix[i] - (nhalf+3) + idx  ;X coordinate in original image array

; ---------------------------------------------------------------------
; check *new* center location for range
; added by Hogg

 if ( (xmax LT nhalf) or ((xmax + nhalf) GT xsize-1) ) then begin
     if not keyword_set(SILENT) then message,/INF, $
           'Position '+ pos + ' moved too near edge of image'
     xcen[i] = -1
     goto, DONE
 endif
; ---------------------------------------------------------------------

;  Extract smaller 'STRBOX' sized subimage centered on maximum pixel 

 strbox = img[xmax-nhalf : xmax+nhalf]
 if (dtype NE 4) and (dtype NE 5) then strbox = float(strbox)

 if keyword_set(DEBUG) then begin
       message,'Subarray used to compute centroid:',/inf
       print,strbox
 endif  

 ir = (nhalf-1) > 1 
 dd = indgen(nbox-1) + 0.5 - nhalf
; Weighting factor W unity in center, 0.5 at end, and linear in between 
 w = 1. - 0.5*(abs(dd)-0.5)/(nhalf-0.5) 
 sumc   = total(w)

; Find X centroid

 deriv = shift(strbox,-1) - strbox    ;Shift in X & subtract to get derivative
 deriv = deriv[0:nbox-2] ;Don't want edges of the array
 sumd   = total( w*deriv )
 sumxd  = total( w*dd*deriv )
 sumxsq = total( w*dd^2 )

 if sumxd GT 0 then begin  ;Reject if X derivative not decreasing
   
   if not keyword_set(SILENT) then message,/INF, $
        'Unable to compute X centroid around position '+ pos
   xcen[i]=-1
   goto,DONE
 endif 
 dx = sumxsq*sumd/(sumc*sumxd)
 if ( abs(dx) GT nhalf ) then begin    ;Reject if centroid outside box  
   if not keyword_set(SILENT) then message,/INF, $
       'Computed X centroid for position '+ pos + ' out of range'
   xcen[i]=-1
   goto, DONE
 endif

 xcen[i] = xmax - dx    ;X centroid in original array

 DONE: 

 endfor

 if(oversample ge 2) then begin
   xcen=xcen/double(fix(oversample))
 endif
 return
 end


