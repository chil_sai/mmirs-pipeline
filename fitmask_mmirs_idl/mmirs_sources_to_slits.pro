function mmirs_sources_to_slits,objlist,mask_params,field_geometry,$
    slit_geometry,gs_geometry=gs_geometry,$
    box_geometry=box_geometry,min_box_count=min_box_count,max_box_count=max_box_count,$
    slits=slits,guidestars=guidestars,uselst=uselst,$
    wfs_mag_lim=wfs_mag_lim,gs_min=gs_min,verbose=verbose

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; objlist is an array of the object structure:
;     .target -- target ID (longint)
;     .object -- object names (string)
;     .ra -- J2000 R.A., degrees (double precision)
;     .dec -- J2000 Dec, degrees (double precision)
;     .pmra -- R.A. proper motion mas/year (double precision)
;     .pmdec -- Dec proper motion mas/year (double precision)
;     .mag -- magnitude (float)
;     .type -- 'TARGET', 'BOX', 'GUIDE' (both for guide and WFS stars)
;     .priority -- priority for targets, larger numbers higher priority (float)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; field_geometry is a structure:
;     .mask -- bounbox of side a in the focal plane, mm (float[4])
;     .guide -- boundboxes of guide star areas, mm (float[4,N])
;     .wfs -- boundbox of the WFS region, mm (float)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; slit_geometry is a structure:
;     .height -- slit height in arcsec (float)
;     .width  -- slit height in arcsec (flaat)
;     .gap    -- minimal acceptable gap between slits in arcsec (float)
;
; box_geometry is a structure of the same type as slit_geometry, .gap from slit_geometry will be used
; gs_geometry is a structure of the same type as slit_geometry
; wfs_mag_lim is the faint mag limit for wfs stars (which is brighter
; than the general guide star limit)  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

if(n_elements(box_geometry) ne 1) then box_geometry=slit_geometry
if(n_elements(min_box_count) ne 1) then min_box_count=4
if(n_elements(max_box_count) ne 1) then max_box_count=4
if(n_elements(gs_geometry) ne 1) then gs_geometry=slit_geometry
if(n_elements(wfs_mag_lim) ne 1) then wfs_mag_lim=14.5
if(n_elements(gs_min) ne 1) then gs_min=1
guide_limits={wfs_mag_lim:wfs_mag_lim, wfs_bright_lim:12.,guide_mag_lim:16.,guide_bright_lim:12.0, ng_limit:3}

mmirs_expand_mask_params,mask_params
if(keyword_set(uselst)) then begin
    ; no proper motion correction possible
    ra_corr=objlist.ra
    dec_corr=objlist.dec
    lst=(mask_params.ha - mask_params.ra)/15d
    if(lst lt 0) then lst+=24d
    if(lst ge 24) then lst-=24d
    mmirs_radec2xy,$
        ra_corr,dec_corr,$
        mask_params.ra,mask_params.dec,mask_params.posa,lst=lst,objx,objy
 endif else begin
    ; proper motion correction
    mjd_in=mask_params.mjdstart+mask_params.duration/60d/24d/2d
    j_now=(mjd_in-51544.4d)/365.25d + 2000d
    ra_corr=objlist.ra+objlist.pmra*(j_now-objlist.epoch)/cos(objlist.dec/!radeg)/(1000d*3600d) ;milliarsec/yr
    dec_corr=objlist.dec+objlist.pmdec*(j_now-objlist.epoch)/(3600d*1000d)
    mmirs_radec2xy,$
        ra_corr,dec_corr,$
        mask_params.ra,mask_params.dec,mask_params.posa,$
        mjd=mjd_in,objx,objy
 endelse
 
 target_id = where(objlist.type eq 'TARGET' or objlist.type eq 'BOX' or $
                   objlist.type eq 'SKY' or objlist.type eq 'STANDARD',n_slits)
gs_id = where(objlist.type eq 'GUIDE',n_guidestars)
slits=replicate(mmirs_slit_stub(),n_slits)
guidestars=replicate(mmirs_guidestar_stub(),n_guidestars)

slits.x=objx[target_id]
slits.y=-1.0*objy[target_id]
slits.target=objlist[target_id].target
slits.object=objlist[target_id].object
slits.type=objlist[target_id].type
slit_height=fltarr(n_slits)+slit_geometry.height*mask_params.arc2mm
slit_width=fltarr(n_slits)+slit_geometry.width*mask_params.arc2mm
box_id=where(slits.type eq 'BOX',cbox)
if(cbox gt 0) then begin
    if(keyword_set(verbose)) then message,'Number of boxes (input):'+string(cbox),/inf
    slit_height[box_id]=box_geometry.height*mask_params.arc2mm
    slit_width[box_id]=box_geometry.width*mask_params.arc2mm
endif
slits.ra=ra_corr[target_id]
slits.dec=dec_corr[target_id]
slits.pmra=objlist[target_id].pmra
slits.pmdec=objlist[target_id].pmdec
slits.epoch=objlist[target_id].epoch
slits.height=slit_height
slits.width=slit_width
for i=0,n_slits-1 do slits[i].bbox=[slits[i].x,slits[i].y,slits[i].width,slits[i].height]
slits.mag=objlist[target_id].mag
slits.priority=objlist[target_id].priority

guidestars.ra=ra_corr[gs_id]
guidestars.dec=dec_corr[gs_id]
guidestars.x=objx[gs_id]/1.00376 ;guides don't need correction for cold/warm
guidestars.xmask=guidestars.x
guidestars.y=-1.0*objy[gs_id]/1.00376
;for k=0, n_guidestars-1 do if objx[gs_id[k]] > 0 then guidestars[k].xmask= 1.000079d*(objx[gs_id[k]]-56.236)+56.2356 else guidestars[k].xmask = 1.000079d*(objx[gs_id[k]]+56.236)-56.2356  ;56.2356 factor, to be consistent with BinoMask treatment of slits 
guidestars.ymask=guidestars.y;4321.72d*asin(objy[gs_id]/4321.72d)
guidestars.mag=objlist[gs_id].mag
guidestars.pmra=objlist[gs_id].pmra
guidestars.pmdec=objlist[gs_id].pmdec
guidestars.epoch=objlist[gs_id].epoch
guidestars.object=objlist[gs_id].object


id_mask = where(slits.bbox[0]-slits.bbox[2]/2. gt field_geometry.mask[0] and $
                  slits.bbox[0]+slits.bbox[2]/2. lt field_geometry.mask[2] and $
                  slits.bbox[1]-slits.bbox[3]/2. gt field_geometry.mask[1] and $
                  slits.bbox[1]+slits.bbox[3]/2. lt field_geometry.mask[3], c_mask, /null)
if(c_mask lt 1) then begin
    if(keyword_set(verbose)) then message,'Mask is empty!  Configuration rejected',/inf
    return,-1
endif

slits=slits[id_mask]
box_id=where(slits.type eq 'BOX',cbox)
if(keyword_set(verbose)) then message,'Number of boxes (output):'+string(cbox),/inf
if(cbox lt min_box_count) then begin
    if(keyword_set(verbose)) then message,'Too few alignment boxes!  Configuration rejected',/inf
    return,-1
endif

if c_mask ge 1 then begin
   slits.bbox[0]=slits.x ;-56.2356 ;1.000079d*(slits.x-(field_geometry.mask[0]+field_geometry.mask[2])/2.0)
   slits.bbox[1]=slits.y ;4321.7162d*asin(slits.y/4321.7162d)
   slits.slit=lindgen(c_mask)+1l
   for i=0,c_mask-1 do slits[i].polygon=[slits[i].bbox[0]-slits[i].width/2.,slits[i].bbox[1]-slits[i].height/2.,$
                 slits[i].bbox[0]-slits[i].width/2.,slits[i].bbox[1]+slits[i].height/2.,$
                 slits[i].bbox[0]+slits[i].width/2.,slits[i].bbox[1]+slits[i].height/2.,$
                 slits[i].bbox[0]+slits[i].width/2.,slits[i].bbox[1]-slits[i].height/2.]
endif

pri=total(slits.priority)

;new function to select the guide stars
guidestars=mmirs_filter_guides(guidestars, mask_params,gs_geometry,field_geometry, guide_limits, gs_min,valid=valid, verbose=verbose)
;valid=1

return,(valid eq 0? -1 : pri)

end
