pro subtract_dark_mmirs,conf_str,suffix=suffix,median=median

version=get_mmirs_pipeline_version()

if(n_elements(suffix) ne 1) then suffix='.fix.fits'

;;; SCIENCE

if((strlen(conf_str.sci[0]) gt 0) and (strlen(conf_str.sci_dark[0]) gt 0)) then begin
    h_sci=headfits(conf_str.rdir+conf_str.sci+suffix)
    sxaddpar,h_sci,'SOFTWARE',version,' data reduction software'
    if(n_elements(h_sci) eq 1) then message,'FITS file: '+conf_str.rdir+conf_str.sci+suffix+' not found'
    sci=mrdfits(conf_str.rdir+conf_str.sci+suffix,1,h_sci_ext,/silent)
    sxaddpar,h_sci_ext,'BZERO',0.0
    sxaddpar,h_sci_ext,'SOFTWARE',version,' data reduction software'
    dark=average_img(conf_str.rdir+conf_str.sci_dark+suffix,median=median)
    writefits,conf_str.wdir+'obj_dark.fits',0,h_sci
    mwrfits,sci-dark,conf_str.wdir+'obj_dark.fits',h_sci_ext
endif

if((strlen(conf_str.arc[0]) gt 0) and (strlen(conf_str.arc_dark[0]) gt 0)) then begin
    h_arc=headfits(conf_str.rdir+conf_str.arc[0]+suffix)
    sxaddpar,h_arc,'SOFTWARE',version,' data reduction software'
    if(n_elements(h_arc) eq 1) then message,'FITS file: '+conf_str.rdir+conf_str.arc[0]+suffix+' not found'
    arc=average_img(conf_str.rdir+conf_str.arc+suffix,img_hdr=h_arc_ext,median=median)
    sxaddpar,h_arc_ext,'BZERO',0.0
    sxaddpar,h_arc_ext,'SOFTWARE',version,' data reduction software'
    dark=average_img(conf_str.rdir+conf_str.arc_dark+suffix,median=median)
    writefits,conf_str.wdir+'arc_dark.fits',0,h_arc
    mwrfits,arc-dark,conf_str.wdir+'arc_dark.fits',h_arc_ext
endif

if((strlen(conf_str.flat[0]) gt 0) and (strlen(conf_str.flat_dark[0]) gt 0)) then begin
    h_flat=headfits(conf_str.rdir+conf_str.flat[0]+suffix)
    sxaddpar,h_flat,'SOFTWARE',version,' data reduction software'
    if(n_elements(h_flat) eq 1) then message,'FITS file: '+conf_str.rdir+conf_str.flat[0]+suffix+' not found'
    flat=average_img(conf_str.rdir+conf_str.flat+suffix,img_hdr=h_flat_ext,median=median)
    sxaddpar,h_flat_ext,'BZERO',0.0
    sxaddpar,h_flat_ext,'SOFTWARE',version,' data reduction software'
    dark=average_img(conf_str.rdir+conf_str.flat_dark+suffix,median=median)
    writefits,conf_str.wdir+'flat_dark.fits',0,h_flat
    mwrfits,flat-dark,conf_str.wdir+'flat_dark.fits',h_flat_ext
endif

;;; MISC exposures such as tellurics

if((strlen(conf_str.misc[0]) gt 0) and (strlen(conf_str.misc_dark[0]) gt 0)) then begin
    dark=average_img(conf_str.rdir+conf_str.misc_dark+suffix,median=median)
    for i=0,n_elements(conf_str.misc)-1 do begin
        h_misc=headfits(conf_str.rdir+conf_str.misc[i]+suffix)
        sxaddpar,h_misc,'SOFTWARE',version,' data reduction software'
        if(n_elements(h_misc) eq 1) then message,'FITS file: '+conf_str.rdir+conf_str.misc[i]+suffix+' not found'
        misc=mrdfits(conf_str.rdir+conf_str.misc[i]+suffix,1,h_misc_ext,/silent)
        sxaddpar,h_misc_ext,'BZERO',0.0
        sxaddpar,h_misc_ext,'SOFTWARE',version,' data reduction software'
        writefits,conf_str.wdir+conf_str.misc[i]+'_dark.fits',0,h_misc
        mwrfits,misc-dark,conf_str.wdir+conf_str.misc[i]+'_dark.fits',h_misc_ext
    endfor
endif

end
