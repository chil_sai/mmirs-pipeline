function telluric_slit_id,logfile,n_telluric=n_telluric,d_thr=d_thr

if(n_elements(d_thr) ne 1) then d_thr=10.0 ;;; min_dist threshold (arcsec)

log=readlog(logfile)
wdir=def_wdir(logfile)
if(n_elements(n_telluric) ne 1) then n_telluric=1

f_list=get_telluric_list(logfile,n_telluric=n_telluric)
n_f=n_elements(f_list)

mask=read_mask_mmirs_ms(wdir+'mask_mos.txt',logfile=logfile)
n_slits=n_elements(mask)
   
arcsecmm=30.2101*0.2012

match_str=replicate({obs_id:'',slit:-1,dist:!values.d_nan},n_f)

for i=0,n_f-1 do begin
    im_c=mrdfits(wdir+f_list[i]+'_dark.fits',1,h_c,/silent)
    instaz=sxpar(h_c,'INSTAZ')
    instel=sxpar(h_c,'INSTEL')
    d_arr=sqrt((mask.x*arcsecmm-instaz)^2+(mask.y*arcsecmm+instel)^2)

    min_d=min(d_arr,min_d_idx)
    match_str[i].obs_id=f_list[i]
    if(min_d gt d_thr) then begin
        print,'Too far from the slit:',f_list[i],min_d,min_d_idx+1
    endif else begin
        print,'star,Min_dist,slit=',f_list[i],min_d,min_d_idx+1
        match_str[i].slit=min_d_idx+1
        match_str[i].dist=min_d
    endelse
endfor

return,match_str
end
