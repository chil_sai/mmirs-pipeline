; procedure to convert MMIRS processed data into the ESO3D format
; descibed at:
; ftp://ftp.eso.org/pub/dfs/pipelines/doc/VLT-SPE-ESO-19500-5667_DataFormat.pdf
;
;  inp_file -- filename of MMIRS MEF file (N 2D images)
;  out_file -- filename of output E3D file
;
;  KEYWORDS
;  err_file -- error MEF file (same format as inp_file)
;              by default is set to 0
;

pro mmirs_slits_to_eso3d, inp_file, out_file, err_file=err_file, maskfile=maskfile, dithpos=dithpos

if(n_elements(maskfile) ne 1) then maskfile='mask_mos.txt'
if(n_elements(dithpos) ne 1) then dithpos=0.0

mask=read_mask_mmirs_ms(maskfile)
n_slits=n_elements(mask)

inp_data=readfits(inp_file,inp_hdr)

out_hdr_pri=inp_hdr
sxaddpar,out_hdr_pri,'ESO3D','T','All mandatory ESO3D extensions present'
sxaddpar,out_hdr_pri,'NAXIS',0
sxdelpar,out_hdr_pri,'NAXIS1'
sxdelpar,out_hdr_pri,'NAXIS2'
sxdelpar,out_hdr_pri,'NAXIS3'
writefits,out_file,0,out_hdr_pri

n_sp=0L
for s=0,n_slits-1 do begin
    im_cur=mrdfits(inp_file,s+1,inp_ext_hdr,/silent)
    n_wl=sxpar(inp_ext_hdr,'NAXIS1')
    ny_cur=sxpar(inp_ext_hdr,'NAXIS2')
    im_new=transpose(reform(im_cur,n_wl,ny_cur,1),[2,1,0])

    if(keyword_set(err_file)) then begin
        err_cur = mrdfits(err_file,s+1,/silent)
        im_err = transpose(reform(err_cur,n_wl,ny_cur,1),[2,1,0])
        im_qual = fix(im_new*0)
    endif else begin
        im_err = im_new*0.0
        im_qual = fix(im_new*0)
    endelse
    badpix=where(finite(im_err+im_new) ne 1, cbadpix)
    if(cbadpix gt 0) then im_qual[badpix]=1

    out_hdr_data=inp_ext_hdr
    sxaddpar,out_hdr_data,'NAXIS',3
    sxaddpar,out_hdr_data,'NAXIS1',1
    sxaddpar,out_hdr_data,'NAXIS2',ny_cur
    sxaddpar,out_hdr_data,'NAXIS3',n_wl

    sxaddpar,out_hdr_data,'PCOUNT',0
    sxaddpar,out_hdr_data,'GCOUNT',1

    sxaddpar,out_hdr_data,'EXTNAME','SLIT.'+mask[s].OBJECT+'.SCI'
    sxaddpar,out_hdr_data,'HDUCLASS','ESO'
    sxaddpar,out_hdr_data,'HDUDOC','DICD'
    sxaddpar,out_hdr_data,'HDUVERS','DICD version 6'
    sxaddpar,out_hdr_data,'HDUCLAS1','IMAGE'
    sxaddpar,out_hdr_data,'HDUCLAS2','DATA'
    sxaddpar,out_hdr_data,'ERRDATA','SLIT.'+mask[s].OBJECT+'.ERR'
    sxaddpar,out_hdr_data,'QUALDATA','SLIT.'+mask[s].OBJECT+'.DQ'

    arcsecpix=0.2012
    arcsecmm=30.2101*0.2012

    pa = atan(sxpar(inp_hdr,'CD1_1'),sxpar(inp_hdr,'CD1_2'))*!radeg

    cospa=cos(pa*!dpi/180.0)
    sinpa=sin(pa*!dpi/180.0)
;    get_coords,instr=sxpar(inp_hdr,'CAT-RA')+' '+sxpar(inp_hdr,'CAT-DEC'),coords
;    coords[0]=coords[0]*15d
    ;;;coords = [sxpar(inp_hdr,'CRVAL1'),sxpar(inp_hdr,'CRVAL2')]

    coords=[sxpar(inp_ext_hdr,'MASKRA'),sxpar(inp_ext_hdr,'MASKDEC')]

;    xcent=(sxpar(inp_hdr,'CRPIX1')-1024.0)*arcsecpix
;    ycent=(sxpar(inp_hdr,'CRPIX2')-1024.0)*arcsecpix

    xcent=(ny_cur-1.0)/2.0+1.0+dithpos/arcsecpix
    ycent=1.0

    epoch = 2000.0
    wavunit=sxpar(inp_ext_hdr,'CUNIT1',count=cwavunit)
    if(cwavunit eq 0) then wavunit='nm'
    ctype3 = sxpar(inp_ext_hdr,'CTYPE1')
    crval3 = sxpar(inp_ext_hdr,'CRVAL1')
    cdelt3 = sxpar(inp_ext_hdr,'CDELT1')

    sec = arcsecpix/3600d

    c=299792.458d   ;; speed of light

    sxaddpar,out_hdr_data,'EPOCH',2000.0,'Epoch of the coordinates for WCS'

    sxaddpar,out_hdr_data,'RA',coords[0],'Right Ascension J2000'
    sxaddpar,out_hdr_data,'DEC',coords[1],'Declination J2000'
    sxaddpar,out_hdr_data,'PA',pa,'positional angle'

    ;;; setting up spectral WCS
    sxaddpar,out_hdr_data,'CTYPE3',ctype3,'wavelength axis'
    sxaddpar,out_hdr_data,'CRVAL3',crval3,'reference wavelength'
    sxaddpar,out_hdr_data,'CDELT3',cdelt3,'wavelength increment per vector element'
    sxaddpar,out_hdr_data,'CUNIT3','nm','wavelength unit'
    sxaddpar,out_hdr_data,'CRPIX3',1.,'reference pixel'    
    bunit = sxpar(inp_hdr,"BUNIT", count=cbunit)
    if(cbunit gt 0) then sxaddpar,out_hdr_data,'BUNITS', bunit, 'flux unit'

    sxaddpar,out_hdr_data,'CTYPE1','RA---TAN','Right ascension'
    sxaddpar,out_hdr_data,'CUNIT1','deg','units'
    sxaddpar,out_hdr_data,'CRVAL1',coords[0],'reference value'
    sxaddpar,out_hdr_data,'CRPIX1',xcent,'reference point'

    sxaddpar,out_hdr_data,'CTYPE2','DEC--TAN','Declination'
    sxaddpar,out_hdr_data,'CUNIT2','deg','units'
    sxaddpar,out_hdr_data,'CRVAL2',coords[1],'reference value'
    sxaddpar,out_hdr_data,'CRPIX2',ycent,'reference point'

    sxaddpar,out_hdr_data,'CD1_1',-sec*cospa
    sxaddpar,out_hdr_data,'CD2_1', sec*sinpa
    sxaddpar,out_hdr_data,'CD1_2', sec*sinpa
    sxaddpar,out_hdr_data,'CD2_2', sec*cospa

    ;sxaddpar,out_hdr_data,'CROTA1',pa

    pressure=double(sxpar(inp_hdr,'PRESSURE'))
    outtemp=double(sxpar(inp_hdr,'OUTTEMP'))
    airmass=sxpar(inp_hdr,'AIRMASS')

    out_hdr_err=out_hdr_data
    sxdelpar,out_hdr_err,'ERRDATA'
    sxaddpar,out_hdr_err,'EXTNAME','SLIT.'+mask[s].OBJECT+'.ERR'
    sxaddpar,out_hdr_err,'SCIDATA','SLIT.'+mask[s].OBJECT+'.SCI'
    sxaddpar,out_hdr_err,'QUALDATA','SLIT.'+mask[s].OBJECT+'.DQ'
    sxaddpar,out_hdr_err,'HDUCLAS2','ERROR'
    sxaddpar,out_hdr_err,'HDUCLAS3','RMSE'

    out_hdr_qual=out_hdr_data
    sxdelpar,out_hdr_qual,'QUALDATA'
    sxaddpar,out_hdr_qual,'EXTNAME','SLIT.'+mask[s].OBJECT+'.DQ'
    sxaddpar,out_hdr_qual,'SCIDATA','SLIT.'+mask[s].OBJECT+'.SCI'
    sxaddpar,out_hdr_qual,'ERRDATA','SLIT.'+mask[s].OBJECT+'.ERR'
    sxaddpar,out_hdr_qual,'HDUCLAS2','QUALITY'
    sxaddpar,out_hdr_qual,'HDUCLAS3','MASKZERO'

    mwrfits,im_new,out_file,out_hdr_data
    mwrfits,im_err,out_file,out_hdr_err
    mwrfits,im_qual,out_file,out_hdr_qual
endfor

end

