pro linearisation,logfile,obstype,adj=adj,$
    subskybox=subskybox,subskytarget=subskytarget,$
    telluric=telluric,diffmode=diffmode, $
    usebadpixmask=usebadpixmask,maskbadpixels=maskbadpixels,$
    debug=debug

if(n_params() eq 1) then obstype=['obj']

grism=def_grism(LOGFILE,filter=filter)
wdir=def_wdir(LOGFILE)
log=readlog(logfile)
bright=sxpar(log,'BRIGHT')
if keyword_set(telluric) then bright=1
f_disp=(keyword_set(adj))? 'disper_table_adj.fits' : 'disper_table.fits'
wl_data=mrdfits(wdir+f_disp,1,/silent)

Ny=n_elements(wl_data)
gg=where(finite(wl_data.y_mask) eq 1)

mask=read_mask_mmirs_ms(wdir+'mask_mos.txt',logfile=logfile)
dist_map=mrdfits(wdir+'dist_map.fits',1,/silent)

l_par = linearisation_params(grism,filter)
wl0=l_par.wl0
dwl=l_par.dwl
npix=l_par.npix
wl_min=l_par.wl_min
wl_max=l_par.wl_max


ccdscl=0.2 ;;; 0.2 arcsec per pix -- hardcoded CCD scale


for k=0,n_elements(obstype)-1 do begin
    filename=wdir+obstype[k]+'_slits.fits'
    fileout=wdir+obstype[k]+'_slits_lin.fits'
    hpri=headfits(filename)
    writefits,fileout,0,hpri

    val=sxpar(log,'DITHPOS',count=cntval)
    dithpos=(cntval eq 1)? double(strsplit(val,',',/extract)) : 0.0
    n_dith=n_elements(dithpos)
    if not keyword_set(diffmode) then diffmode = (obstype[k] eq 'obj_diff-sky' or obstype[k] eq 'obj_diff' or obstype[k] eq 'err_obj_diff')? 1 : 0
    if (diffmode eq 1) then begin
        val2=sxpar(log,'DITHPOS2',count=cntval2)
        dithpos2=(cntval2 eq 1)? double(strsplit(val2,',',/extract)) : 0.0
        if(cntval2 eq 1) then begin
            dithpos=[dithpos,dithpos2]
            n_dith=n_dith+n_elements(dithpos2)
        endif
    endif

    if(keyword_set(subskybox) or keyword_set(subskytarget)) then begin
        e_apw=10.0
        c_mask = max(abs(dithpos))/ccdscl + e_apw*3
        n_mask = 2*c_mask+1

        sky_mask = bytarr(n_mask)
        for i=0,n_dith-1 do begin
            d = dblarr(n_mask)
            d[c_mask-floor(e_apw/2.0):c_mask+floor(e_apw/2.0)]=1.0
            d0=d
            d=shift_s(d0,dithpos[i]/ccdscl)
            sky_mask[*] = sky_mask[*] + d
        endfor
    endif

    for s=0,n_elements(mask)-1 do begin
        obs=mrdfits(filename,s+1,h,/silent)
        y_off=sxpar(h,'YOFFSET')
        Nx_cur=sxpar(h,'NAXIS1')
        Ny_cur=sxpar(h,'NAXIS2')

        prof_med = median(obs,dim=1)

        if(keyword_set(usebadpixmask) and ((obstype[k] eq 'obj_diff-sky') or (obstype[k] eq 'obj_diff') or (obstype[k] eq 'err_obj_diff')) ) then begin
            bpmask_obs=mrdfits(wdir+'obj_diff-sky_bpmask_slits.fits',s+1,/silent)
            bpix=where(bpmask_obs ne 0,cbpix)
            if(cbpix gt 0) then begin
                obs[bpix]=!values.f_nan
                if(not (keyword_set(maskbadpixels))) then begin
                    obsmed3=djs_median(obs,width=3,boundary='reflect')
                    obs[bpix]=obsmed3[bpix]
                    bpix2nd=where(finite(obs) ne 1,cbpix2nd)
                    if(cbpix2nd gt 0) then begin
                        obsmed5=djs_median(obs,width=5,boundary='reflect')
                        obs[bpix2nd]=obsmed5[bpix2nd]
                    endif
                endif
            endif
        endif

;        obs_t = fltarr(Nx_cur,Ny)+!values.f_nan
;        ;;;;;;;;;; FIXIT!!!!!!!!!!!!
;        extract_slit_image,obs_t,obs,y_off,/f
;        obs_r=rotate(poly_2d(rotate(obs_t,5),dist_map.kxwrap,dist_map.kywrap,2,cubic=-0.5),5)
;        extract_slit_image,obs_r,obs,y_off,ny_cur
;        ;;;;;;;;;;;;;;;;;;;;;;;;;;;
        obs=mmirs_rectify_slit(obs,dist_map,y_off,/edge)
    
        obs_lin=fltarr(npix,Ny_cur)
        wl_sc_lin=wl0+findgen(npix)*dwl
        flag_arr=bytarr(Ny_cur)
    
        for i=0,Ny_cur-1 do begin
            y_corr=y_off+i
            if(y_corr lt 0 or y_corr gt Ny-1) then continue
            wl_sc_cur=poly(findgen(Nx_cur),wl_data[y_corr].wl_sol)
            obs_lin[*,i]=interpol(obs[*,i],wl_sc_cur,wl_sc_lin)
            goodpix=where(finite(obs[*,i]) eq 1, cgoodpix, ncompl=cbad)
            if(cgoodpix gt 0) then begin
                mingoodwl=min(wl_sc_cur[goodpix],minidx,max=maxgoodwl,subscript_max=maxidx)
    ;            print,'slit,minwl,maxwl=',s+1,wl_sc_cur[minidx],wl_sc_cur[maxidx]
                badwl=where((wl_sc_lin lt mingoodwl) or $
                            (wl_sc_lin gt maxgoodwl), cbadwl)
                if(cbadwl gt 1) then obs_lin[badwl,i]=!values.f_nan
            endif
        endfor
    
        wl=wl_sc_lin/10d

        if((grism eq 'HK') or (grism eq 'H') or (grism eq 'H3000')) then begin
            wlmin_reg=1500.0
            wlmax_reg=1700.0
        endif
        if(grism eq 'J') then begin
            wlmin_reg=1180.0
            wlmax_reg=1300.0
        endif
        if(grism eq 'K3000') then begin
            wlmin_reg=2100.0
            wlmax_reg=2250.0
        endif

        good_reg=where((wl ge wlmin_reg) and (wl le wlmax_reg))
        prof_spec = median(obs_lin[good_reg,*],dim=1)
        gprof_spec = where(finite(prof_spec) eq 1, cgprof_spec)
        xb=findgen(Ny_cur)-(Ny_cur-1)/2.0

        if(n_elements(telluric) gt 0) then begin
            ;;;;;;;;;;;;;;; telluric code here
            detected=0
            if(cgprof_spec gt 7) then begin
               if diffmode eq 1 then $
                  fit_prof_spec=gaussfit(xb[gprof_spec],abs(prof_spec[gprof_spec]),nterms=3,cgauss,chisq=c2) else $
                  fit_prof_spec=gaussfit(xb[gprof_spec],prof_spec[gprof_spec],nterms=3,cgauss,chisq=c2)
                if((cgauss[0] gt 0) and (cgauss[2] gt 0.05/ccdscl) and (cgauss[2] lt 1.0/ccdscl)) then begin ;;; 0.7/ccdscl is too low for bad seeing
                    detected = 1 ;;;;; proper condition must come here
                    print,'Object detected in slit'+string(s+1)
                    dith_offset=cgauss[1]*ccdscl
                    sxaddpar,h,'OBJPOS',dith_offset,' object position estimated from the spectrum'
                    sxaddpar,h,'OBJFWHM',cgauss[2]*ccdscl*2.355,' trace Gaussian FWHM estimated from the spectrum'
                    print,'slit,ObjPos,FWHM=',s+1,dith_offset,cgauss[2]*ccdscl*2.355,'  chi2=',c2
                    if(keyword_set(debug)) then begin
                        plot,xb*0.2,prof_spec,xs=1,ys=1,psym=-4,title='Slit #'+string(s+1,format='(i2)'),xtit='position, arcsec'
                        oplot,xb[gprof_spec]*0.2,fit_prof_spec,linest=2,col=128
;                        aaa=''
;                        read,aaa
                    endif
                endif
            endif

            if(detected eq 1) then begin
                e_apw=10.0
                c_mask = max(abs(dithpos))/ccdscl + e_apw*3
                n_mask = 2*c_mask+1
                sky_mask = bytarr(n_mask)
                for i=0,n_dith-1 do begin
                    d = dblarr(n_mask)
                    d[c_mask-floor(e_apw/2.0):c_mask+floor(e_apw/2.0)]=1.0
                    d0=d
                    d=shift_s(d0,dith_offset/ccdscl)
                    sky_mask[*] = sky_mask[*] + d
                endfor
            endif
        endif

        if(keyword_set(subskybox) or keyword_set(subskytarget)) then begin
            sky_cur=0.0
            mask_cur=bytarr(ny_cur)

            if(diffmode eq 1 and bright eq 1 and cgprof_spec ge 6) then begin
                prof_spec_pos=(prof_spec > 0.0)
                max_prof = max(smooth(prof_spec_pos,3,/nan), max_prof_idx)
                fit_prof_spec=gaussfit(xb[gprof_spec],prof_spec_pos[gprof_spec],nterms=3,cgauss)
		res_std=robust_sigma(prof_spec_pos[gprof_spec]-fit_prof_spec)
		if((cgauss[0] gt 20.0*res_std) and $
		   (cgauss[2] gt 0.1/ccdscl) and $
		   (abs(cgauss[1]) lt Ny_cur/4.0)) then begin ;;;;; detected
		    hwidth=((round(cgauss[2])) > 1)
		    dshift = round((dithpos[0]-dithpos[1])/ccdscl)

		    idx1min=0 > (cgauss[1]+(Ny_cur-1)/2.0-hwidth) < (Ny_cur-1)
		    idx1max=0 > (cgauss[1]+(Ny_cur-1)/2.0+hwidth) < (Ny_cur-1)
		    mask_cur[idx1min:idx1max]=1

		    idx2min=0 > (cgauss[1]+(Ny_cur-1)/2.0-hwidth-dshift) < (Ny_cur-1)
		    idx2max=0 > (cgauss[1]+(Ny_cur-1)/2.0+hwidth-dshift) < (Ny_cur-1)
		    mask_cur[idx2min:idx2max]=1
		    
                    goodm = where((mask_cur eq 0) and (finite(prof_spec) eq 1),cgoodm)
                    if(cgoodm gt 2) then sky_cur=median(obs_lin[*,goodm],dim=2)
		endif else begin ;;;; undetected
		    goodm = where((mask_cur eq 0) and (finite(prof_spec) eq 1),cgoodm)
                    if(cgoodm gt 2) then sky_cur=median(obs_lin[*,goodm],dim=2)
		endelse
            endif else begin
                c_m_c=(ny_cur-1)/2
                if(ny_cur ge n_mask) then $
                    mask_cur[c_m_c-c_mask:c_m_c+c_mask]=sky_mask $
                else $
                    mask_cur[*]=sky_mask[c_mask-c_m_c:c_mask+c_m_c]
                goodm = where(mask_cur eq 0)
                
                sky_cur=median(obs_lin[*,goodm],dim=2)
            endelse

            if((keyword_set(subskybox) and mask[s].type eq 'BOX') or $
               (keyword_set(subskytarget) and mask[s].type eq 'TARGET')) then begin
                for i=0,Ny_cur-1 do obs_lin[*,i]=obs_lin[*,i]-sky_cur
                sxaddhist,'Sky additionally subtracted from linearised spectra',h
            endif
        endif

        if(mask[s].type eq 'BOX' and ~(obstype[k] eq 'arc' or obstype[k] eq 'flat' or $
           obstype[k] eq 'err_obj' or obstype[k] eq 'err_obj_diff')) then begin
            prof_box = median(obs_lin,dim=1)
            gprof_box = where(finite(prof_box) eq 1, cgprof_box)
            if(cgprof_box gt 7) then begin
                xb=findgen(Ny_cur)-(Ny_cur-1)/2.0
                fit_prof_box=gaussfit(xb[gprof_box],prof_box[gprof_box],nterms=3,cgauss,chisq=c2)
                dith_offset=cgauss[1]*ccdscl
                trcfwhm=cgauss[2]*ccdscl*2.355
                if(finite(trcfwhm) ne 1 or finite(dith_offset) ne 1) then begin 
                    dith_offset=0d
                    trcfwhm=-1d
                endif
                print,'slit,DithPos,FWHM=',s+1,dith_offset,trcfwhm,'  chi2=',c2
                sxaddpar,h,'DITHCOMP',dith_offset,' dithering position estimated from alignbox'
                sxaddpar,h,'TRCFWHM',trcfwhm,' trace Gaussian FWHM estimated from alignbox'
;                plot,xb,prof_box,xs=1
;                oplot,xb[gprof_box],fit_prof_box,psym=-4,linest=2
            endif
        endif
            
        sxaddpar,h,'CTYPE1','WAVE'
        sxaddpar,h,'CUNIT1','nm'
        sxaddpar,h,'CRPIX1',1.0
        sxaddpar,h,'CRVAL1',wl0/10d
        sxaddpar,h,'CDELT1',dwl/10d
        sxaddpar,h,'CD1_1',dwl/10d
        sxaddpar,h,'SLITID',mask[s].slit,' slit id'
        sxaddpar,h,'SLITRA',mask[s].ra,' ra slit coordinate'
        sxaddpar,h,'SLITDEC',mask[s].dec,' dec slit coordinate'
        sxaddpar,h,'SLITX',mask[s].x,' x slit coordinate'
        sxaddpar,h,'SLITY',mask[s].y,' y slit coordinate'
        sxaddpar,h,'SLITTARG',mask[s].target,' target'
        sxaddpar,h,'SLITOBJ',mask[s].object,' object'
        sxaddpar,h,'SLITTYPE',mask[s].type,' type'
        sxaddpar,h,'SLITHEIG',mask[s].height,' slit height'
        sxaddpar,h,'SLITWIDT',mask[s].width,' slit width'
        sxaddpar,h,'SLITOFFS',mask[s].offset,' slit offset'
        sxaddpar,h,'SLITTHET',mask[s].theta,' slit tilt (theta)'
;        sxaddpar,h,'NAXIS',3
;        sxaddpar,h,'NAXIS3',1
;        sxaddpar,h,'CTYPE2','RA---ZPN'
;        sxaddpar,h,'CTYPE3','DEC--ZPN'
;        sxaddpar,h,'CRVAL2',mask[s].ra
;        sxaddpar,h,'CRVAL3',mask[s].dec
;        sxaddpar,h,'CDELT2',ccdscl/3600d
;        sxaddpar,h,'CDELT3',1
;        sxaddpar,h,'CRPIX2',1+(Ny_cur-1)/2.0+dithpos[0]/ccdscl
;        sxaddpar,h,'CRPIX3',1
;        sxaddpar,h,'LONPOLE',90.0-double(sxpar(hpri,'ROTANGLE'))
        sxaddpar,h,'RADECSYS','FK5',' Coordinate System'
        mwrfits,obs_lin,fileout,h
    endfor
endfor

end
