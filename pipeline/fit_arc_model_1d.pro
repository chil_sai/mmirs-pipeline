function arc_model_1d, nx, wlsol, fwhm, line_tables_inp=line_tables_inp, list_idx=list_idx

common arc_model_1d_data, line_tables, list_counts, wl_lines, flag_lines, flux_lines

    if(n_elements(list_idx) ne 1) then list_idx=0

    wl=poly(dindgen(nx),wlsol)
    wl_0=wl[0]
    d_wl=wl[1]-wl[0]

    if(n_elements(line_tables) eq 0) then line_tables=[' ']
    if(array_equal(line_tables_inp,line_tables) eq 0) then begin
        message,/inf,'Reading line lists'
        n_lists=n_elements(line_tables_inp)
        line_tables=line_tables_inp
        list_counts=lonarr(n_lists)
        wl_lines=dblarr(5000,n_lists)+!values.d_nan
        flag_lines=intarr(5000,n_lists)+!values.d_nan
        flux_lines=dblarr(5000,n_lists)+!values.d_nan

        for i=0,n_lists-1 do begin
            tab_tmp=read_asc(line_tables[i])
            list_counts[i]=(n_elements(tab_tmp[0,*])) < 5000
            wl_lines[0:list_counts[i]-1]=transpose(tab_tmp[0,*])
            flag_lines[0:list_counts[i]-1]=transpose(tab_tmp[1,*])
            flux_lines[0:list_counts[i]-1]=transpose(tab_tmp[2,*])
        endfor
    endif

    ; filtering the input line list
    N_lines=list_counts[list_idx]
    line_sel=where(wl_lines[*,list_idx] gt wl[0] and wl_lines[*,list_idx] lt wl[Nx-1],nline_sel)

    x=dindgen(nx)
    x_lin=interpol(x,wl,transpose(wl_lines[line_sel,list_idx]))

    arc_model_spectrum=dblarr(nx)
    sigma=fwhm/2.35

    if(fwhm gt 0.0) then begin
        for k=0,nline_sel-1 do begin
            idxmin=0 > (round(x_lin[k]-7*sigma)) < (nx-1)
            idxmax=0 > (round(x_lin[k]+7*sigma)) < (nx-1)
            if(idxmin lt idxmax) then $
                arc_model_spectrum[idxmin:idxmax]=arc_model_spectrum[idxmin:idxmax]+$
                    flux_lines[line_sel[k],list_idx]*exp(-((x[idxmin:idxmax]-x_lin[k])/sigma)^2/2)
        endfor
    endif else begin
        x_lin_low = 0 > (double(fix(x_lin))) < (nx-2)
        w_x=x_lin-x_lin_low
        arc_model_spectrum[x_lin_low] = arc_model_spectrum[x_lin_low] + flux_lines[line_sel]*(1d -w_x)
        arc_model_spectrum[x_lin_low+1] = arc_model_spectrum[x_lin_low+1] + flux_lines[line_sel]*w_x
    endelse
    return,arc_model_spectrum
end


function evaluate_arc_model_1d, pars, ndeg=ndeg, nparlsf=nparlsf, $
    spectrum=spectrum, noise=noise, goodpixels=goodpixels, mdegree=mdegree, degree=degree, $
    weights=weights, linetabs=linetabs, mpoly=mpoly, addcont=addcont, splcont=splcont, lincont=lincont, debug=debug

common mpoly_arr_cache, mpoly_arr

;;; pars[0:Ndeg] => dispersion relation
;;; pars[Ndeg+1:Ndeg+nparlsf] => losvd params: gaussian sigma, h3, h4... 1 for pure Gaussian
;;; pars[Ndeg+nparlsf+1:*] => mpoly


;    print,'Current step wl guess:',pars[0:ndeg]

    t0=systime(1)
    npix=n_elements(spectrum)

;    if(n_elements(linetabs) eq 0) then linetabs=getenv('MMIRS_PIPELINE_PATH')+['calib_MMIRS/linelists/linesArGNIRS.tab']
    if(n_elements(linetabs) eq 0) then linetabs=getenv('MMIRS_PIPELINE_PATH')+['calib_MMIRS/linelists/t0.tab']
;    if(n_elements(linetabs) eq 0) then linetabs=getenv('MMIRS_PIPELINE_PATH')+['calib_MMIRS/linelists/t1.tab','calib_MMIRS/linelists/t2.tab']
    if(n_elements(degree) ne 1) then degree=-1

    nmodels=n_elements(linetabs)

    npars=n_elements(pars) - ndeg-1 - mdegree*(1-keyword_set(lincont)) ; Parameters of the LOSVD

    sigidx=1 + ndeg
    maxvel=0 ;;; no gaussian shift in the lsf
    maxsig=pars[ndeg+1]
    dx = ceil(abs(maxvel)+5d*maxsig) ; Sample the Gaussian and GH at least to vel+5*sigma
    n = 2*dx + 1
    x = range(dx,-dx,n)   ; Evaluate the Gaussian using steps of 1 pixel
    lsf = dblarr(n,/NOZERO)

    vel = 0.0 ;;; no gaussian velocity shift in the lsf
    w = (x - vel)/pars[sigidx]
    w2 = w^2
 
    lsf = exp(-0.5d*w2)/(sqrt(2d*!dpi)*pars[sigidx]) ; Normalized total(Gaussian)=1

    ; Hermite polynomials normalized as in Appendix A of van der Marel & Franx (1993).
    ; These coefficients are given e.g. in Appendix C of Cappellari et al. (2002)
    ;
    if npars gt 1 then begin
        h3idx = 2 + ndeg
        h4idx = 3 + ndeg
        polyh = 1d + pars[h3idx]/Sqrt(3d)*(w*(2d*w2-3d)) $     ; H3
                  + pars[h4idx]/Sqrt(24d)*(w2*(4d*w2-12d)+3d)   ; H4
        if npars eq 6 then begin
            h5idx =4 + ndeg
            h6idx =5 + ndeg
            polyh = polyh + pars[h5idx]/Sqrt(60d)*(w*(w2*(4d*w2-20d)+15d)) $    ; H5
                        + pars[h6idx]/Sqrt(720d)*(w2*(w2*(8d*w2-60d)+90d)-15d)  ; H6
        endif
        lsf = lsf*polyh
    endif

;;    wl=poly(dindgen(npix),pars[0:ndeg])

    x = range(-1d,1d,npix) ; X needs to be within [-1,1] for Legendre Polynomials
    xgrid=range(-1d,1d,mdegree+1)

    c=dblarr(npix,degree+1+nmodels)

    for j=0,degree do $ ; Fill first columns of the Design Matrix
        c[*,j] = legendre(x,j)

    for i=0,nmodels-1 do begin
        mod_tmp=arc_model_1d(npix,pars[0:ndeg],1.0,line_tab=linetabs,list_idx=i)
        c[*,degree+1+i]=convol(mod_tmp,lsf,/edge_truncate)
    endfor

    a = c                     ; This array is used for the actual solution of the system
    for j=0,(degree+1)+nmodels-1 do a[*,j] = c[*,j]/noise ; Weight all columns with errors

    mpoly=1d

    if(mdegree ge 1 and keyword_set(lincont)) then begin
        s_mp=size(mpoly_arr)
        if(s_mp[0] ne 2 or s_mp[1] ne npix or s_mp[2] ne mdegree) then begin
            mpoly_arr = dblarr(npix,mdegree)
            for j=1,mdegree do begin
                mpoly_arr[0:npix-1,j-1] = double(legendre(x,j))
            endfor
        endif
    endif else begin
        if(keyword_set(splcont)) then begin
            mpoly=(interpol( pars[ndeg+nparlsf+1:ndeg+nparlsf+1+mdegree],xgrid,x,/spline))^2
        endif else begin
            for j=1,mdegree+1 do $
                mpoly = mpoly + double(legendre(x,j)) * pars[ndeg+nparlsf+j]
        endelse
    endelse

    if(mdegree ge 1 and keyword_set(lincont)) then begin
        weights_t = BVLS_Solve_pxf(a[goodPixels,degree+1:*],spectrum[goodPixels]/noise[goodPixels],nmodels)
;        print,'WEIGHTS_T: ',weights_t,format='(a,5g30.25)'
        bestfit = c[*,degree+1:*] # weights_t
        mpoly_arr_t = mpoly_arr[goodPixels,*]
        bnd_mpoly=dblarr(2,mdegree)
        bnd_mpoly[0,0]=1d-10 ;;;;-10000d
        bnd_mpoly[1,0]=1d10 ;;; 10000d
        bnd_mpoly[0,1:*]=-1d
        bnd_mpoly[1,1:*]=1d
        for j=1,mdegree do mpoly_arr_t[*,j-1]=mpoly_arr_t[*,j-1]*bestfit[goodPixels]/noise[goodPixels]
        bvls,mpoly_arr_t,((spectrum-bestfit)/noise)[goodPixels],bnd_mpoly,mpoly_weights,itmax=100
        mpoly = 1d + (mpoly_arr # mpoly_weights)
;        print,'mpoly: ',mpoly_weights
        bestfit = bestfit*mpoly
    endif

    for j=(degree+1),(degree+1)+nmodels-1 do begin
        a[*,j] = mpoly*a[*,j]
        c[*,j] = mpoly*c[*,j]
    endfor

    npoly=degree+nmodels

    if(degree eq -1) then begin
        weights = BVLS_Solve_pxf(a[goodPixels,*],spectrum[goodPixels]/noise[goodPixels],npoly)
;                        print,'WEIGHTS: ',weights,format='(a,5g30.25)'
        full_total=total(weights,/DOUBLE)
        small_weights=where((weights lt 1d-03*full_total),$
                    sm_count,complement=large_weights)
        if(sm_count gt 0) then begin
            weights[small_weights]=0.0
            aa=a[*,large_weights]
            weights_new=BVLS_Solve_pxf(aa[goodPixels,*],$
                           spectrum[goodPixels]/noise[goodPixels],npoly)
            weights[large_weights]=weights_new[*]
            ;print,'WEIGHTSN:',weights,format='(a,5g30.25)'
        endif
        bestfit = c # weights
    
        addcont=dblarr(n_elements(bestfit))
    endif else begin
        weights_all = double(BVLS_Solve_pxf(a[goodPixels,*],spectrum[goodPixels]/noise[goodPixels],npoly))
        weights=weights_all[degree+1:*]
        bestfit0 = c[*,degree+1:*] # weights_all[degree+1:*]
        addcont = (c[*,0:degree] # weights_all[0:degree])
        bestfit = bestfit0 + addcont
    endelse

    if(keyword_set(debug)) then begin
        wl=poly(dindgen(npix),pars[0:ndeg])
        plot,wl,spectrum,xs=1,ys=3,yr=[-250,1250]; /10d
        spec_good=spectrum*!values.f_nan
        spec_good[goodPixels]=spectrum[goodPixels]
        oplot,wl,spec_good,col=220,thick=4
        oplot,wl,bestfit,col=128
        oplot,wl,spectrum-bestfit,col=80
        oplot,wl,spec_good-bestfit,col=80,thick=4
        oplot,wl,noise,col=40
        oplot,wl,-noise,col=40
        oplot,wl,mpoly*100.0,col=254
        oplot,wl,addcont,col=220
    endif

    return, bestfit
    
end





FUNCTION fitfunc_arc_model_1d, pars, $
    NDEG=ndeg, BESTFIT=bestFit, BIAS=bias, CLEAN=clean, DEGREE=degree, $
    SPECTRUM=spectrum, GOODPIXELS=goodPixels, MDEGREE=mdegree, SPLCONT=splcont, $
    NOISE=noise, WEIGHTS=weights,$
    ADDCONT=addcont, NPARLSF=nparlsf, LINCONT=lincont, DEBUG=debug, linetabs=linetabs


bestfit=evaluate_arc_model_1d(pars, ndeg=ndeg, nparlsf=nparlsf, debug=debug, $
    spectrum=spectrum, noise=noise, goodpixels=goodpixels, mdegree=mdegree, splcont=splcont, $
    degree=degree, weights=weights, linetabs=linetabs, mpoly=mpoly,addcont=addcont,lincont=lincont)

err = double((spectrum[goodPixels]-bestfit[goodPixels])/noise[goodPixels])

; In a previous version we used to iterate the fit of the fraction
; with the kappa-sigma clipping.
; Now we do it separately
; Iterate to exclude pixels deviating more than 3*sigma if /CLEAN
; keyword is set.
if keyword_set(clean) then begin
    repeat begin
        rbst_sig=stddev(err[*])
        tmp = where(abs(err[*]) gt 3/1.*rbst_sig, m, COMPLEM=w) ; select errors larger than 3*sigma
        ; avoid to remove too many
        if (m gt 0.03*n_elements(goodPixels)) then $
          tmp = where(abs(err[*]) gt 4/1.*rbst_sig, m, COMPLEM=w)
        if (m gt 0.03*n_elements(goodPixels)) then $
          tmp = where(abs(err[*]) gt 5/1.*rbst_sig, m, COMPLEM=w)
        if (m gt 0.03*n_elements(goodPixels)) then $
          tmp = where(abs(err[*]) gt 7/1.*rbst_sig, m, COMPLEM=w)

        if (m ne 0) then begin
            print, 'Number of clipped outliers:', m,' out of',n_elements(goodPixels), rbst_sig
            goodPixels = goodPixels[w]
            err=err[w]
        endif
    endrep until (m eq 0)
endif

; Penalize the solution towards (h3,h4,...)=0 if the inclusion of
; these additional terms does not significantly decrease the error.
;
if ((nparlsf gt 2) and (bias ne 0)) then $
    err = err + bias*robust_sigma(err, /ZERO)*sqrt(total(pars[ndeg+2:ndeg+2+nparlsf-1]^2,/DOUBLE))

t5=systime(1)

if(keyword_set(debug)) then print,'Current step wl guess:',pars[0:ndeg+1],' chi2/DOF=',total(err^2)/(n_elements(err)-n_elements(pars)-1)

return, err
END



    
function fit_arc_model_1d,spectrum=spectrum0,noise=noise0,disp_ini=disp_ini,fix_disp=fix_disp,nparlsf=nparlsf,$
    mdegree=mdegree,degree=degree,goodpixels=goodpixels0,splcont=splcont,lincont=lincont,fwhm=fwhm,bias=bias,$
    clean=clean,smooth=smooth,debug=debug,pars=pars,approxcont=approxcont,linetabs=linetabs


    if(n_elements(linetabs) eq 0) then linetabs=getenv('MMIRS_PIPELINE_PATH')+['calib_MMIRS/linelists/t0.tab']

    spectrum=spectrum0
    npix=n_elements(spectrum)
    noise=(n_elements(noise0) eq n_elements(spectrum))? noise0 : spectrum*0.0+1d

    gspec=where(finite(spectrum + noise) eq 1)
    if(n_elements(goodpixels0) ge n_elements(spectrum) or n_elements(goodpixels0) eq 0) then goodpixels0=gspec
    goodpixels=goodpixels0
    cgoodpixels=n_elements(goodpixels)

    if(keyword_set(approxcont)) then begin
        model = arc_model_1d(npix,disp_ini,fwhm,line=linetabs)

        n_win=10
        flux_spec=dblarr(n_win)
        flux_mod=dblarr(n_win)
        idx_mean=dblarr(n_win)
        for i=0,n_win-1 do begin
            idxmin=0 > ((i/double(n_win))*cgoodpixels) < (cgoodpixels-1)
            idxmax=0 > (((i+1)/double(n_win))*cgoodpixels) < (cgoodpixels-1)
            idx_mean[i]=(idxmin+idxmax)/2.0
            flux_spec[i]=total((spectrum[goodpixels[idxmin:idxmax]]) > 0)
            flux_mod[i]= total(model[goodpixels[idxmin:idxmax]])
        endfor
        good_cont=where(flux_spec gt 0 and flux_mod gt 0, cgood_cont)
        cont_vec = interpol((flux_spec/flux_mod)[good_cont],idx_mean[good_cont],dindgen(npix),/spline)

        spectrum=spectrum/cont_vec
        noise=noise/(cont_vec > (min((flux_spec/flux_mod)[good_cont])))
        
    endif



    ndeg=n_elements(disp_ini)-1
    if(n_elements(nparlsf) ne 1) then nparlsf=1
    if(n_elements(degree) ne 1) then degree=-1
    if(n_elements(mdegree) ne 1) then mdegree=5 ;;; default spline with 5 nodes
    if(n_elements(bias) ne 1) then bias=0d
    if(n_elements(fwhm) ne 1) then fwhm=1.5d
    if(n_elements(fix_disp) ne ndeg+1) then fix_disp=intarr(ndeg+1)

    if(n_elements(smooth) eq 1) then begin
        gkrn=psf_gaussian(ndim=1,fwhm=[smooth],npix=[smooth*3.0],/norm)
        spectrum[gspec]=convol(spectrum[gspec],gkrn,/norm,/edge_truncate)
        noise[gspec]=convol(noise[gspec],gkrn,/norm,/edge_truncate)
        fwhm=smooth
    endif

    functArgs = {BIAS:bias, DEGREE:degree, NDEG:ndeg, spectrum:double(spectrum), $
        GOODPIXELS:goodPixels, MDEGREE:mdegree, SPLCONT:keyword_set(splcont), NOISE:double(noise), $
        NPARLSF:fix(nparlsf),LINCONT:keyword_set(lincont), DEBUG:keyword_set(debug), LINETABS:linetabs}

    start1 = dblarr(ndeg+1+nparlsf+(mdegree+1)*(1.0-keyword_set(lincont)))

    start1[0:ndeg]=disp_ini     ;;;; initial guess for the dispersion relation
    start1[ndeg+1]=fwhm/2.355          ;;;; default gaussian sigma of 1pix
    
    if(not keyword_set(lincont)) then $
        start1[ndeg+nparlsf+1:*]=1.0  ;;;; no multiplicative continuum initially (exp(cont)=0.0, so cont=1.0)
    nst1=n_elements(start1)
    parinfo = REPLICATE({step:5d-2,limits:[-10d,10d],limited:[0,0],fixed:0,relstep:0}, nst1)

    parinfo[0].step=disp_ini[1]
    parinfo[1].step=disp_ini[1]/500.0
    for i=0,1 do begin ;;;; starting wl and dispersion
        parinfo[i].limits=[0.95,1.05]*disp_ini[i]
        parinfo[i].limited=[1,1]
    endfor

    parinfo[0:ndeg].fixed=fix_disp[*]

    for i=2,ndeg do parinfo[i].step=10d^(-i*2) ;;; force smaller steps for high order params

    
    parinfo[ndeg+1].limits=[0.1d,100d]
    parinfo[ndeg+1].limited=[1,1]

;;    if(n_elements(smooth) eq 1) then parinfo[ndeg+1].fixed=1 ;;; fixing the LSF sigma for a case of smoothed version

    for i=1,nparlsf-1 do begin
        parinfo[ndeg+1+i].limits=[0d,0.3d]
        parinfo[ndeg+1+i].limited=[1,1]
    endfor

    if(not keyword_set(splcont) and not keyword_set(lincont)) then begin
        for i=ndeg+nparlsf+1,ndeg+nparlsf+1+mdegree do begin
            start1[i]=0d
            parinfo[i].limits=[-1d,1d]
            parinfo[i].limited=[1,1]
        endfor
    endif

    ftol=1d-10
    good = goodPixels
    for j=0,5 do begin ; Do at most five cleaning iterations
        time0=systime(1)

        ; when cleaning is enabled, we gradually decrease ftol along the iterations
        if keyword_set(clean) then begin
            print,'iteration:',j,' number of good pixels',n_elements(goodPixels)
            if j eq 0 then ftol=1d-1
            if j eq 1 then ftol=0.5d-1
            if j eq 2 then ftol=1d-2
            if j eq 3 then ftol=4d-3
            if j eq 4 then ftol=1d-3
            if j eq 5 then ftol=5d-4
        endif

        pars = mpfit('fitfunc_arc_model_1d', start1, ERRMSG=errmsg, $
            FTOL=ftol, FUNCTARGS=functArgs, NFEV=ncalls, PARINFO=parinfo, $
            PERROR=error, STATUS=mpfstat, XTOL=1d-10, /QUIET, COVAR=pcovar)
        time1=systime(1)

        ;print,'Total timing (sec): ',time1-time0,' per step (ms):',$
        ;   1000.0*(time1-time0)/ncalls,format='(a,f8.3,a,i6)'
        message,'MPFIT STATUS='+string(mpfstat,format='(i4)'),/inf
        print,'res=',pars
        if (errmsg ne '' or mpfstat eq 0) then begin
            message, errmsg, /inf
            bestfit=spectrum*!values.f_nan            
            message,/inf,'EXITING FITTING PROCEDURE WITH NaN results'
            return,!values.f_nan
        endif
        if not keyword_set(clean) then break
        goodOld = goodPixels
        if j gt 1 then begin
            tmp = fitfunc_arc_model_1d(res, $
                NDEG=ndeg, BESTFIT=bestFit, BIAS=bias, /CLEAN, DEGREE=degree, $
                SPECTRUM=spectrum, GOODPIXELS=goodPixels, MDEGREE=mdegree, $
                NOISE=noise, WEIGHTS=weights,$
                ADDCONT=addcont, NPARLSF=nparlsf,LINCONT=LINCONT,SPLCONT=SPLCONT,LINETABS=linetabs,DEBUG=DEBUG)
            if array_equal(goodOld,goodPixels) then break
        endif

        start1=res  ; next iteration starts from results of present iteration
    endfor

    print,'FINISHED!!!!!!'
    return,pars[0:ndeg]
end
