function mmirs_read_trace_geometry, filterfile, grismfile
  readcol, filterfile,delim=string(9b),filter,wave1,wave2,format='A,F,F',count=nfilt,/silent
  readcol, grismfile,delim=string(9b),grism,wzero,wscale,wangle,spoff,format='A,F,F,F,F',count=ngrism,/silent
  filters=replicate({filt:'',wave1:0.0,wave2:0.0},nfilt)
  grisms=replicate({gris:'',wzero:0.0,wscale:0.0,wangle:0.0,spoff:0.0},ngrism)
  for i=0,nfilt-1 do begin
     filters[i].filt=filter[i]
     filters[i].wave1=wave1[i]
     filters[i].wave2=wave2[i]
  endfor
  for i=0,ngrism-1 do begin
     grisms[i].gris=grism[i]
     grisms[i].wzero=wzero[i]
     grisms[i].wscale=wscale[i]
     grisms[i].wangle=wangle[i]
     grisms[i].spoff=spoff[i]
  endfor
  trace_geometry={filters:filters,grisms:grisms}
  return, trace_geometry
end
