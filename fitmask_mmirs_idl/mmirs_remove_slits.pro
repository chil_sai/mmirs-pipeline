function mmirs_check_trace_overlap, cur_slit, mask_params, slits=slits,trace_geometry=trace_geometry
  ;if no trace_geometry structure passed, skip this test
  if not keyword_set(trace_geometry) then return, 1
  ;read wl range and tilt for requested filter/grism
  case strtrim(mask_params.grism,2) of
     'J': grism=trace_geometry.grisms[where(trace_geometry.grisms.gris eq 'J')]        
     'HK':grism=trace_geometry.grisms[where((strmid(trace_geometry.grisms.gris,0,2) eq 'HK') and (trace_geometry.grisms.gris ne 'HK2') and (trace_geometry.grisms.gris ne 'HK3'))]
     'H3000':grism=trace_geometry.grisms[where(trace_geometry.grisms.gris eq 'H3000')]
     'K3000':grism=trace_geometry.grisms[where(trace_geometry.grisms.gris eq 'K3000')]
     'H':grism=trace_geometry.grisms[where((trace_geometry.grisms.gris eq 'H') or (trace_geometry.grisms.gris eq 'H2'))]
  endcase
  filter=trace_geometry.filters[where(trace_geometry.filters.filt eq strtrim(mask_params.filter,2))]
;printf, -2, grism, filter
  ;pixmm, aka mm/pixel from xfitmask fitmask-opt.c
  pixmm=0.033198
  detcenterx=1012.              ;match instrument center in /data/mmti/etc/mmirs/mmirs.conf
  detcentery=1007.
  ;calculate polygons on chip for existing slits
  nslits=n_elements(slits)
  norder=n_elements(grism)
  npolygons=norder*(nslits+1);add cur_slit to the end of slits
  slits_clean=[slits,cur_slit]
  slits_poly=fltarr(npolygons,4,2)

  for i=0,nslits do begin
    for k=0,norder-1 do begin
     idx=i+k*nslits
     this_centerx=(slits_clean[i].bbox[0])/pixmm+detcenterx
     this_centery=(slits_clean[i].bbox[1])/pixmm+detcentery

     ;lowest wavelength of lowest x on slit, pos y side
     slits_poly[idx,0,0]=(slits_clean[i].bbox[0]-slits_clean[i].bbox[2]/2.0)/pixmm+detcenterx+$
        (filter.wave1-grism[k].wzero)/grism[k].wscale               
     slits_poly[idx,0,1]=(slits_clean[i].bbox[1]+slits_clean[i].bbox[3]/2.0)/pixmm+detcentery+$
        grism[k].spoff

     ;highest wavelength of highest x on slit, pos y side
     slits_poly[idx,1,0]=(slits_clean[i].bbox[0]+slits_clean[i].bbox[2]/2.0)/pixmm+detcenterx-$
        (filter.wave2-grism[k].wzero)/grism[k].wscale
     slits_poly[idx,1,1]=(slits_clean[i].bbox[1]+slits_clean[i].bbox[3]/2.0)/pixmm+detcentery+$
        grism[k].spoff
                         
     ;highest wavelength of highest x on slit, neg y side
     slits_poly[idx,2,0]=(slits_clean[i].bbox[0]+slits_clean[i].bbox[2]/2.0)/pixmm+detcenterx-$
        (filter.wave2-grism[k].wzero)/grism[k].wscale
     slits_poly[idx,2,1]=(slits_clean[i].bbox[1]-slits_clean[i].bbox[3]/2.0)/pixmm+detcentery+$
        grism[k].spoff
     
     ;lowest wavelength of lowest x on slit, neg y side
     slits_poly[idx,3,0]=(slits_clean[i].bbox[0]-slits_clean[i].bbox[2]/2.0)/pixmm+detcenterx+$
        (filter.wave1-grism[k].wzero)/grism[k].wscale
     slits_poly[idx,3,1]=(slits_clean[i].bbox[1]-slits_clean[i].bbox[3]/2.0)/pixmm+detcentery+$
        grism[k].spoff

     ;now rotate this polygon
     angle=grism[k].wangle/!radeg ;in radians
     slits_poly[idx,0,*]=[(cos(angle)*(slits_poly[idx,0,0]-this_centerx)-sin(angle)*(slits_poly[idx,0,1]-this_centery)+this_centerx),(sin(angle)*(slits_poly[idx,0,0]-this_centerx)+cos(angle)*(slits_poly[idx,0,1]-this_centery)+this_centery)]
     slits_poly[idx,1,*]=[(cos(angle)*(slits_poly[idx,1,0]-this_centerx)-sin(angle)*(slits_poly[idx,1,1]-this_centery)+this_centerx),(sin(angle)*(slits_poly[idx,1,0]-this_centerx)+cos(angle)*(slits_poly[idx,1,1]-this_centery)+this_centery)]
     slits_poly[idx,2,*]=[(cos(angle)*(slits_poly[idx,2,0]-this_centerx)-sin(angle)*(slits_poly[idx,2,1]-this_centery)+this_centerx),(sin(angle)*(slits_poly[idx,2,0]-this_centerx)+cos(angle)*(slits_poly[idx,2,1]-this_centery)+this_centery)]
     slits_poly[idx,3,*]=[(cos(angle)*(slits_poly[idx,3,0]-this_centerx)-sin(angle)*(slits_poly[idx,3,1]-this_centery)+this_centerx),(sin(angle)*(slits_poly[idx,3,0]-this_centerx)+cos(angle)*(slits_poly[idx,3,1]-this_centery)+this_centery)]
     
    endfor
  endfor
  ;split back out the cur_slit
  cur_poly=slits_poly[nslits*norder:*,*,*]
  slits_poly=slits_poly[0:nslits*norder-1,*,*]

  ;first verify that the cur_slit has at least one order on the chip
  onchip=where(mgh_pnpoly(reform(cur_poly[*,0,0]),reform(cur_poly[*,0,1]),[1,1,2048,2048],[1,2048,2048,1])$
           or mgh_pnpoly(reform(cur_poly[*,1,0]),reform(cur_poly[*,1,1]),[1,1,2048,2048],[1,2048,2048,1])$
           or mgh_pnpoly(reform(cur_poly[*,2,0]),reform(cur_poly[*,2,1]),[1,1,2048,2048],[1,2048,2048,1])$
           or mgh_pnpoly(reform(cur_poly[*,3,0]),reform(cur_poly[*,3,1]),[1,1,2048,2048],[1,2048,2048,1]),cnt)
  if cnt eq 0 then return, 0 else cur_poly=cur_poly[onchip,*,*]

  ;now remove and ignore any slit polygons that are completely off the chip
  onchip=where(mgh_pnpoly(reform(slits_poly[*,0,0]),reform(slits_poly[*,0,1]),[1,1,2048,2048],[1,2048,2048,1])$
            or mgh_pnpoly(reform(slits_poly[*,1,0]),reform(slits_poly[*,1,1]),[1,1,2048,2048],[1,2048,2048,1])$
            or mgh_pnpoly(reform(slits_poly[*,2,0]),reform(slits_poly[*,2,1]),[1,1,2048,2048],[1,2048,2048,1])$
            or mgh_pnpoly(reform(slits_poly[*,3,0]),reform(slits_poly[*,3,1]),[1,1,2048,2048],[1,2048,2048,1]))
  slits_poly=slits_poly[onchip,*,*]
  ;check if cur_slit polygon(s) overlap, return 1 if no overlap, 0 otherwise.
  ;remember that this slit should be tossed even if only its higher-order light crosses a slit
  for i=0, cnt-1 do begin
     overlap=where(mgh_pnpoly(reform(slits_poly[*,*,0]),reform(slits_poly[*,*,1]),reform(cur_poly[i,*,0]),reform(cur_poly[i,*,1])),ocnt)
     if ocnt ge 1 then return, 0
  endfor
  return, 1  
end

function mmirs_remove_slits_filter,slits,slits_rank_idx,slit_geometry,gapmm,$
    mask_params,trace_geometry=trace_geometry, nkeep=nkeep,nmax=nmax

n_slits=n_elements(slits)
if(n_elements(nmax) ne 1) then nmax=n_slits
if(n_elements(nkeep) ne 1) then nkeep=1l

slits_clean_idx = [slits_rank_idx[0:nkeep-1]]

for i=nkeep,nmax-1l do begin
    cur_slit = slits[slits_rank_idx[i]]
    r_edge1 = slits[slits_clean_idx].bbox[1]-slits[slits_clean_idx].bbox[3]/2.0 - (cur_slit.bbox[1]+cur_slit.bbox[3]/2.0)
    bad_r_edge1=where(slits[slits_clean_idx].bbox[1]-cur_slit.bbox[1] le 0, cbad_r_edge1)
    if(cbad_r_edge1 gt 0) then r_edge1[bad_r_edge1]=!values.f_nan
    l_edge1 = -(slits[slits_clean_idx].bbox[1]+slits[slits_clean_idx].bbox[3]/2.0 - (cur_slit.bbox[1]-cur_slit.bbox[3]/2.0))
    bad_l_edge1=where(slits[slits_clean_idx].bbox[1]-cur_slit.bbox[1] ge 0, cbad_l_edge1)
    if(cbad_l_edge1 gt 0) then l_edge1[bad_l_edge1]=!values.f_nan
    r_edge2 = slits[slits_clean_idx].bbox[1]-slits[slits_clean_idx].bbox[3]/2.0 - (cur_slit.bbox[1]-cur_slit.bbox[3]/2.0)
    bad_r_edge2=where(slits[slits_clean_idx].bbox[1]-cur_slit.bbox[1] le 0, cbad_r_edge2)
    if(cbad_r_edge2 gt 0) then r_edge2[bad_r_edge2]=!values.f_nan
    l_edge2 = -(slits[slits_clean_idx].bbox[1]+slits[slits_clean_idx].bbox[3]/2.0 - (cur_slit.bbox[1]+cur_slit.bbox[3]/2.0))
    bad_l_edge2=where(slits[slits_clean_idx].bbox[1]-cur_slit.bbox[1] ge 0, cbad_l_edge2)
    if(cbad_l_edge2 gt 0) then l_edge2[bad_l_edge2]=!values.f_nan
    tmp_slits=slits[slits_clean_idx]
    pass_overlap=mmirs_check_trace_overlap(cur_slit,mask_params,slits=tmp_slits,trace_geometry=trace_geometry)
    if((min(r_edge1,/nan) gt gapmm or finite(min(r_edge1,/nan)) ne 1) and $
       (min(l_edge1,/nan) gt gapmm or finite(min(l_edge1,/nan)) ne 1) and $
       (min(r_edge2,/nan) gt gapmm or finite(min(r_edge2,/nan)) ne 1) and $
       (min(l_edge2,/nan) gt gapmm or finite(min(l_edge2,/nan)) ne 1) and pass_overlap) then slits_clean_idx=[slits_clean_idx,slits_rank_idx[i]]
endfor

return,slits_clean_idx

end

function mmirs_remove_slits, slits_all, mask_params, slit_geometry,$
    box_geometry=box_geometry,trace_geometry=trace_geometry, $
    min_box_count=min_box_count,max_box_count=max_box_count,$
    verbose=verbose, optimize_type=optimize_type

if(n_elements(min_box_count) ne 1) then min_box_count=4
if(n_elements(max_box_count) ne 1) then max_box_count=4

ycent = (mask_params.corners[1]+mask_params.corners[3])/2d
xcent = (mask_params.corners[0]+mask_params.corners[2])/2d
gapmm=slit_geometry.gap*mask_params.arc2mm
;printf, -2, gapmm, slit_geometry.gap, mask_params.arc2mm, 'gap'

boxes=slits_all[where(slits_all.type eq 'BOX',cbox,compl=targ_idx,ncompl=ctarg)]
slits=slits_all[targ_idx]

boxes_rank_idx=reverse(sort((boxes.x-xcent)^2+(boxes.y-ycent)^2))
box_clean_idx=mmirs_remove_slits_filter(boxes,boxes_rank_idx,box_geometry,gapmm,mask_params,trace_geometry=trace_geometry,nmax=max_box_count)
cbox=n_elements(box_clean_idx)

if (not keyword_set(optimize_type)) then optimize_type=1
case optimize_type of
    1: slits_rank_idx = reverse(multisort(slits.priority,-abs(slits.x-xcent)))
    2: slits_rank_idx = reverse(multisort(slits.priority,-abs(slits.y-ycent)))
    3: slits_rank_idx = reverse(multisort(slits.priority,randomu(seed,n_elements(slits.priority))))
    else: slits_rank_idx = reverse(multisort(slits.priority,-abs(slits.x-xcent)))
endcase

slits=[boxes[box_clean_idx],slits]
slits_rank_idx=[lindgen(cbox),slits_rank_idx+cbox]
slits_clean_idx=mmirs_remove_slits_filter(slits,slits_rank_idx,slit_geometry,gapmm,mask_params,trace_geometry=trace_geometry,nkeep=cbox)

if(keyword_set(verbose)) then message,/inf,string(n_elements(slits_all)-n_elements(slits_clean_idx),form='(i4)')+' overlapping slits removed'

return,slits[slits_clean_idx[sort(slits_clean_idx)]]

end
