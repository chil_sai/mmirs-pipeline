pro find_peak,x_coord,y_val,thres,pix_peak,x_peak,y_peak,bgr_peak,n_peak,nmax=nmax
;
;+
; NAME:
;        FIND_PEAK
; PURPOSE:
;        Find emission line peaks in an irregularly sampled vector.
;        Based on the routine written by V.Afanasiev for the MPFS data reduction
; CATEGORY:
;        MMIRS data reduction pipeline
; CALLING SEQUENCE:
;        peak_find
; INPUTS:
;        x_coord:  x array 
;        y_val:    y array, must be same size as x_coord
;        thres:    threshold value for the peak search
; KEYWORD PARAMETERS:
;        no
; OUTPUTS:
;        pix_peak:  pixel location of peaks, vector
;        x_peak:    x location of peaks, vector
;        y_peak:    y location of peaks, vector
;        bgr_peak:  background at peak locations, vector
;        n_peak:    number of found peaks. -1 if nothing found
; COMMON BLOCKS:
;        None
; SIDE EFFECTS:
;        None.
; RESTRICTIONS:
;        None.
; PROCEDURE:
;        no
; MODIFICATION HISTORY:
;	Written by Victor Afanasiev, Special Astrophysical Observatory RAS, Jul 1999
;	Rewritten by Igor Chilingarian, Smithsonian Astrophysical Observatory, Jul 2012
;-

if(n_elements(nmax) ne 1) then nmax=500
x_peak=fltarr(nmax)
y_peak=fltarr(nmax)
bgr_peak=fltarr(nmax)
pix_peak=intarr(nmax)
asiz=size(x_coord)
v_size=asiz[1]
n_peak=0
w_size=7
nscan=10

;(w_size)>1.
mm=1 ;;; zoom factor
pix_cur=w_size

while (pix_cur lt v_size-w_size) and (pix_cur ge w_size) and (n_peak lt nmax) do begin
    idx1=(pix_cur-nscan)>0
    idx2=(pix_cur+nscan)<(v_size-1)
    ych=y_val[idx1:idx2]
    back2=min(ych)*1.01
    ymx=max(y_val[pix_cur-w_size:pix_cur+w_size])
    ytmp=y_val[pix_cur]-back2

    if (y_val[pix_cur] eq ymx) and (ytmp gt thres) then begin
;
; ....Have found a line peak
;
        pix_peak[n_peak]=pix_cur
        x_peak[n_peak]=x_coord[pix_cur]
        y_peak[n_peak]=y_val[pix_cur]
        bgr_peak[n_peak]=back2
        n_peak=n_peak+1
        pix_cur=pix_cur+mm*(w_size+1)
    endif else pix_cur=pix_cur+mm*1
endwhile

if (n_peak eq 0) then begin
   n_peak=-1
   x_peak=[0.]
   y_peak=[0.]
   bpk=[0.]
   pix_peak=[0]
endif else begin
   x_peak=x_peak[0:n_peak-1]
   y_peak=y_peak[0:n_peak-1]
   bgr_peak=bgr_peak[0:n_peak-1]
   pix_peak=pix_peak[0:n_peak-1]
endelse

return
end
