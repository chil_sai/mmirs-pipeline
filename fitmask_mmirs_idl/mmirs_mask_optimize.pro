function mmirs_mask_optimize,objlist,mask_params,field_geometry,$
    slit_geometry,box_geometry=box_geometry,min_box_count=min_box_count,max_box_count=max_box_count,$
    gs_geometry=gs_geometry,rotator_limits=rotator_limits,$
    posa_arr=posa_arr,ra_arr=ra_arr,dec_arr=dec_arr,trace_geometry=trace_geometry,$
    mask_params_out=mask_params_out,slits=slits,guidestars=guidestars,$
    merit_arr=merit_arr,uselst=uselst,gs_min=gs_min,verbose=verbose, optimize_type=optimize_type,obstype=obstype, extend_slits=extend_slits

if(n_elements(min_box_count) ne 1) then min_box_count=4
if(n_elements(max_box_count) ne 1) then max_box_count=4
if(n_elements(posa_arr) eq 0) then posa_arr=-179.0+findgen(360)
if(n_elements(ra_arr) eq 0) then ra_arr=[mask_params.ra]
if(n_elements(dec_arr) eq 0) then dec_arr=[mask_params.dec]
if(n_elements(rotator_limits) ne 2) then rotator_limits=[-180.0,164.0]  
if(n_elements(obstype) ne 1) then obstype='mos'

n_pa=n_elements(posa_arr)
n_ra=n_elements(ra_arr)
n_dec=n_elements(dec_arr)

merit_arr=fltarr(n_pa,n_ra,n_dec)

for x=0,n_ra-1 do begin
    for y=0,n_dec-1 do begin
        for p=0,n_pa-1 do begin
            mask_params_cur=mask_params
            mask_params_cur.ra=ra_arr[x]
            mask_params_cur.dec=dec_arr[y]
            mask_params_cur.posa=posa_arr[p]
            mmirs_expand_mask_params,mask_params_cur,rotator_range=rotator_range
            if(mmirs_check_rotator(rotator_range,rotator_limits) ne 1) then begin
                 merit_arr[p,x,y]=-1.0
                 if(keyword_set(verbose)) then message,/inf,'Forbidden rotator position: '+string(rotator_range)
                 continue
            endif
            if obstype ne 'imaging' then begin
                b=mmirs_sources_to_slits(objlist,mask_params_cur,field_geometry,$
                    slit_geometry,box_geometry=box_geometry,min_box_count=min_box_count,max_box_count=max_box_count,gs_geometry=gs_geometry, $
                    slits=slits,guidestars=guidestars,uselst=uselst,gs_min=gs_min,verbose=verbose)
                if(b lt 0) then begin
                    merit_arr[p,x,y]=-2.0
                    if(keyword_set(verbose)) then message,'No objects placed'
                    continue
                endif
                numa=n_elements(slits)
                mask_params_cur.corners=field_geometry.mask
                if numa ge 1 then slits_clean=mmirs_remove_slits(slits,mask_params_cur,slit_geometry,box_geometry=box_geometry,trace_geometry=trace_geometry,min_box_count=min_box_count,max_box_count=max_box_count,verbose=verbose, optimize_type=optimize_type) else slits_clean=!null
                mask_params_cur.corners=field_geometry.mask
                pri=total(slits_clean.priority)
                merit_arr[p,x,y]=pri
           endif else begin
                b=mmirs_guides_only(objlist,mask_params_cur,field_geometry,$
                    gs_geometry=gs_geometry, guidestars=guidestars,$
                    uselst=uselst,gs_min=gs_min,verbose=verbose)
                if(b lt 0) then continue
                merit_arr[p,x,y]=1.0
           endelse
        endfor
    endfor
endfor

m=max(merit_arr,idxm)
;printf,-2, n_elements(where(merit_arr eq 1.0))
;printf, -2, merit_arr
if(m le 0.0) then begin
    message,/inf,'No objects could be placed!'
    return,-1
endif
idx=array_indices(merit_arr,idxm)
if(n_elements(idx) ne 3) then idx=[idx,0l]
if(n_elements(idx) ne 3) then idx=[idx,0l]
mask_params_out=mask_params
mask_params_out.posa=posa_arr[idx[0]]
mask_params_out.ra=ra_arr[idx[1]]
mask_params_out.dec=dec_arr[idx[2]]
if obstype ne 'imaging' then begin
    b=mmirs_sources_to_slits(objlist,mask_params_out,field_geometry,$
        slit_geometry,box_geometry=box_geometry,min_box_count=min_box_count,max_box_count=max_box_count,gs_geometry=gs_geometry,$
        slits=slits,guidestars=guidestars, gs_min=gs_min)
    numa=n_elements(slits)
    if numa ge 1 then slits=mmirs_remove_slits(slits,mask_params_out,slit_geometry,box_geometry=box_geometry,trace_geometry=trace_geometry,min_box_count=min_box_count,max_box_count=max_box_count,optimize_type=optimize_type) else slits=!null
    m_val=total(slits_clean.priority)
    if keyword_set(extend_slits) then begin
        if slits ne !null then mmirs_extend_slits, slits, slit_geometry=slit_geometry, mask_params=mask_params
    endif
endif else begin
    b=mmirs_guides_only(objlist,mask_params_out,field_geometry,$
                    gs_geometry=gs_geometry, guidestars=guidestars,$
                    uselst=uselst,gs_min=gs_min,verbose=verbose)
    return, 1.0
endelse

;print,'Best parameters (R.A., Dec, PA):',mask_params_out.ra,mask_params_out.dec,mask_params_out.posa
;print,'Merit function:',m_val

return,m_val
end
