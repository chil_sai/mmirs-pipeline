function def_slit,logfile

log=readlog(logfile)

slit = sxpar(log,'SLIT',count=cntsl)
if(cntsl eq 0) then slit='mos'
slit = strlowcase(slit)

return, slit

end
