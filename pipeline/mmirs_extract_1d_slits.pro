function generate_extr_krnl,dithpos,e_apw,e_meth=e_meth,$
    ccdscl=ccdscl,c_krnl=c_krnl,n_krnl=n_krnl

if(n_elements(e_meth) ne 1) then e_meth=1
if(n_elements(ccdscl) ne 1) then ccdscl=0.2

n_dith=n_elements(dithpos)

c_krnl = fix(max(abs(dithpos))/ccdscl) + fix(e_apw*3)
n_krnl = 2*c_krnl+1

extr_krnl = dblarr(n_krnl)

for i=0,n_dith-1 do begin
    if(e_meth eq 2) then begin
        d = psf_gaussian(npix=[n_krnl],ndim=1,$
            centroid=[c_krnl+dithpos[i]/ccdscl],fwhm=[e_apw],/nor,/double)
    endif else begin
        d = dblarr(n_krnl)
        d[c_krnl-floor(e_apw/2.0):c_krnl+floor(e_apw/2.0)]=1.0
        d0=d ;;;/total(d)
        d=shift_s(d0,dithpos[i]/ccdscl)
    endelse
    extr_krnl[*] = extr_krnl[*] + d*((-1.0)^i)
endfor

return,extr_krnl
end


pro mmirs_extract_1d_slits,logfile,obstype,$
    dith_from_box=dith_from_box, box_exp=box_exp, $
    diffmode=diffmode, detect=detect, optimal=optimal, $
    estimate=estimate, fitprofile=fitprofile, fulltrace=fulltrace, nsegments=nsegments,$
    error=error,clean=clean,n_apwmax=n_apwmax,$
    suffix=suffix,writesuffix=writesuffix,debug=debug, split1d=split1d

log=readlog(logfile)
wdir=sxpar(log,'W_DIR')
if(n_elements(suffix) ne 1) then suffix='_lin'
if(n_elements(writesuffix) ne 1) then writesuffix=''

e_meth = sxpar(log,'EXTMETH')
if(keyword_set(optimal)) then e_meth=2 ;;;; forcing optimal extraction

e_apw = sxpar(log,'EXTAPW')

val=sxpar(log,'DITHPOS',count=cntval)
dithpos=(cntval eq 1)? double(strsplit(val,',',/extract)) : 0.0

if(keyword_set(diffmode)) then begin
    val2=sxpar(log,'DITHPOS2',count=cntval2)
    dithpos2=(cntval2 eq 1)? double(strsplit(val2,',',/extract)) : 0.0
    dithpos=[dithpos,dithpos2]
endif

ccdscl=0.2

mask=read_mask_mmirs_ms(wdir+'mask_mos.txt',logfile=logfile)
n_slits=n_elements(mask)

inpfile=wdir+obstype+'_slits'+suffix+'.fits'
varfile=(obstype eq 'sum_obj-sky')? wdir+'err_sum_obj-sky_slits'+suffix+'.fits' : inpfile
varfile=(obstype eq 'obj_diff-sky')? wdir+'err_obj_diff_slits'+suffix+'.fits' : varfile
errflag=0
if (strmid(obstype,0,11) eq 'sum_obj-sky') or (strmid(obstype,0,12) eq 'obj_diff-sky') then errflag=1

hpri=headfits(inpfile)
hpri_mod=hpri[0:n_elements(hpri)-2] ;remove the extra 'END'
delkw=['RA','RA ','DEC','EPOCH','PA','WCSNAME', 'CUNIT2', 'CTYPE1', 'CTYPE2', 'CRPIX1','CRPIX2','CRVAL1','CRVAL2','CD1_1','CD2_2','CD1_2','CD2_1','NAXIS','BITPIX','EXTNAME','EXTVER','INHERIT']
n_bkw=n_elements(delkw)
if(n_bkw gt 1) then for i=1,n_bkw-1 do sxdelpar,hpri_mod,delkw[i]

if(keyword_set(dith_from_box)) then begin
    if(n_elements(box_exp) ne 1) then box_exp='obj-sky'
    boxlist=where(mask.type eq 'BOX',cbox)
    for i=0,cbox-1 do begin
        t=mrdfits(wdir+box_exp+'_slits'+suffix+'.fits',boxlist[i]+1,hb,/silent)
        val=sxpar(hb,'DITHCOMP',count=cntval)
        dithpos_tmp=(cntval eq 1)? double(strsplit(val,',',/extract)) : 0.0
        if(i eq 0) then dithpos_arr=dblarr(n_elements(dithpos_tmp),cbox)
        dithpos_arr[*,i]=dithpos_tmp[*]/double(cbox)
    endfor
    if(keyword_set(diffmode)) then begin
        d_dithpos=dithpos[1]-dithpos[0]
    endif
    dithpos=(cbox gt 0)? total(dithpos_arr,2) : [0.0]
    if(keyword_set(diffmode)) then dithpos=[dithpos,dithpos+d_dithpos]
    print,'Using empirically estimated dithering positions: ',dithpos
endif

extr_krnl_ref=generate_extr_krnl(dithpos,e_apw,ccdscl=ccdscl,e_meth=e_meth,$
    c_krnl=c_krnl,n_krnl=n_krnl)*e_apw

outfile=wdir+obstype+'_slits_extr'+writesuffix+'.fits'
writefits,outfile,0,hpri
for i=0,n_slits-1 do begin
    print,'Extracting from slit #',i+1,'/',n_slits
    slit_img=mrdfits(inpfile,i+1,himg,/silent)
    var_img=mrdfits(varfile,i+1,hvar,/silent)
    if (i eq 0) then himg0=himg
    if(errflag eq 1) then var_img=double(var_img)^2
    nx=sxpar(himg,'NAXIS1')
    ny=sxpar(himg,'NAXIS2')
    parse_spechdr,himg,wl=wl
    if(i eq 0) then begin
        out_img=dblarr(nx,n_slits)
        err_img=dblarr(nx,n_slits)
        sky_img=dblarr(nx,n_slits)
    endif

    ;mask_entry=(i lt n_slits_a)? mask_a[i] : mask_b[i-n_slits_a]
    ;target_offset=(mask_entry.y-(mask_entry.bbox[5]+mask_entry.bbox[1])/2.0)*5.98802 ;; target offset in arcsec, 5.98802=scale in arcsec/mm
    ;print, (mask_entry.bbox[5]-mask_entry.bbox[1])*5.98802/ccdscl, ny
    extr_krnl_ref=generate_extr_krnl(dithpos,e_apw,ccdscl=ccdscl,e_meth=e_meth,$
        c_krnl=c_krnl,n_krnl=n_krnl)*e_apw ;+target_offset
    junk=''
    skipestimate=0
    baddetect=0
    obj_pos=dithpos ;+target_offset
    obj_fwhm=e_apw
    if(keyword_set(detect)) then begin
        ;calculate profile first, even if we use it just for detecting centroid
        extr_krnl_est=mmirs_estimate_extraction_profile(slit_img,wl*10.0,maskoh=1,fulltrace=fulltrace,nsegments=nsegments,debug=debug)
         sn_krnl_est=mmirs_estimate_extraction_profile(slit_img/sqrt(var_img),wl*10.0,maskoh=1,fulltrace=fulltrace,nsegments=nsegments,debug=debug,slit_lin_op=slit_lin_op_sn)
         ;resistant_mean,slit_lin_op,3.0,prof_cur,dim=1
         ;resistant_mean,sqrt(slit_lin_op_err),3.0,prof_err,dim=1
         resistant_mean,slit_lin_op_sn,3.0,prof_sn,prof_sn_mean,dim=1
         goodpix=where(finite(prof_sn),goodcnt)
         if goodcnt ge 10 then begin
            goodpix=goodpix[4:(goodcnt-5)]
            if keyword_set(diffmode) then peak_sig=(max(prof_sn[goodpix])-min(prof_sn[goodpix]))/2. else peak_sig=max(abs(prof_sn[goodpix]))-median(prof_sn[goodpix])
            ;peak_sig_err=max(abs(prof_sn[goodpix])/prof_sn_mean[goodpix])
            print, peak_sig, median(prof_sn_mean[goodpix])  ;, peak_sig_err
            if peak_sig lt 3.0 then begin
               skipestimate=1
               if peak_sig lt 0.5 then baddetect=1
            endif
         endif else  begin 
            skipestimate=1
            baddetect=1
         endelse        
         ;if goodcnt ge 10 then begin
         ;   print, max(prof_sn[goodpix])-median(prof_sn[goodpix])
         ;   splot, prof_sn[goodpix]
         ;   junk=get_kbrd()
         ;endif
        extr_krnl_est=reform(extr_krnl_est)
        ;obj_pos=sxpar(himg,'OBJPOS',count=cnt_obj)/ccdscl
        ;obj_fwhm=sxpar(himg,'OBJFWHM')/ccdscl
        ;if(cnt_obj eq 1) then print,'Object extracted in slit '+string(i+1),'  pos=',obj_pos,' fwhm=',obj_fwhm else begin
            obj_pos=dithpos ;+target_offset ;0.0 edit SM 06/25/18
            obj_fwhm=e_apw
        ;endelse
                                ;if detect=true but estimate=false use the
                                ;spatial profile to at least centroid
        if ~(baddetect) then begin
          xpeak=dindgen(n_elements(extr_krnl_est))
          smoothpro=smooth(extr_krnl_est,3)
          if keyword_set(diffmode) then find_peak, xpeak, abs(smoothpro), 0.0, ipix, peaks, ypeak, bgpeak, npeak else find_peak, xpeak, smoothpro, 0.005, ipix, peaks, ypeak, bgpeak, npeak
          if keyword_set(diffmode) then posnegtest = ((total(smoothpro[peaks] gt 0) eq npeak) or (total(smoothpro[peaks] lt 0) eq npeak)) else posnegtest=(total(smoothpro[peaks] lt 0) eq npeak)
          if posnegtest then baddetect=1
       endif else npeak=0 
           centerpix=(ny-1)/2
           
        if (npeak ge 1) and ~(baddetect) then begin
           ;limit to brightest pos/neg pair in diffmode, or the brightest single peak
            if keyword_set(diffmode) and (npeak gt 1) then begin
               peaks=peaks[(reverse(sort(ypeak)))[0:((npeak-1)<3)]]
               ;fix to make sure two adjacent peaks are chosen, even if a distant peak is technically 2nd brightest
               if n_elements(peaks) gt 2 then begin
                  match_peaks=peaks[1:*]
                  min=min(abs(abs(match_peaks-peaks[0])*ccdscl-abs(dithpos[1]-dithpos[0])),match)
                  peaks=[peaks[0], match_peaks[match]]
                endif
                ;what is the center of dithered pair
                detect_pos=(peaks[1]-peaks[0])/2.0+peaks[0] ;(peaks-n_elements(extr_krnl_est)/2.0)*ccdsc
                ;centerpix=detect_pos ;(ny-1)/2
                print, 'detect_pos: ', detect_pos, peaks[1], peaks[0]
            endif else begin
                ;centerpix=(ny-1)/2
                peaks=peaks[(reverse(sort(ypeak)))[0]]
                detect_pos=peaks[0]
                print, 'detect_pos: ', detect_pos
             endelse
            gcntrd_1d,abs(extr_krnl_est),peaks,xcen,obj_fwhm*2.0,/silent,/keepcenter
         ;read, junk                       ;centroid on these peaks to refine
            ;print, 'obj_pos centroid peak:', xcen
            ;match detect_pos to dithpos
            if keyword_set(diffmode) and (npeak le 1) then skipestimate=1
            if min(xcen) ne -1 then begin
              if npeak ge n_elements(dithpos) then obj_pos=(xcen-centerpix)*ccdscl
              print, 'obj_pos: ', obj_pos, 'dithpos: ', dithpos, 'centerpix: ', centerpix, 'xcen: ', xcen
              if keyword_set(diffmode) and (npeak gt 1) then if ( (obj_pos[1]-obj_pos[0])/(dithpos[1]-dithpos[0]) < 0.0) then obj_pos=reverse(obj_pos[0:1]) ;SM 01/25 trim to brightest 2 peaks before reversing order, and switch test for whether reversal needs to happen 
           endif else begin
               baddetect=1
               skipestimate=1
           endelse
            ;if the best peak is far away from expected position, ignore it
            ;if (abs(detect_pos[best]-obj_pos[0]) lt obj_fwhm) then begin
            ;    if (xcen ne -1) then $
            ;        obj_pos=(xcen-n_elements(extr_krnl_est)/2.0)*ccdscl $
            ;    else skipestimate=1
            ;endif else skipestimate=1 ;don't use estimate profile if no peak found in expected position

            print, 'final obj_pos:', (obj_pos/ccdscl+ny/2.0)
            if ~(skipestimate) then c_krnl=(ny-1)/2 ;obj_pos
           ; c_krnl=obj_pos
        endif else skipestimate=1
        if keyword_set(estimate) and ~(skipestimate) then begin
            extr_krnl=extr_krnl_est*obj_fwhm
            if keyword_set(n_apwmax) then begin
                llim=floor(min(obj_pos)/ccdscl-n_apwmax*obj_fwhm+centerpix)>0
                ulim=ceil(max(obj_pos)/ccdscl+n_apwmax*obj_fwhm+centerpix)<(n_elements(extr_krnl)-1)
                apmask=extr_krnl_est*0.0d
                apmask[llim:ulim]=1.0d
                ;zap the middle, too
                if keyword_set(diffmode) and (npeak gt 1) then begin
                  ;if (strmid(obstype,0,12) eq 'obj_diff-sky') then stop
                  llim2=floor(((min(obj_pos)/ccdscl+n_apwmax*obj_fwhm)<(detect_pos-centerpix))+centerpix)<(n_elements(extr_krnl)-1)
                  ulim2=ceil(((max(obj_pos)/ccdscl-n_apwmax*obj_fwhm)>(detect_pos-centerpix))+centerpix)>0
                  if llim2 lt ulim2 then apmask[llim2:ulim2]=0.0d
                endif
                ;stop
                extr_krnl=extr_krnl_est*apmask

                if(keyword_set(fitprofile)) then begin ;; fitting the profile with the Moffat function
                    if keyword_set(diffmode) and (npeak gt 1) then g_extr_krnl=llim+lindgen(llim2-llim+1l) else g_extr_krnl=llim+lindgen(ulim-llim+1l)
                    if n_elements(g_extr_krnl) ge 5 then ek_fit=mpfitpeak(double(g_extr_krnl),extr_krnl[g_extr_krnl],ek_c,/moffat,nterms=5) else begin
                       ek_fit=float(g_extr_krnl)*0.0
                       ek_c=fltarr(5)
                    endelse
                    if keyword_set(diffmode) and (npeak gt 1) then begin
                       g_extr_krnl2=ulim2+lindgen(ulim-ulim2+1l)
                       if n_elements(g_extr_krnl2) ge 5 then ek_fit2=mpfitpeak(double(g_extr_krnl2),extr_krnl[g_extr_krnl2],ek_c2,/moffat,nterms=5) else begin
                         ek_fit2=float(g_extr_krnl2)*0.0
                         ek_c2=fltarr(5)
                       endelse
                    endif
                    extr_krnl_fit=extr_krnl*0d ;;;ek_c[0]/(((dindgen(ny)-ek_c[1])/ek_c[2])^2+1d)^ek_c[3]
                    if keyword_set(diffmode) and (npeak gt 1) then begin
                       extr_krnl_fit[llim:llim2]=ek_fit-(((strmid(obstype,0,11) eq 'sum_obj-sky') and (ek_c[4] ge 0.01))?0.0:ek_c[4])
                       extr_krnl_fit[ulim2:ulim]=ek_fit2-(((strmid(obstype,0,11) eq 'sum_obj-sky') and (ek_c2[4] le -0.01))?0.0:ek_c2[4])
                       ;extr_krnl_fit[llim:llim2]=ek_fit-ek_c[4]
                       ;extr_krnl_fit[ulim2:ulim]=ek_fit2-ek_c2[4]
                       print, stdev(extr_krnl_fit-extr_krnl)
                       if (n_elements(g_extr_krnl) ge 5) and (n_elements(g_extr_krnl2) ge 5)  then extr_krnl=extr_krnl_fit
                    endif else begin
                       ;extr_krnl_fit[llim:ulim]=ek_fit-((strmid(obstype,0,11) eq 'sum_obj-sky')?0.0:ek_c[4])
                       extr_krnl_fit[llim:ulim]=ek_fit-ek_c[4]
                       print, stdev(extr_krnl_fit-extr_krnl)
                       if (n_elements(g_extr_krnl) ge 5) then extr_krnl=extr_krnl_fit
                    endelse
                 endif
                 ;stop
            endif
            ;adjust obj_fwhm for actual kernel
            ;halfmax=where(extr_krnl ge 0.5*max(extr_krnl),hmcnt)
            ;if hmcnt ge 2 then obj_fwhm=(max(halfmax)-min(halfmax))
        endif else $
            extr_krnl=(e_meth eq 2)? generate_extr_krnl([obj_pos],obj_fwhm,ccdscl=ccdscl,e_meth=e_meth,$
                                                        c_krnl=c_krnl,n_krnl=n_krnl)*e_apw : $
                                     generate_extr_krnl([obj_pos],e_apw,ccdscl=ccdscl,e_meth=e_meth,$
                                                        c_krnl=c_krnl,n_krnl=n_krnl)*e_apw
    endif

    if((size(extr_krnl))[0] le 1 and n_elements(extr_krnl) ne ny) then begin
        if(~keyword_set(detect)) then extr_krnl=extr_krnl_ref
        if baddetect then extr_krnl=extr_krnl_ref
        krnl_cur=dblarr(ny)
        c_k_c=(ny-1)/2
        if(ny ge n_krnl) then begin
            krnl_cur[c_k_c-c_krnl:c_k_c+c_krnl]=extr_krnl
        endif else begin
            krnl_cur[*]=extr_krnl[c_krnl-c_k_c:c_krnl+c_k_c]
        endelse
        extr_krnl=krnl_cur
    endif

    ;renormalize kernel
    extr_krnl=extr_krnl/total(abs(extr_krnl))
    ;if (strmid(obstype,0,12) eq 'obj_diff-sky') or (strmid(obstype,0,11) eq 'star_tel_01') then begin
    ;if (strmid(obstype,0,11) eq 'star_tel_01') then begin
    if keyword_set(debug) then begin
      if (strmid(obstype,0,11) eq 'sum_obj-sky') then begin
      ;if (strmid(obstype,0,12) eq 'obj_diff-sky') then begin
         splot, extr_krnl
         soplot, extr_krnl_est, color='red'
         if keyword_set(fitprofile) and ~(skipestimate) then soplot, extr_krnl_fit, color='green'
        junk=get_kbrd()
      endif
    endif
    spec_extr=mmirs_extract_lin_spec(slit_img,var2d=var_img,extr_krnl,sky=sky_extr,error=error_extr,sum_extraction=sum_extraction,clean=clean,absflux=absflux,debug=debug)
     ;read, junk
    ;if (strmid(obstype,0,11) eq 'sum_obj-sky') or (strmid(obstype,0,12) eq 'obj_diff-sky') then stop
    ;spec_extr=sum_extraction ;SM 01/19 temporarily use sum extraction instead of true optimal-weighted extraction
    out_img[*,i]=spec_extr
    err_img[*,i]=error_extr
    sky_img[*,i]=sky_extr
    ;add aperture keywords to header
    minmaxgood=minmax(where(finite(spec_extr)))
    mask_extr=(finite(spec_extr) eq 0)
    if(minmaxgood[0] eq -1) then minmaxgood=[0,0] ;else if minmaxgood[0] ne minmaxgood[1] then minmaxgood[0]=minmaxgood[0]+1 ;hack because 1st pixel often bad
;;;    sxaddpar, himg0, 'OBPOS'+strn(i+1,length=3,padchar='0'), obj_pos/ccdscl+ny/2.0, 'obj center pos in pixels'
    sxaddpar, himg0, 'OBPOS'+strn(i+1), obj_pos[0]/ccdscl+ny/2.0, 'obj center pos in pixels'
    sxaddpar, himg0, 'OFWHM'+strn(i+1), obj_fwhm, 'fwhm of extract profile'
;    sxaddpar, himg0, 'APNUM'+strn(i+1), strn(i+1)+' '+string(minmaxgood[0],format='(I4)')+' '+string(minmaxgood[1],format='(I4)')+' '+string(c_krnl-e_apw*(keyword_set(n_apwmax)?n_apwmax:3.0/2.35), format='(F7.2)')+' '+string(c_krnl+e_apw*(keyword_set(n_apwmax)?n_apwmax:3.0/2.35), format='(F7.2)')
;    sxaddpar, himg0, 'APID'+strn(i+1), strcompress(mask_entry.object,/remove)+' '+string(mask_entry.ra,format='(F11.6)')+' '+string(mask_entry.dec,format='(F11.6)')+' '+string(mask_entry.target, format='(I9)')+' '+string(mask_entry.slit,format='(I4)')+' '+mask_entry.side
;    sxaddpar, himg0, 'APSLT'+strn(i+1), string(mask_entry.x,format='(F11.6)')+' '+string(mask_entry.y,format='(F11.6)')+' '+string(mask_entry.height, format='(F7.3)')+' '+string(mask_entry.width, format='(F7.3)')+' '+string(mask_entry.theta, format='(F7.3)')+' '+string(sxpar(himg,'TILTEDSL'), format='(I1)')
    if keyword_set(split1d) then begin
       himg=[hpri_mod,himg]
      sxaddpar, himg, 'APNUM1', strn(i+1)+' '+string(minmaxgood[0],format='(I4)')+' '+string(minmaxgood[1],format='(I4)')+' '+string((obj_pos[0]/ccdscl+ny/2.0)-e_apw*(keyword_set(n_apwmax)?n_apwmax:3.0/2.35), format='(F7.2)')+' '+string((obj_pos[0]/ccdscl+ny/2.0)+e_apw*(keyword_set(n_apwmax)?n_apwmax:3.0/2.35), format='(F7.2)')
      sxaddpar, himg, 'APID1', strcompress(sxpar(himg,'OBJECT'),/remove)+' '+string(sxpar(himg,'SLITRA'),format='(F11.6)')+' '+string(sxpar(himg,'SLITDEC'),format='(F11.6)')+' '+string(sxpar(himg,'SLITTARG'), format='(I9)')+' '+string(sxpar(himg,'SLITID'),format='(I4)')
;      sxaddpar, himg, 'APSLT1', string(mask_entry.x,format='(F11.6)')+' '+string(mask_entry.y,format='(F11.6)')+' '+string(mask_entry.height, format='(F7.3)')+' '+string(mask_entry.width, format='(F7.3)')+' '+string(mask_entry.theta, format='(F8.3)')+' '+string(sxpar(himg,'TILTEDSL'), format='(I1)') 
      sxaddpar, himg, 'OBJPOS', (obj_pos[0]/ccdscl+ny/2.0), 'obj center pos in pixels'
      sxaddpar, himg, 'OBJFWHM', obj_fwhm, 'fwhm of extract profile'
      wave1d=wl[minmaxgood[0]:minmaxgood[1]]
      sxdelpar,himg,'NAXIS'
      sxdelpar,himg,'NAXIS1'
      sxdelpar,himg,'NAXIS2'
      sxdelpar,himg,'BITPIX'
      sxaddpar,himg,'BITPIX', -32, after='SIMPLE'
      sxaddpar,himg,'NAXIS', 2, after='BITPIX'
      sxaddpar,himg,'NAXIS1', minmaxgood[1]-minmaxgood[0]+1,after='NAXIS'
      sxaddpar,himg,'NAXIS2', 4,after='NAXIS1'
      sxdelpar, himg, 'EXTNAME'
      sxdelpar, himg, 'XTENSION'
      sxaddpar, himg, 'BANDID1', 'flux'
      sxaddpar, himg, 'BANDID2', 'error'
      sxaddpar, himg, 'BANDID3', 'sky'
      sxaddpar, himg, 'BANDID4', 'mask'
      ;trim each spectrum and wcs to only the valid wavelength range
      sxaddpar, himg, 'CRVAL1', wl[minmaxgood[0]]
      ;sxaddpar, himg, 'CDELT1', sxpar(himg,'CDELT1')*10.0
      ;sxaddpar, himg, 'CD1_1', sxpar(himg,'CD1_1')*10.0
      sxaddpar, himg, 'CD2_2', 1
      sxaddpar, himg, 'CUNIT1', 'nm'
      sxaddpar, himg, 'APID2', 'error'
      sxaddpar, himg, 'APID3', 'sky'
      sxaddpar, himg, 'APID4', 'mask'
      sxaddpar, himg, 'APNUM2', '2 0 1.00 1.00'
      sxaddpar, himg, 'APNUM3', '3 0 1.00 1.00'
      sxaddpar, himg, 'APNUM4', '4 0 1.00 1.00'
      sxaddpar, himg, 'WAT0_001', 'system=equispec'
;      if strmid(mask_entry.mask_name,0,4) eq 'Long' then sxaddpar, himg, 'OBJECT', strcompress(sxpar(himg,'CATID'),/remove)+'_'+mask_entry.side else sxaddpar, himg, 'OBJECT', strcompress(sxpar(himg,'SLITOBJ'),/remove)+'_'+strcompress(string(sxpar(himg,'SLITTARG')),/remove)
;      sxaddpar, himg, 'RA', strcompress(sxpar(himg,'SLITRA'),/remove)
;      sxaddpar, himg, 'DEC', strcompress(sxpar(himg,'SLITDEC'),/remove)
      if n_slits eq 1 then begin 
        ;sxaddpar, himg, 'OBJECT', strcompress(sxpar(himg,'CAT-ID'),/remove)
        sxaddpar, himg, 'RA', strcompress(sxpar(himg,'CAT-RA'),/remove)
        sxaddpar, himg, 'DEC', strcompress(sxpar(himg,'CAT-DEC'),/remove)
        outfile1D=wdir+obstype+'_1D/'+strcompress(sxpar(himg,'OBJECT'),/remove)+writesuffix+'.fits'
      endif else begin
        sxaddpar, himg, 'OBJECT', mask[i].object
        sxaddpar, himg, 'RA', mask[i].dec
        sxaddpar, himg, 'DEC', mask[i].dec
        outfile1D=wdir+obstype+'_1D/'+strcompress(sxpar(himg,'OBJECT'),/remove)+'_'+strcompress(string(mask[i].slit,format='(i03)'),/remove)+writesuffix+'.fits'

      endelse
;      sxaddpar, himg, 'COMMENT', 'APNUM: #, spec start pixel, end pixel, min extract coord, max extr coord'
;      sxaddpar, himg, 'COMMENT', 'APID: Targ #, RA, DEC, Object, Slit#, Side'
;      sxaddpar, himg, 'COMMENT', 'APSLT: Slit X, Slit Y (mm), Height, Width, Angle, Tilted?(0/1)'
      if(NOT file_test(wdir+obstype+'_1D',/directory)) then file_mkdir, wdir+obstype+'_1D'
;      if strmid(mask_entry.mask_name,0,4) eq 'Long' then outfile1D=wdir+obstype+'_1D/'+strcompress(sxpar(himg,'CATID'),/remove)+'_'+mask_entry.side+writesuffix+'.fits'  else outfile1D=wdir+obstype+'_1D/slit'+mask_entry.side+strcompress(string(mask_entry.slit,format='(i03)'),/remove)+'_'+strcompress(mask_entry.object,/remove)+'t'+strcompress(string(mask_entry.target),/remove)+writesuffix+'.fits'
      mwrfits, float([[spec_extr[minmaxgood[0]:minmaxgood[1]]],[error_extr[minmaxgood[0]:minmaxgood[1]]],[sky_extr[minmaxgood[0]:minmaxgood[1]]],[mask_extr[minmaxgood[0]:minmaxgood[1]]]]), outfile1D, himg, /create
    endif
;    goodk = where(abs(krnl_cur) gt 1e-4)
;
;    for x=0,nx-1 do begin
;        out_img[x,i]=(keyword_set(error))? $
;            sqrt(total((transpose(slit_img[x,goodk])*krnl_cur[goodk])^2,nan=diffmode)) : $
;            total(transpose(slit_img[x,goodk])*krnl_cur[goodk],nan=diffmode)
;    endfor

 endfor
himg=himg0
;sxaddpar, himg, 'COMMENT', 'APNUM: #, spec start pixel, spec end pixel,...'
;sxaddpar, himg, 'COMMENT', '  min extract coord, max extract coord'
;sxaddpar, himg, 'COMMENT', 'APID: Targ #, RA, DEC, Object, Slit#, Side'
;sxaddpar, himg, 'COMMENT', 'APSLT: Slit X, Slit Y (mm), Height, Width, Angle, Tilted?(0/1)'
sxdelpar,himg,'YOFFSET'
sxaddpar,himg,'DITHPOS',sxpar(log,'DITHPOS')
if(n_slits eq 1) then sxdelpar,himg,'NAXIS2'

;delkw=['TILTEDSL', 'AIRMASS', 'RA', 'DEC', 'EQUINOX','EPOCH','RADECSYS','SIDE']

;for i=0,n_elements(himg)-1 do begin
;    if(strmid(himg[i],0,4) eq 'SLIT') then delkw=[delkw,strcompress(strmid(himg[i],0,8),/remove)]
;endfor
;n_bkw=n_elements(delkw)
;if(n_bkw gt 1) then for i=1,n_bkw-1 do sxdelpar,himg,delkw[i]

sxaddpar,himg,'EXTNAME','FLUX'
mwrfits,out_img,outfile,himg
sxaddpar,himg,'EXTNAME','ERROR'
mwrfits,err_img,outfile,himg
sxaddpar,himg,'EXTNAME','SKY'
mwrfits,sky_img,outfile,himg
;mwrfits,mask_a,outfile
;mwrfits,mask_b,outfile

end
