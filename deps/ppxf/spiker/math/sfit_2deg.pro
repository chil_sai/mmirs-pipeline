function sfit_2deg, z, err=uerr0, xgrid=xgrid, ygrid=ygrid, $
    degree1, degree2, KX=kx_f, $
    IRREGULAR=irreg_p, MAX_DEGREE=max_p, zero_coeff=zero_coeff, chi2=chi2, $
    la_lsq_method=la_lsq_method
;+
; NAME:
;	SFIT_2DEG
;
; PURPOSE:
;	This function determines a polynomial fit to a surface sampled
;	  over a regular or irregular grid.
;
; CATEGORY:
;	Curve and surface fitting.
;
; CALLING SEQUENCE:
;	Result = SFIT_2DEG(Data, Degree1, Degree2)    ;Regular input grid
;	Result = SFIT_2DEG(Data, Degree1, Degree2, /IRREGULAR)  ;Irregular input grid
;
; INPUTS:
; 	Data:	The array of data to fit. If IRREGULAR
; 	is not set, the data are assumed to be sampled over a regular 2D
; 	grid, and should be in an Ncolumns by Nrows array.  In this case, the
; 	column and row subscripts implicitly contain the X and Y
; 	location of the point.  The sizes of the dimensions may be unequal.
;	If IRREGULAR is set, Data is a [3,n] array containing the X,
;	Y, and Z location of each point sampled on the surface.  
;
;	Degree1:  The maximum degree of fit in X.
;	Degree2:  The maximum degree of fit in Y.
;
; KEYWORDS:
;       ERR: uncertainties of measurements (assumed to be Gaussian).
;         If set, should be the same dimensionality as the measurement, i.e.
;         [n] array (1D) if "IRREGULAR" is set. 
;         Defaults to a uniform vector of 1
; 	IRREGULAR: If set, Data is [3,n] array, containing the X, Y,
; 	  and Z locations of n points sampled on the surface.  See
; 	  description above.
; 	MAX_DEGREE: If set, the Degree parameter represents the
; 	    maximum degree of the fitting polynomial of all dimensions
; 	    combined, rather than the maximum degree of the polynomial
; 	    in a single variable. For example, if Degree is 2, and
; 	    MAX_DEGREE is not set, then the terms returned will be
; 	    [[K, y, y^2], [x, xy, xy^2], [x^2, x^2 y, x^2 y^2]].
; 	    If MAX_DEGREE is set, the terms returned will be in a
; 	    vector, [K, y, y^2, x, xy, x^2], in which no term has a
; 	    power higher than two in X and Y combined, and the powers
; 	    of Y vary the fastest. 
;       ZERO_COEFF: an optional [Degree1+1, Degree2+1] array specifying, 
;           which coefficients of a polynomial surface to include in the
;           fit. "0" (default) includes a given term in the fit, "1"
;           excludes it. For example, specyfing zero_coeff[1:*,1:*]=1 will
;           disable fitting all cross-terms between X and Y
;       LA_LSQ_METHOD: method for LA_LEAST_SQUARES
;
;
; OUTPUT:
;	This function returns the fitted array.  If IRREGULAR is not
;	set, the dimensions of the result are the same as the
;	dimensions of the Data input parameter, and contain the
;	calculated fit at the grid points.  If IRREGULAR is set, the
;	result contains n points, and contains the value of the
;	fitting polynomial at the sample points.
;
; OUTPUT KEYWORDS:
;	Kx:	The array of coefficients for a polynomial function
;		of x and y to fit data. If MAX_DEGREE is not set, this
;		parameter is returned as a (Degree+1) by (Degree+1)
;		element array.  If MAX_DEGREE is set, this parameter
;		is returned as a (Degree+1) * (Degree+2)/2 element
;		vector. 
;       Chi2:   goodness-of-fit chi^2 value (scalar)
;
; PROCEDURE:
; 	Fit a 2D array Z as a polynomial function of x and y.
; 	The function fitted is:
;  	    F(x,y) = Sum over i and j of kx[j,i] * x^i * y^j
; 	where kx is returned as a keyword.  If the keyword MAX_DEGREE
; 	is set, kx is a vector, and the total of the X and Y powers will
; 	not exceed DEGREE, with the Y powers varying the fastest.
;
; MODIFICATION HISTORY:
;	July, 1993, DMS		Initial creation
;	July, 2001		Added MAX_DEGREE and IRREGULAR keywords.
;       April, 2009, Igor Chilingarian: Re-written, added Degree2
;       December, 2010, Igor Chilingarian: Added filtering of NaNs
;       July, 2016, Igor Chilingarian: Switched to LA_LEAST_SQUARES
;       Apr, 2020, Igor Chilingarian: Added the LA_LSQ_METHOD keyword
;
;-

   on_error, 2

   s = size(z)
   irreg = keyword_set(irreg_p)
   max_deg = keyword_set(max_p)
   if(n_params() eq 2) then degree2=degree1
   n2 = max_deg ? ((degree1 < degree2)+1) * (2*(degree1 > degree2)+2-(degree1 < degree2)) / 2 : (degree1+1)*(degree2+1) ;# of coeff to solve
   if(n_elements(zero_coeff) ne n2) then begin
       if(n_elements(zero_coeff) ne 0) then message,/inf,'zero_coeff has wrong size'
       zero_coeff=bytarr(n2)
   endif
   good_coeff=where(zero_coeff eq 0, n2_red)

   if irreg then begin
       if (s[0] ne 2) or (s[1] ne 3) then $
         message, 'For IRREGULAR grids, input must be [3,n]'
       good_pnts=where(finite(total(z,1)) eq 1, cgood_pnts,ncompl=cbad_pnts)
       m0 = n_elements(z) / 3    ;# of points
       m = cgood_pnts
       x = double(z[0,good_pnts])       ;Do it in double...
       y = double(z[1,good_pnts])
       zz = double(z[2,good_pnts])
   endif else begin             ;Regular
       if s[0] ne 2 then message, 'For regular grids, input must be [nx, ny]'
       nx = s[1]
       ny = s[2]
       m0 = nx * ny		;# of points to fit
       if(n_elements(xgrid) ne nx) then xgrid=dindgen(nx)
       x = xgrid # replicate(1d, ny) ;X values at each point
       if(n_elements(ygrid) ne ny) then ygrid=dindgen(ny)
       y = replicate(1d,nx) # ygrid
       good_pnts=where(finite(z) eq 1, cgood_pnts,ncompl=cbad_pnts)
       m = cgood_pnts
       x = x[good_pnts]
       y = y[good_pnts]
       zz = z[good_pnts]
   endelse
   if(n_elements(uerr0) ne m0) then $
      uerr=transpose(dblarr(m))+1d $
   else $
      uerr=uerr0[good_pnts]

   if n2_red gt m then message, 'Fitting degree of '+strtrim(degree1,2)+$
     ' requires ' + strtrim(n2_red,2) + ' points.'
   ut = dblarr(n2_red, m, /nozero)
   k = 0L
   k_f = 0L
   for i=0, degree1 do begin
       for j=0,degree2 do begin ;Fill each column of basis
           if max_deg and (i+j gt (degree1>degree2)) then continue
           k_f = k_f + 1L
           if ((reform(zero_coeff))[k_f-1L] ne 0) then continue
           ut[k, 0] = reform(x^i * y^j, 1, m)/transpose(uerr)
           k = k + 1L
       endfor
   endfor

   kx = bounded_least_squares(ut,reform(zz,m)/reform(uerr,m),/double,resid=chi2) ;; la_least_squares(ut,reform(zz,m)/reform(uerr,m),/double,resid=chi2,method=la_lsq_method)
   kx_f = dblarr(n2)
   kx_f[good_coeff] = kx
   if max_deg eq 0 then kx_f = reform(kx_f, degree2+1, degree1+1)

   result = dblarr(m0)
   result[good_pnts] = reform(reform(kx_f[good_coeff],n2_red) # ut, m)*uerr
   return, irreg ? result : reform(result, nx, ny)
end
