;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;  Cosmic ray and warm pixel rejection
;  Works only for difference spectra
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

pro crhp_reject_ms,logfile,_Extra=extra_kw

log=readlog(logfile)
wdir=def_wdir(logfile)

sci1=sxpar(log,'SCI')
sci2=sxpar(log,'SCI2',count=cntsci2)

if(cntsci2 ne 1) then begin
    message,/inf,'Cosmic ray rejection works only for difference spectra. Exiting'
    return
endif

mask=read_mask_mmirs_ms(wdir+'mask_mos.txt',logfile=logfile)
n_slits=n_elements(mask)

;; extracting 2D slits from the paired image
extract_2d_slits,logfile,sci2

f_sp1=wdir+'obj_slits.fits'
f_sp2=wdir+sci2+'_slits.fits'
f_diff=wdir+'obj_diff-sky_slits.fits'
f_nflat=wdir+'flatn_slits.fits'
f_out_mask=wdir+'obj_diff-sky_bpmask_slits.fits'

period=get_mmirs_period(f_sp1, year=year, exten=0)
detector_id = (period eq '2014B' or year gt 2014)? 'H2RG' : 'H2_56'

h1=headfits(f_sp1)
writefits,f_out_mask,0,h1
exptime=sxpar(h1,'EXPTIME')

if(detector_id eq 'H2RG') then begin
    rdnoise=12.0
    gain=0.1
endif else begin
    rdnoise=12.0 ;;; 15 in the fits header
    gain=2.1 ;;; 5.0 in the fits header
endelse

for i=1,n_slits do begin
    print,'Processing slit #',i
    sp1=mrdfits(f_sp1,i,hsp1,/silent)
    sp2=mrdfits(f_sp2,i,hsp2,/silent)
    nflat=mrdfits(f_nflat,i,/silent)
    spdiff=mrdfits(f_diff,i,hdiff,/silent)

    noise=sqrt(djs_median(sp1+sp2,width=5,boundary='reflect')*exptime*gain*nflat+2*rdnoise^2)/(exptime*gain*nflat)

    la_cosmic_array,spdiff,noise=noise,/diff,gain=gain,readn=rdnoise,maskarr=out_mask,_Extra=extra_kw

    mwrfits,out_mask,f_out_mask,hdiff
endfor

end
