Telescope input angle to mm polynomial

set telescope_poly      { 0.0 609.3600 0.0000 64.7463 0.0000 29.2709 }
set telescope_invs      { 0.0
                          0.00164111178535851
                         -1.88696443420653e-09   
                         -4.43658409712805e-10   
                         -1.5192193524748e-13   
                          1.82202225565953e-16 }


Mask position in mm to pixel position on chip:

double  pixmm           = -0.033198;

  pixels = mm / pixmm + 1024


Grism table:

Grism   Wzero           Wscale
-----   -----           -----
HK      2.54638         -0.00067289
J       1.51185         -0.0002531
H       2.09109         -0.00035136
HK2     1.31439         -0.00035722
HK3     0.884125        -0.00024323 


pixel offset  = wave - Wzero / Wscale


Wave length ranges for MMIRS filters:

Filter  Wave1   Wave2
------  -----   -----
J       1.17    1.33
H       1.50    1.79
Y       0.967   1.072
K       2.00    2.32
HK      1.25    2.49
HH      1.5055  1.7653

