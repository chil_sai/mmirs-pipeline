function poly2d,x,y,c0,deg1=deg1,deg2=deg2,irregular=irregular

c=c0
s=size(c)
if(s[0] eq 2) then begin
    deg1a=s[2]-1L
    deg2a=s[1]-1L
    c1=transpose(c)
endif else begin
    if((n_elements(deg1) ne 1) and (n_elements(deg2) ne 1)) then deg2a=3 ;;;; default, horribly wrong :)
    if(n_elements(deg2) eq 1) then deg2a=deg2
    deg1a = ((deg2a+2.0)*(float(deg2a+1)/2.0) lt n_elements(c))? n_elements(c)/(float(deg2) + 1) + float(deg2)/2 - 1 : (float(deg2)+1.0/2.0)-sqrt((float(deg2)+1/2.0)^2+2.0*(deg2+1-n_elements(c)))
    c1=dblarr(deg1a+1,deg2a+1)
;    print,'Init d1',n_elements(c),float(deg1),float(deg2)
;    print,'Init: d1,d2=',deg1,deg2,deg1a,deg2a,size(c1)
    ii=0L
    for i=0, deg1a do begin
        for j=0, deg2a do begin
            if(i+j gt (deg1a>deg2a)) then continue
            c1[i,j]=c[ii]
            ii=ii+1L
        endfor
    endfor
endelse

nx=n_elements(x)
ny=n_elements(y)
if(keyword_set(irregular) and (nx ne ny)) then message,'X and Y should have equal number of elements when IRREGULAR is set'

if(keyword_set(irregular)) then begin
    x_crd=x
    y_crd=y
endif else begin
    x_crd=reform(transpose(congrid(transpose([x]),ny,nx)),nx*ny)
    y_crd=reform(congrid(transpose(y),nx,ny),nx*ny)
endelse

res = (keyword_set(irregular))? dblarr(nx) : dblarr(nx*ny)
;print,size(res),nx,ny
;print,c1
;print,'deg1a,deg2a=',deg1a,deg2a
;print,'size(c1)=',size(c1)
;print,float(c1)
for i=0, deg1a do begin
    for j=0, deg2a do begin
        res = res+x_crd^i*y_crd^j*c1[i,j]
    endfor
endfor

if(not keyword_set(irregular)) then res=reform(res,nx,ny)
return,res
end
