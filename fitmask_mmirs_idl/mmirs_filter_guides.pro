;subroutine to mmirs_sources_to_slits to compute valid guide/wfs
;stars for the mask
function mmirs_remove_guides, guidestars, gs_boxh,gs_boxw,gs_box_sep, wfs_sep, verbose=verbose, ng_limit=ng_limit
  if not keyword_set(ng_limit) then ng_limit=3
  wfs=guidestars[where(guidestars.wfs ge 10,n_wfs,/null)]
  unassigned=guidestars[where(guidestars.wfs lt -80,/null)]
  guides=guidestars[where((guidestars.wfs ge 1) and (guidestars.wfs ne 10) and (guidestars.wfs ne 20), n_guide,/null)]
  if n_guide eq 0 then return, guidestars ;nothing to be done
  printf, -2, 'n_wfs_start:', n_wfs
  ;FIRST CLEAN WFS
  if n_wfs ge 1 then begin
    wfs_rank_idx = multisort(wfs.mag)
    wfs_clean_idx = !null
    while n_elements(wfs_rank_idx) gt 0 do begin
     cur_wfs = wfs[wfs_rank_idx[0]]
     if n_elements(wfs_rank_idx) eq 1 then begin
        others=!null
        ;just check against unassigned
        if unassigned eq !null then bcnt2=0 else ounassigned=where((sqrt((cur_wfs.x-unassigned.x)^2.0+(cur_wfs.y-unassigned.y)^2.0) lt wfs_sep) and ((unassigned.mag-cur_wfs.mag) lt 1.6) OR (sqrt((cur_wfs.x-unassigned.x)^2.0+(cur_wfs.y-unassigned.y)^2.0) lt 0.8*wfs_sep and ((unassigned.mag-cur_wfs.mag) lt 3.5)),bcnt2,/null)
        if bcnt2 eq 0 then wfs_clean_idx=[wfs_clean_idx,wfs_rank_idx[0]] else begin
           cur_wfs.wfs=-99
           unassigned=[unassigned,cur_wfs]
        endelse
        wfs_rank_idx=others
        continue
     endif else others=wfs_rank_idx[1:*]
        overlaps=where((sqrt((cur_wfs.x-wfs[others].x)^2.0+(cur_wfs.y-wfs[others].y)^2.0) lt wfs_sep) and ((wfs[others].mag-cur_wfs.mag) lt 1.6) OR (sqrt((cur_wfs.x-wfs[others].x)^2.0+(cur_wfs.y-wfs[others].y)^2.0) lt 0.8*wfs_sep and ((wfs[others].mag-cur_wfs.mag) lt 3.5)),bcnt,complement=nonoverlaps,/null)
                                ;also check if there are any
                                ;unassigned stars that are bright
                                ;enough to interfere
        if unassigned eq !null then bcnt2=0 else ounassigned=where((sqrt((cur_wfs.x-unassigned.x)^2.0+(cur_wfs.y-unassigned.y)^2.0) lt wfs_sep) and ((unassigned.mag-cur_wfs.mag) lt 1.6) OR (sqrt((cur_wfs.x-unassigned.x)^2.0+(cur_wfs.y-unassigned.y)^2.0) lt 0.8*wfs_sep and ((unassigned.mag-cur_wfs.mag) lt 3.5)),bcnt2,/null)
        ;printf,-2,bcnt,bcnt2
        if (bcnt eq 0) and (bcnt2 eq 0) then wfs_clean_idx=[wfs_clean_idx,wfs_rank_idx[0]] else begin
           ;printf, -2, sqrt((cur_wfs.x-wfs[others].x)^2.0+(cur_wfs.y-wfs[others].y)^2.0)/.167
           cur_wfs.wfs=-99
           unassigned=[unassigned,cur_wfs]
        endelse
        ;wfs[others[overlaps]].wfs=-99
        ;unassigned=[unassigned,guides[others[overlaps]]] ;need to add these for later checking to be complete
        others=others[nonoverlaps]
        wfs_rank_idx=others
     endwhile
     wfs=wfs[wfs_clean_idx]
 endif                           ;end wfs clean
  printf, -2, 'n_wfs_end:', n_elements(wfs)
  if n_elements(wfs) eq 0 then return, [guides,unassigned]
  
  ;NOW CLEAN THE GUIDE STARS
  guide_rank_idx = multisort(guides.wfs,guides.mag)
  guide_clean_idx = !null

;loop until we've consumed all items in guide_rank_idx
  while n_elements(guide_rank_idx) gt 0 do begin
     cur_guide = guides[guide_rank_idx[0]]
     if n_elements(guide_rank_idx) eq 1 then begin
        others=!null
        ;just check against unassigned
        if unassigned eq !null then bnct2=0 else bounassigned=where((abs(cur_guide.x-unassigned.x) lt (gs_boxw/2.0)) and (abs(cur_guide.y-unassigned.y) lt (gs_boxh/2.0+gs_boxh/3.)) and ((unassigned.mag-cur_guide.mag) lt 3.0),bcnt2,/null)
        if (bcnt2 eq 0) then guide_clean_idx=[guide_clean_idx,guide_rank_idx[0]] else begin
           cur_guide.wfs=-99
           unassigned=[unassigned,cur_guide]
        endelse
        guide_rank_idx=others
        continue
     endif else others=guide_rank_idx[1:*]
     overlaps=where((abs(cur_guide.x-guides[others].x) lt (gs_box_sep/2.+gs_boxw/2.)) and (abs(cur_guide.y-guides[others].y) lt (gs_box_sep/2.+gs_boxh/2.)),ocnt,complement=nonoverlaps,/null)
     ;printf, -2, 'ocount',ocnt
     if ocnt gt 0 then begin
        ;we've found some overlaps. If any of these are within the
        ;primary guide's box, plus a width/3 buffer for dithering, and within 3mags, then we need to discard *both*
        badoverlaps=where((abs(cur_guide.x-guides[others].x) lt (gs_boxw/2.0)) and (abs(cur_guide.y-guides[others].y) lt (gs_boxh/2.0+gs_boxh/3.0)) and ((guides[others].mag-cur_guide.mag) lt 3.0),bcnt,/null)
        ;also check if there are any unassigned stars that will fall into this guide box
        if unassigned eq !null then bnct2=0 else bounassigned=where((abs(cur_guide.x-unassigned.x) lt (gs_boxw/2.0)) and (abs(cur_guide.y-unassigned.y) lt (gs_boxh/2.0+gs_boxh/3.0)) and ((unassigned.mag-cur_guide.mag) lt 3.0),bcnt2,/null)
        ;printf,-2,bcnt,bcnt2
        if (bcnt eq 0) and (bcnt2 eq 0) then guide_clean_idx=[guide_clean_idx,guide_rank_idx[0]] else begin
           cur_guide.wfs=-99
           unassigned=[unassigned,cur_guide]
        endelse
        guides[others[overlaps]].wfs=-99
        unassigned=[unassigned,guides[others[overlaps]]] ;need to add these for later checking to be complete
        others=others[nonoverlaps]
        guide_rank_idx=others
     endif else begin
        guide_clean_idx=[guide_clean_idx,guide_rank_idx[0]]
        guide_rank_idx=others
     endelse
  endwhile

if(keyword_set(verbose)) then message,/inf,string(n_guide-n_elements(guide_clean_idx),form='(i4)')+' overlapping guides removed'
;finally, only pick up to 3 brightest guide in each quadrant, ignore
;wfs=5 through 8, as they are already limited to 1
guides=guides[guide_clean_idx]
if n_elements(guides) gt 0 then begin
  for i=1,4 do begin
   this=where(guides.wfs eq i,cnt,/null)
   if cnt gt ng_limit then begin
      magsort=sort(guides[this].mag)
      ;printf,-2, guides[this].mag, guides[this[magsort[0:2]]].mag
      guides[this[magsort[ng_limit:*]]].wfs=-99
   endif
  endfor
endif
guidestars=[guides,wfs,unassigned]

return, guidestars

end

function mmirs_filter_guides,guidestars, mask_params,gs_geometry,field_geometry, maglimits, gs_min, valid=valid, verbose=verbose, nofilter=nofilter

if tag_exist(maglimits, 'ng_limit') then ng_limit=maglimits.ng_limit else ng_limit=3
valid=1
id_guide_list=!NULL
;gs_boxh=mask_params.arc2mm*gs_geometry.height
;gs_boxw=mask_params.arc2mm*gs_geometry.width
gs_sep=mask_params.arc2mm*3. ; keep valid guide stars 3asec apart 
wfs_sep=mask_params.arc2mm*30. ;keep valid wfs stars 30asec apart

;printf, -2, guidestars.x, guidestars.y
id_guide1=where(mgh_pnpoly(guidestars.x,guidestars.y,reform(field_geometry.guide1[0,*]),reform(field_geometry.guide1[1,*])) and guidestars.mag lt maglimits.guide_mag_lim and guidestars.mag gt maglimits.guide_bright_lim,g1cnt)
;printf,-2,size(mgh_pnpoly(guidestars.x,guidestars.y,field_geometry.guide1[0,*],field_geometry.guide1[1,*]))
;printf,-2,n_elements(guidestars.x)
guidestars[id_guide1].wfs=1
id_guide2=where(mgh_pnpoly(guidestars.x,guidestars.y,reform(field_geometry.guide2[0,*]),reform(field_geometry.guide2[1,*])) and guidestars.mag lt maglimits.guide_mag_lim and guidestars.mag gt maglimits.guide_bright_lim,g2cnt)
guidestars[id_guide2].wfs=2
id_wfs1=where(mgh_pnpoly(guidestars.x,guidestars.y,reform(field_geometry.wfs1[0,*]),reform(field_geometry.wfs1[1,*])) and guidestars.mag lt maglimits.wfs_mag_lim and guidestars.mag gt maglimits.wfs_bright_lim,w1cnt)
guidestars[id_wfs1].wfs=(guidestars[id_wfs1].wfs>0)+10
id_wfs2=where(mgh_pnpoly(guidestars.x,guidestars.y,reform(field_geometry.wfs2[0,*]),reform(field_geometry.wfs2[1,*])) and guidestars.mag lt maglimits.wfs_mag_lim and guidestars.mag gt maglimits.wfs_bright_lim,w2cnt)
guidestars[id_wfs2].wfs=(guidestars[id_wfs2].wfs>0)+20
nguide=g1cnt+g2cnt
nwfs=w1cnt+w2cnt
printf, -2, 'ng1, ng2,wfs1,wfs2', g1cnt,g2cnt,w1cnt,w2cnt
        ;REMOVE ANY OVERLAPS
        ;if not keyword_set(nofilter) then guidestars=mmirs_remove_guides(guidestars,gs_boxh,gs_boxw,gs_box_sep,wfs_sep, ng_limit=ng_limit, verbose=verbose)
       ;id_guide_list=where((guidestars.wfs ge 1) and (guidestars.wfs le 4),nguide,/null)
    if (nguide eq 0) or (nwfs eq 0) then begin
        if(keyword_set(verbose)) then message,'Warning: no guide or WFS stars found!!! Configuration rejected',/inf
        valid=0
        return,-1
    endif else begin
       ;need at least one guide or wfs on each side
       if ((g1cnt eq 0) and (w1cnt eq 0)) or ((g2cnt eq 0) and (w2cnt eq 0)) then begin
            if(keyword_set(verbose)) then message,'Warning: Must have at least one guide/WFS star for each camera. Configuration rejected',/inf
            valid=0
            return,-1
       endif
     
       id_guide_list=where((guidestars.wfs ge 1) and (guidestars.wfs le 22),nguide,/null)
       valid=1
       printf, -2, 'number of guides: ', nguide
       return, guidestars[id_guide_list]
    endelse
end
