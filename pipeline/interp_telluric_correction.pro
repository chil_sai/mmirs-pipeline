function interp_telluric_correction,tell_arr,airmass_arr,targ_airmass,$
       water_vapor_arr,targ_water_vapor,$
       atm_model_tab=atm_model_tab,simple=simple

    n_par = n_params()

    if(keyword_set(simple) or n_par eq 3) then begin
        min_am=min(airmass_arr,idxmin,max=max_am)
        n_tel=n_elements(airmass_arr)
        if(n_tel eq 1) then begin
            message,/inf,'Only one telluric spectrum is given. Returning it.'
            return,tell_arr
        endif
        if(targ_airmass lt min_am) then begin
            message,/inf,'Target airmass '+string(targ_airmass,format='(f7.3)')+$
                ' is lower than the min available one for the telluric correction. Using '+string(min_am,format='(f7.3)')
            targ_airmass=min_am
        endif
        if(targ_airmass gt max_am) then begin
            message,/inf,'Target airmass '+string(targ_airmass,format='(f7.3)')+$
                ' is higher than the max available one for the telluric correction. Using '+string(max_am,format='(f7.3)')
            targ_airmass=max_am
        endif
    
        quadratic=(n_tel eq 3)? 1 : 0
        lsquadratic=(n_tel gt 3)? 1 : 0
        n_wl=n_elements(tell_arr[*,0])
        tell_int=dblarr(n_wl)
        for i=0L,n_wl-1L do tell_int[i]=exp(interpol(transpose(alog(tell_arr[i,*])),airmass_arr,[targ_airmass]))
    endif else begin
        atm_model_int=interp_telluric_model(atm_model_tab,airmass=airmass_arr[0])
        atm_model_ref=interp_telluric_model(atm_model_int,water_vapor=water_vapor_arr[0])
        atm_model_int1=interp_telluric_model(atm_model_tab,airmass=targ_airmass)
        atm_model_ref1=interp_telluric_model(atm_model_int1,water_vapor=targ_water_vapor)
        tell_int=tell_arr[*,0]*atm_model_ref1.transmission/atm_model_ref.transmission
    endelse
    return,tell_int
end
    
    