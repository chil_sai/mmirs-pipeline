function get_bias,d_f,n1=n1,n2=n2,compress=compress,h2rg=h2rg
    if(n_elements(n1) ne 1) then n1=3
    if(n_elements(n2) ne 1) then n2=4

    b1=float(mrdfits(d_f,n1,/silent,compress=compress))+32768.0
    if(keyword_set(h2rg)) then b1=refpix(b1)
    if((n1 eq 1 and n2 eq 2) or (n1 eq 0)) then return,b1 ;;;; returning the 1st readout if the file is in the fowler mode
    b2=float(mrdfits(d_f,n2,/silent,compress=compress))+32768.0
    if(keyword_set(h2rg)) then b2=refpix(b2)
    b=b1-(b2-b1)

    return,b
end

