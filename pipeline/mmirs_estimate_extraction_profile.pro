function mask_emission_lines,wl,lines_wl,w
   if(n_params() eq 2) then w=0.15 ;;; 0.15nm
   flag=byte(wl*0)
   for i=0,n_elements(lines_wl)-1 do begin 
       b_l=where(abs(wl-lines_wl[i]) lt w, cb_l)
       if(cb_l gt 0) then flag[b_l]=1
   endfor
   return,flag
end

function mmirs_estimate_extraction_profile,slit_lin_2d,wl,$
    slitmask=slitmask,maskoh=maskoh,$
    nsegments=nsegments,fulltrace=fulltrace,$
    slit_profile_seg=slit_profile_seg,debug=debug, slit_lin_op=slit_lin_op
    
    if(n_elements(nsegments) ne 1) then nsegments=5

    s_slit=size(slit_lin_2d)
    if(n_elements(slitmask) ne 1) then slitmask=fix(slit_lin_2d*0)
    if(s_slit[1] ne n_elements(wl)) then begin
        message,/inf,'Extracted slit size does not correspond to the wavelength vector'
        return,!values.f_nan
    endif
    slit_lin_op=slit_lin_2d
    if (keyword_set(maskoh)) then begin
;        lines_oh=read_asc(getenv('MMIRS_PIPELINE_PATH')+'calib_FIRE/linelists/linesOH_R2k.tab')
        lines_oh=read_asc(getenv('MMIRS_PIPELINE_PATH')+'calib_MMIRS/linelists/linesOH_R2k.tab')
;        lines_oh=read_asc(getenv('BINO_PIPELINE_PATH')+'calib_Bino/linelists/linesOH_R2k_1050nm_corr.tab')
        lines_oh=lines_oh[*,where(lines_oh[2,*] gt 1e-1)]
        flag=mask_emission_lines(wl,transpose(lines_oh[0,*]),(wl[1]-wl[0])*2.0)
        slitmask=slitmask+transpose(congrid(transpose(flag),s_slit[2],s_slit[1]))
    
        mask_bad=where(slitmask gt 0, cmask_bad)
        if(cmask_bad gt 0) then slit_lin_op[mask_bad]=!values.d_nan
    endif
    ;b_wl=where(wl lt 3780. or wl gt 10400.0,cb_wl)
    ;if(cb_wl gt 0) then slit_lin_op[b_wl,*]=!values.f_nan

    aaa=''
    skypix=3
    if(keyword_set(fulltrace)) then begin
        slit_profile_seg=dblarr(nsegments,s_slit[2])
        skippix=10
        overlap=0.2
        for i=0,nsegments-1 do begin
            nmin = (i eq 0)? skippix : fix((double(i)-overlap)*s_slit[1]/nsegments)
            nmax = (i eq nsegments-1)? s_slit[1]-skippix-1 : fix((double(i)+1.0+overlap)*s_slit[1]/nsegments)
            if(nmin lt 0) then nmin=0
            if(nmax ge s_slit[1]) then nmax=s_slit[1]-1
;;            slit_profile_seg[i,*]=transpose(median(slit_lin_op[nmin:nmax,*],dim=1))
            resistant_mean,slit_lin_op[nmin:nmax,*],3.0,prof_cur,dim=1
            slit_profile_seg[i,*]=prof_cur
            bad_slit=where(finite(slit_profile_seg[i,*]) ne 1, cbad_slit, compl=good_slit, ncompl=cgood_slit)
            if(cgood_slit gt 2*skypix) then begin
                skylev=median([slit_profile_seg[i,good_slit[0:skypix-1]],$
                              slit_profile_seg[i,good_slit[cgood_slit-skypix:*]]])
;                skylev=min([median(slit_profile_seg[i,good_slit[0:skypix-1]]),$
;                            median(slit_profile_seg[i,good_slit[cgood_slit-skypix:*]])])
                slit_profile_seg[i,*]-=skylev
                if(keyword_set(debug)) then read,aaa
            endif
            if(cbad_slit gt 0) then slit_profile_seg[i,bad_slit]=0d
            slit_profile_seg[i,*]=(slit_profile_seg[i,*]>0)
            slit_profile_seg[i,*]=slit_profile_seg[i,*]/total(abs(slit_profile_seg[i,*]))
;oplot,slit_profile_seg[i,*],col=40+i*10
        endfor
        slit_profile=congrid(slit_profile_seg,s_slit[1],s_slit[2],/interp)
    endif else begin
;        slit_profile=median(slit_lin_op,dim=1)
        resistant_mean,slit_lin_op,3.0,prof_cur,dim=1
        slit_profile=prof_cur
        bad_slit=where(finite(slit_profile) ne 1, cbad_slit, compl=good_slit, ncompl=cgood_slit)
        if(cgood_slit gt 2*skypix) then begin
            skylev=median([slit_profile[good_slit[0:skypix-1]],slit_profile[good_slit[cgood_slit-skypix:*]]])
;            skylev=min([median(slit_profile[good_slit[0:skypix-1]]),$
;                        median(slit_profile[good_slit[cgood_slit-skypix:*]])])
print,'Skylev=',skylev,(slit_profile[good_slit[0:skypix-1]]),good_slit[0:skypix-1],$
                       (slit_profile[good_slit[cgood_slit-skypix:*]]),good_slit[cgood_slit-skypix:*]
            slit_profile-=skylev
            slit_profile[good_slit[0:skypix-1]]=0.
            slit_profile[good_slit[cgood_slit-skypix:*]]=0.
            if(keyword_set(debug)) then read,aaa
        endif
        if(cbad_slit gt 0) then slit_profile[bad_slit]=0d
        ;slit_profile=(slit_profile>0)
        ;stop
        slit_profile=slit_profile/total(abs(slit_profile))
    endelse

    if(keyword_set(debug)) then begin
        pp=(keyword_set(fulltrace))? slit_profile[s_slit[1]/2.,*] : slit_profile
        plot,pp,xs=1,ys=3
        read,aaa
    endif

    return,slit_profile
end

