function mmirs_extract_lin_spec,spec2d,var2d=var2d,psf,sky=sky,error=error,clean=clean,thres=thres,sum_extraction=sum_extraction,absflux=absflux,debug=debug
;;;; orde2d: a 2-dimensional array, linearized and rectified single order

    s_ord=size(float(spec2d))
    if(n_elements(var2d) ne n_elements(spec2d)) then var2d=spec2d*0d +1d
    s_psf=size(float(psf))
    if(n_elements(thres) ne 1) then thres=0.01 ;; 0.01

    if(not (array_equal(s_ord,s_psf) or ((s_psf[0] eq 1) and (s_ord[2] eq s_psf[1])))) then begin
        message,/inf,'PSF and the input order image have incompatible dimensions'
        return,-1
    endif

    psf2d=(s_psf[0] eq 1)? congrid(transpose(psf),s_ord[1],s_ord[2]) : psf
    ord_extr_fit=dblarr(s_ord[1])+!values.d_nan
    sky=ord_extr_fit
    error=ord_extr_fit
    sum_extraction=total(spec2d*psf2d,2,/nan)/total(psf2d^2.0,2,/nan)
    n_maxprof=1.5
    nbadpix=0l
    for x=0,s_ord[1]-1 do begin
        prof_cur=transpose(spec2d[x,*])
        ;eprof_cur=sqrt((abs(transpose(psf2d[x,*])))>1d-5) ;;;; prof_cur*0d +1d ;;; has to be correctly computed
        eprof_cur=(transpose(var2d[x,*])) > (keyword_set(absflux)? 1d-41 : 1)
        gkrnl=where(abs(psf2d[x,*]) gt 0.0,gkrnl_cnt)
        if gkrnl_cnt gt 1 then prof_maxval=max((median(abs(prof_cur[gkrnl])/eprof_cur[gkrnl],3)),/nan) else if gkrnl_cnt eq 1 then prof_maxval=abs(prof_cur[gkrnl])/eprof_cur[gkrnl] else prof_maxval=0 
        ;prof_maxval=max((median(abs(prof_cur)/eprof_cur,3))[1:s_ord[2]-2],/nan)
        gdata=where((finite(prof_cur) eq 1) and (abs(prof_cur)/eprof_cur lt n_maxprof*prof_maxval), cgdata)
        ;print, prof_maxval, cgdata,total(abs(psf2d[x,gdata])) 
        if(cgdata lt 10) or (total(abs(psf2d[x,gdata])) lt 0.05) then continue ;; lt 10
        c=regress(transpose(psf2d[x,gdata]),prof_cur[gdata],/double,chisq=chi2,const=c0,status=status,measure_errors=sqrt(eprof_cur[gdata]))
        if(status ne 0) then continue
        if(keyword_set(clean)) then begin
            dprof=(c0+c[0]*transpose(psf2d[x,*])-prof_cur[*])/total(abs(prof_cur),/nan)
            gdata1=where(((finite(dprof) eq 1) and (abs(prof_cur)/sqrt(eprof_cur)/10d lt n_maxprof*prof_maxval)) and ((abs(dprof) lt thres) or $
                          abs(c0+c[0]*transpose(psf2d[x,*])-prof_cur[*]) lt 10d*sqrt(eprof_cur)), cgdata1, ncompl=cbdata1)
;print,'x,cgdata1=',x,cgdata1
            if(cgdata1 lt 5) then continue ;; lt 10
;if(x eq 1526) then stop
;if (cgdata1 lt s_ord[2]) then print,'x,cgdata1=',x,cgdata1
            c=regress(transpose(psf2d[x,gdata1]),prof_cur[gdata1],/double,chisq=chi2,const=c0,status=status,measure_errors=sqrt(eprof_cur[gdata1]))
            gdata=gdata1
            nbadpix+=cbdata1
        endif
        ;ord_extr_fit[x]=c[0]
        ;ord_extr_fit[x]=total(transpose(psf2d[x,gdata])*(prof_cur[gdata]-c0)/eprof_cur[gdata])/total(transpose(psf2d[x,gdata])^2/eprof_cur[gdata])

        ord_extr_fit[x]=total(transpose(psf2d[x,gdata])*(prof_cur[gdata])/eprof_cur[gdata])/total(transpose(psf2d[x,gdata])^2/eprof_cur[gdata])
;        ord_extr_fit[x]=total(transpose(psf2d[x,gdata])*(prof_cur[gdata]))/total(transpose(psf2d[x,gdata])^2)
        error[x]=sqrt(total(transpose(abs(psf2d[x,gdata])))/total(transpose(psf2d[x,gdata])^2/eprof_cur[gdata]))
        sky[x]=c0
    endfor
    if(keyword_set(debug)) then print,'N_badpix=',nbadpix,' / ',s_ord[1]*s_ord[2]
    return,ord_extr_fit
end
