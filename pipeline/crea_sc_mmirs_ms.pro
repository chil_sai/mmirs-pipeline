function crea_sc_mmirs_ms,logfile,image_type,gzip=gzip,ndeg=ndeg,debug=debug,nomask=nomask

if(n_elements(ndeg) ne 1) then ndeg=5
if(n_params() eq 1) then image_type='obj'
suff=(keyword_set(gzip))? '.gz' : ''
wdir=def_wdir(LOGFILE)
h=headfits(wdir+image_type+'_dark.fits'+suff)
img=transpose(mrdfits(wdir+image_type+'_dark.fits'+suff,1,himg,/silent))

slit=def_slit(logfile)
if(slit ne 'mos') then return,img*0.0 ;;; no scattered light model for longslit

grism=def_grism(logfile,filt=filter)
if((grism eq 'H' and filter eq 'H') or (grism eq 'J' and filter eq 'J')) $
    then return,img*0.0 ;;;; no scattered light model for short spectra

fl=transpose(mrdfits(wdir+'flat_dark.fits'+suff,1,hfl,/silent))
Nx=sxpar(himg,'NAXIS2')
Ny=sxpar(himg,'NAXIS1')

dist_map=mrdfits(wdir+'dist_map.fits',1,/silent)

;;; masking the blinking amplifier -- q&d solution for an MMFIXEN bug
;;; put "/nomask" in order to include this region

if(not (keyword_set(nomask))) then begin
    img[0:1023,384:511]=!values.f_nan
    fl[0:1023,384:511]=!values.f_nan
endif

img_r=poly_2d(img,dist_map.kxwrap,dist_map.kywrap,1)
fl_r=poly_2d(fl,dist_map.kxwrap,dist_map.kywrap,1)

n_st=16L
for i=0,Ny-1 do fl_r[*,i]=median(fl_r[*,i],15)
fprof=total(fl_r,1,/nan) ;;;;median(fl_r,dim=1)

gaps=where(fprof lt median(fprof)*2.0)
mask_gaps=findgen(n_elements(fprof))*0
mask_gaps[gaps]=10
mask_gaps=smooth(mask_gaps,7)
gaps=where(mask_gaps eq 10,ngaps)

xgrid=(dindgen(n_st)+0.5)*(Nx/double(n_st))
dval=dblarr(n_st,ngaps)
xval=(xgrid # (dblarr(ngaps)+1.0))
yval=((dblarr(n_elements(xgrid))+1.0) # gaps)
for xx=0,n_st-1 do begin
    xmin_c=xx*(Nx/double(n_st))
    xmax_c=(xx+1.)*(Nx/double(n_st))-1.
    dval[xx,*]=median(img_r[xmin_c:xmax_c,[gaps]],dim=1)
endfor

s=sort(reform(yval,n_st*ngaps))
t_good=where(finite(dval[s]) eq 1)
s=s[t_good]

sset = bspline_iterfit(yval[s], dval[s], x2=xval[s], $
                       invvar=dval[s]*0.+1.,bkspace=80.0,npoly=ndeg)

xgridn=(dindgen(n_st*4)+0.5)*(Ny/double(n_st*4))
ygridn=(dindgen(n_st*4)+0.5)*(Ny/double(n_st*4))

modbgr=dblarr(n_st*4,n_st*4)
for x=0,n_st*4-1 do begin
    for y=0,n_st*4-1 do begin
        modbgr[x,y]=bspline_valu(ygridn[y],x2=xgridn[x],sset)
    endfor
endfor

bgr=((rebin(modbgr,Nx,Ny)) > 0)

if(keyword_set(debug)) then writefits,wdir+image_type+'_sc.fits',float(rotate(bgr,5))
return,bgr
end
