pro mmirs_extend_slits, slits, slit_geometry=slit_geometry, mask_params=mask_params
  slits=slits[sort(slits.y)]
  nslits=n_elements(slits)
  ;use double the usual minimum slit gap, so we preserve some extra area
  ;between slits
  gapmm_min=2.0*slit_geometry.gap*mask_params.arc2mm
  miny=mask_params.corners[1]
  maxy=mask_params.corners[3]
  ;extend first slit to edge of mask
  slits[0].polygon[1]=miny
  slits[0].polygon[7]=miny
  ;exend the last slit to edge of mask
  slits[nslits-1].polygon[3]=maxy
  slits[nslits-1].polygon[5]=maxy
  total_extra=0.0
  if nslits gt 1 then begin
     for i=1, nslits-1 do begin
        ;gap between this slit and the one before it
        gap=slits[i].polygon[1]-slits[i-1].polygon[3]
        ;space to add
        extra=gap-gapmm_min
;        printf, -2, extra, slits[i].polygon[1], slits[i-1].polygon[3]
;        printf, -2, 0, slits[i].polygon[7], slits[i-1].polygon[5]
        ;add half this extra to previous slit, half to this one, unless the extra is tiny
        if extra gt gapmm_min/10. then begin
           slits[i].polygon[1]=slits[i].polygon[1]-extra/2.0
           slits[i].polygon[7]=slits[i].polygon[7]-extra/2.0
           slits[i-1].polygon[3]=slits[i-1].polygon[3]+extra/2.0
           slits[i-1].polygon[5]=slits[i-1].polygon[5]+extra/2.0
           total_extra=total_extra+extra
        endif
     endfor
  endif
  printf, -2, 'extended slits. recovered sky (arcmin):', total_extra/mask_params.arc2mm/60.
  return
end
