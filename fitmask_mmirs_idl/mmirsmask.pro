pro mmirsmask, fullconfig
;fullconfig=JSON_PARSE(COMMAND_LINE_ARGS(), /toarray, /tostruct)
timestamptovalues, fullconfig.obsdate, day=day, hour=hour,minute=minute,month=month,second=second,year=year
jd=julday(month,day,year,hour,minute,second)

if tag_exist(fullconfig,'extend_slits') then extend_slits=fullconfig.extend_slits else extend_slits=0
n_t=n_elements(fullconfig.targets)
if (fullconfig.obstype eq 'imaging') then begin
   n_t=0
  objlist={target:0l,object:'none',ra:0d,dec:0d,pmra:0d,pmdec:0d,epoch:0d,mag:-1.0,type:'NONE',priority:1000.0}
endif else begin
   objlist=replicate($
    {target:0l,object:'none',ra:0d,dec:0d,pmra:0d,pmdec:0d,epoch:0d,mag:-1.0,type:'TARGET',priority:1000.0},n_t)
;objlist[*].object=fullconfig.targets[*].name
;objlist[*].target=fullconfig.targets[*].num
;objlist[*].ra=fullconfig.targets[*].ra
;objlist[*].dec=fullconfig.targets[*].dec
;objlist[*].pmra=fullconfig.targets[*].pmra
;objlist[*].pmdec=fullconfig.targets[*].pmdec
;objlist[*].epoch=fullconfig.targets[*].epoch
;objlist[*].mag=fullconfig.targets[*].mag
;objlist[*].type=fullconfig.targets[*].type 
;objlist[*].priority=1000.0/(10.^(fullconfig.targets[*].priority > 1.0))
;fill blanks
;nul=where(objlist.type eq !null, /null)
;objlist[nul].type='TARGET'
;nul=where(objlist.mag eq !null, /null)
;objlist[nul].mag=-1.0
;nul=where(objlist.pmra eq !null, /null)
;objlist[nul].pmra=0.0
;nul=where(objlist.pmdec eq !null,/null)
;objlist[nul].pmdec=0.0
;nul=where(objlist.priority eq !null,/null)
;objlist[nul].priority=1.0
;nul=where(objlist.epoch eq !null,/null)
;objlist[nul].epoch=200.0

 ;  printf, -2, minmax(fullconfig.targets.priority)
;normalize priorities
allpri=fltarr(n_t)
for i=0, n_t-1 do allpri[i]=fullconfig.targets[i].priority

   minmaxpri=minmax(allpri)
 if minmaxpri[1] eq minmaxpri[0] then minmaxpri[1]=minmaxpri[0]+1.  
for i=0, n_t-1 do begin
   in=fullconfig.targets[i]
   out=objlist[i]
   struct_assign,in,out, /nozero
   objlist[i]=out
   objlist[i].object=fullconfig.targets[i].name
   objlist[i].target=fullconfig.targets[i].num
   objlist[i].type=strtrim(fullconfig.targets[i].type,2)
   reduced_pri=(objlist[i].priority-minmaxpri[0])/(minmaxpri[1]-minmaxpri[0])*5.0
   objlist[i].priority=100000.0/(10.^(reduced_pri))
endfor
;printf, -2, where(finite(objlist.priority,/inf))
;printf, -2, where(finite(objlist.priority,/nan))
;printf, -2, minmax(objlist.priority)
endelse
n_g=n_elements(fullconfig.guides)
guidelist=replicate($
    {target:0l,object:'none',ra:0d,dec:0d,pmra:0d,pmdec:0d,epoch:0d,mag:-1.0,type:'GUIDE',priority:0.0},n_g)

for i=0, n_g-1 do begin
   in=fullconfig.guides[i]
   out=guidelist[i]
   struct_assign,in,out, /nozero
   guidelist[i]=out
   guidelist[i].object=fullconfig.guides[i].name
   guidelist[i].target=fullconfig.guides[i].num
endfor


field_geometry=mmirs_read_field_geometry('~/IDL/mmirs-pipeline/fitmask_mmirs_idl/config/mmirs_field_geometry.cfg')

mask_params={ $
        label:fullconfig.maskname,$
        guide:'Ok',$
        mjdstart:(jd-2400000.5d),$
        duration:120d,$
        ra:fullconfig.ra,$
        dec:fullconfig.dec,$
        ha:fullconfig.hourangle,$
        airmass:1.0d,$
        parang:0d,$
        posa:fullconfig.pa,$
        rotator:0d,$
        wavelength:0d,$ ;fullconfig.wavelength,$
        arc2mm:0.165d,$
        boxw:fullconfig.alignwidth,$
        boxh:fullconfig.alignlen,$
        corners:dblarr(4),$
        inst:'mmirs.mdf',$
        rank:1,$
        filter:fullconfig.filter,$
        grism:fullconfig.grism,$
        telepoly:dblarr(7),$
        telerevs:dblarr(7) $
    }

mask_params.telepoly=[0.0,599.508,7.25994,0.0,73.5279,0.0,-40.5352]
mask_params.telerevs=[0.0,0.00166808,-3.42784e-08,0.0,-9.15714e-13,0.0,-1.88181e-18]
mask_params.corners=field_geometry.mask
mask_params.duration=double(fullconfig.duration)
gs_min=fullconfig.gs_min
optimize_type=fullconfig.optimize_type

mmirs_expand_mask_params,mask_params

trace_geometry=mmirs_read_trace_geometry('~/IDL/mmirs-pipeline/fitmask_mmirs_idl/config/data/mmirs-filter.tab','~/IDL/mmirs-pipeline/fitmask_mmirs_idl/config/data/mmirs-grism.tab')

if (fullconfig.obstype eq 'imaging') then objlist=guidelist else objlist=[objlist,guidelist]

posa_arr=fullconfig.minpa+findgen(floor(fullconfig.maxpa-fullconfig.minpa) > 1)
if fullconfig.obstype eq 'imaging' then posa_arr=posa_arr[sort(abs(fullconfig.pa-posa_arr))]

if fullconfig.dra gt 0.0001 then ra_arr=fullconfig.ra+fullconfig.dra/60.0d/2.0d*(dindgen(5)-2d) else ra_arr=fullconfig.ra
if fullconfig.ddec gt 0.0001 then dec_arr=fullconfig.dec+fullconfig.ddec/60.0d/2.0d*(dindgen(5)-2d) else dec_arr=fullconfig.dec


slit_geometry={height:fullconfig.slitlen,width:fullconfig.slitwidth,gap:0.25}
box_geometry={height:fullconfig.alignlen,width:fullconfig.alignwidth,gap:0.25}
gs_geometry={height:1.0,width:1.0,gap:0.25}
;need to fix field_geometry for the guides regions, using requested
;guide box height
;gs_boxh=mask_params.arc2mm*gs_geometry.height
;for i=0, n_elements(field_geometry.guide[0,*])-1 do begin
   ;field geometry now reflects the allowed *centers* for guide stars at
   ;outer edges, but two tweaks need to be made
   ;For the FCS 'avoid' regions, these need to be tweaked to subtract a
                                ;boxh/2.0 buffer
;   printf, -2, i
;   case (i MOD 2) of
;      0:field_geometry.guide[3,i]=field_geometry.guide[3,i]-gs_boxh/2.
;      1:field_geometry.guide[1,i]=field_geometry.guide[1,i]+gs_boxh/2.
;   endcase
   ;also need to tweak the inner X limits (+-16.136) so that boxes are
   ;actually on the mask 
;   case 1 of
;      (i lt 4):field_geometry.guide[2,i]-=gs_boxh/2.
;      (i ge 4):field_geometry.guide[0,i]+=gs_boxh/2.
;   endcase
;endfor
;printf, -2,field_geometry.guide

;printf,-2, posa_arr, ra_arr,dec_arr


b=mmirs_mask_optimize(objlist,mask_params,field_geometry,$
        slit_geometry,box_geometry=box_geometry,gs_geometry=gs_geometry,$
        trace_geometry=trace_geometry,slits=slits_a_clean,guidestars=guidestars,$
                         posa_arr=posa_arr,ra_arr=ra_arr,dec_arr=dec_arr,$
                         optimize_type=optimize_type,obstype=fullconfig.obstype, $
        mask_params_out=mask_params_out,gs_min=gs_min,merit_arr=merit_arr, extend_slits=extend_slits)

;fullresult={}
;print, JSON_SERIALIZE(fullresult)
if b ne -1 then begin
   if n_elements(slits_a_clean) ge 1 then begin
      slits_a_clean.PRIORITY=minmaxpri[1]-(minmaxpri[1]-minmaxpri[0])/5.0*alog10(slits_a_clean.PRIORITY) ;3.0-alog10(slits_a_clean.PRIORITY)
      sidea_json=JSON_SERIALIZE(slits_a_clean)
      ;printf, -2, n_elements(slits_a_clean)
   endif   else sidea_json=json_serialize(binospec_slit_stub()) ;'[{"msg":"none"}]'
;   if n_elements(slits_b_clean) ge 1 then begin
;      slits_b_clean.PRIORITY=minmaxpri[1]-(minmaxpri[1]-minmaxpri[0])/5.0*alog10(slits_b_clean.PRIORITY) ;3.0-alog10(slits_b_clean.PRIORITY)
;      sideb_json=JSON_SERIALIZE(slits_b_clean)
;   endif else sideb_json=json_serialize(binospec_slit_stub())
print, '[{"rank":1,"error":0, "errmsg":"", "mask_config":'+JSON_SERIALIZE(mask_params_out)+',"sidea":'+sidea_json+',"guides":'+JSON_SERIALIZE(guidestars)+'}]'
;printf, -2,'[{"rank":1,"error":0, "errmsg":"", "mask_config":'+JSON_SERIALIZE(mask_params_out)+',"sidea":'+sidea_json+',"sideb":'+sideb_json+',"guides":'+JSON_SERIALIZE(guidestars)+'}]'
endif else begin
   num_rotator=where(merit_arr eq -1.0,nrot, /null)
   num_guid=where(merit_arr eq -2.0, nguid,/null)
   num_tries=n_elements(merit_arr)
   print,'[{"rank":1,"error":1, "errmsg":"No workable configurations found in '+string(num_tries, format='(I4)')+' permutations. Failed due to rotator limits '+string(nrot, format='(I4)')+' times, and could not place guide or WFS stars '+string(nguid,format='(I4)')+' times."}]'
endelse
;print, jd
;help,fullconfig,/str
end
