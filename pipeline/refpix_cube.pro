pro refpix_cube, img, namps=namps

    if(n_elements(namps) ne 1) then namps=32
    smolen = 10
    nsig = 3

    ;message,/inf,'Running refpix on a H2RG frame'
    
    s=size(img)
    ampsize=s[1]/namps

    refpix1=[1,2,3]
    refpix2=[findgen(4)+s[1]-4]
    refpix_all=[0,1,2,3,findgen(4)+s[1]-4]
    ;;;refvec=median(img[refpix_all,*],dim=1)
    refvec=total(total(img[refpix_all,*,*],1),2)/n_elements(refpix_all)/s[3]
    svec=smooth(refvec,11,/nan)

    svec_img=congrid(transpose(svec),s[1],s[2])
    
    for i=0,s[3]-1 do img[*,*,i]=img[*,*,i]-svec_img

    for amp=0,namps-1 do begin

        img_ref=img[*,[refpix1,refpix2],*]
        resistant_mean,img_ref[amp*ampsize+2*findgen(ampsize/2),*,*],3.0,ref1,std1
        resistant_mean,img_ref[amp*ampsize+2*findgen(ampsize/2)+1,*,*],3.0,ref2,std2
        ref12=(ref1+ref2)/2.
        message,/inf,'amp,ref='+string([amp,ref1,ref2,ref12],format='(4f8.1)')

        img[amp*ampsize:(amp+1)*ampsize-1,*,*]=img[amp*ampsize:(amp+1)*ampsize-1,*,*]-ref12

    endfor
end
