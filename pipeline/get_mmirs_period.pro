function get_mmirs_period,filename,exten=exten,$
    year=year,month=month,suffix=suffix

if(n_elements(exten) ne 1) then exten=1
h=headfits(filename,exten=exten,/silent)
dateobs=sxpar(h,'DATE-OBS',count=count)
if(count eq 0) then begin
    year=2010
    month=0
    suffix='A'
endif else begin
    year=float(strmid(dateobs,0,4))
    month=float(strmid(dateobs,5,2))
    if(year ge 2010 and year le 2014) then begin ;;; Magellan (semesters)
        suffix=(month le 6)? 'A' : 'B'
    endif else begin ;;; MMT (trimesters)
        suffix=(month le 4)? 'A' : (month le 8)? 'B' : 'C'
    endelse
endelse

period = string(year,format='(i4.4)')+suffix

return,period

end
