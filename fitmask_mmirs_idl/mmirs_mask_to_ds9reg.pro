function simple_xy2radec,xy,center,posa=posa,scale=scale

    if(n_elements(posa) ne 1) then posa=0d
    if(n_elements(scale) ne 1) then scale=1d/3600d ;;; deg per pixel
    radec=xy*0.0

    xi=transpose(xy[0,*])*scale/!radeg
    eta=transpose(xy[1,*])*scale/!radeg
    xi_new =    xi*cos(-posa/!RADEG) - eta*sin(-posa/!RADEG)
    eta_new=    xi*sin(-posa/!RADEG) + eta*cos(-posa/!RADEG)
    ra=atan(xi_new/(cos(center[1]/!radeg)-eta_new*sin(center[1]/!radeg)))*!radeg + center[0]
    dec=atan((sin((ra-center[0])/!radeg)*(sin(center[1]/!radeg)+eta_new*cos(center[1]/!radeg)))/xi_new)*!radeg
    radec[0,*]=transpose(ra)
    radec[1,*]=transpose(dec)
    return,radec
end


pro mmirs_mask_to_ds9reg, mask_params, slits, guidestars, $
    field_geometry, ds9regfile


cd_matrix=[[-cos(mask_params.posa/!radeg),sin(mask_params.posa/!radeg)],$
           [ sin(mask_params.posa/!radeg),cos(mask_params.posa/!radeg)]]/mask_params.arc2mm/3600d



openw,u,ds9regfile,/get_lun
printf,u,'# Region file format: DS9 version 4.1'
printf,u,'# Filename: test.fits'
printf,u,'# WCSAXES =2'
printf,u,'# CRVAL1  ='+string(mask_params.ra,form='(f12.7)')
printf,u,'# CRVAL2  ='+string(mask_params.dec,form='(f12.7)')
printf,u,'# CRPIX1  =0.0'
printf,u,'# CRPIX2  =0.0'
;printf,u,'# CDELT1  =-0.000206332'
;printf,u,'# CDELT2  =0.000206332'
;printf,u,'# CROT  ='+string(mask_params.posa,format='(f8.3)')
printf,u,'# CTYPE1  =RA---TAN'
printf,u,'# CTYPE2  =DEC--TAN'
printf,u,'# CUNIT1  =deg'
printf,u,'# CUNIT2  =deg'
printf,u,'# CD1_1   ='+string(cd_matrix[0,0],format='(g16.10)')
printf,u,'# CD1_2   ='+string(cd_matrix[0,1],format='(g16.10)')
printf,u,'# CD2_1   ='+string(cd_matrix[1,0],format='(g16.10)')
printf,u,'# CD2_2   ='+string(cd_matrix[1,1],format='(g16.10)')
printf,u,'# RADESYS =FK5'
printf,u,'# MJD-OBS = '+string(mask_params.mjdstart,format='(f14.6)')
printf,u,'# EQUINOX =2000.0'
printf,u,'global color=green dashlist=8 3 width=1 font="helvetica 10 normal" select=1 highlite=1 dash=0 fixed=0 edit=1 move=0 delete=1 '
printf,u,'fk5'
printf,u,'circle '+sixty_str(mask_params.ra/15d,prec=3)+' '+$
                   sixty_str(mask_params.dec,prec=2)+' 15" # color=red'
for i=0,n_elements(guidestars)-1 do $
    printf,u,'box '+sixty_str(guidestars[i].ra/15d,prec=3)+' '+$
                    sixty_str(guidestars[i].dec,prec=2)+' '+$
                    string(mask_params.boxw,format='(f6.2)')+'" '+$
                    string(mask_params.boxh,format='(f6.2)')+'" '+$
                    string(mask_params.posa,format='(f8.3)')+' # color=red'
for i=0,n_elements(slits)-1 do $
    printf,u,'box '+sixty_str(slits[i].ra/15d,prec=3)+' '+$
                    sixty_str(slits[i].dec,prec=2)+' '+$
                    string(slits[i].width/mask_params.arc2mm,format='(f6.2)')+'" '+$
                    string(slits[i].height/mask_params.arc2mm,format='(f6.2)')+'" '+$
                    string(mask_params.posa,format='(f8.3)')

;;; simple conversion from tangential coordinates to ra/dec
radec=simple_xy2radec((field_geometry.mask[0:1]+field_geometry.mask[2:3])/2d,$
                        [mask_params.ra,mask_params.dec],posa=mask_params.posa,scale=1d/mask_params.arc2mm/3600d)
radec_wfs=simple_xy2radec((field_geometry.wfs[0:1]+field_geometry.wfs[2:3])/2d,$
                        [mask_params.ra,mask_params.dec],posa=mask_params.posa,scale=1d/mask_params.arc2mm/3600d)
radec_guide=simple_xy2radec((field_geometry.guide[0:1,*]+field_geometry.guide[2:3,*])/2d,$
                        [mask_params.ra,mask_params.dec],posa=mask_params.posa,scale=1d/mask_params.arc2mm/3600d)

printf,u,'box '+sixty_str(radec[0]/15d,prec=3)+' '+$
                sixty_str(radec[1],prec=2)+' '+$
                string((field_geometry.mask[2]-field_geometry.mask[0])/mask_params.arc2mm,format='(f8.2)')+'" '+$
                string((field_geometry.mask[3]-field_geometry.mask[1])/mask_params.arc2mm,format='(f8.2)')+'" '+$
                string(mask_params.posa,format='(f8.3)')+' # color=red'
;printf,u,'box '+sixty_str(radec_wfs[0]/15d,prec=3)+' '+$
;                sixty_str(radec_wfs[1],prec=2)+' '+$
;                string((field_geometry.wfs[2]-field_geometry.wfs[0])/mask_params.arc2mm,format='(f8.2)')+'" '+$
;                string((field_geometry.wfs[3]-field_geometry.wfs[1])/mask_params.arc2mm,format='(f8.2)')+'" '+$
;                string(mask_params.posa,format='(f8.3)')+' # color=orange'
for i=0,n_elements(radec_guide[0,*])-1 do $
    printf,u,'box '+sixty_str(radec_guide[0,i]/15d,prec=3)+' '+$
                sixty_str(radec_guide[1,i],prec=2)+' '+$
                string((field_geometry.guide[2,i]-field_geometry.guide[0,i])/mask_params.arc2mm,format='(f8.2)')+'" '+$
                string((field_geometry.guide[3,i]-field_geometry.guide[1,i])/mask_params.arc2mm,format='(f8.2)')+'" '+$
                string(mask_params.posa,format='(f8.3)')+' # color=blue'


close,u
free_lun,u

end
