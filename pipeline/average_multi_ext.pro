pro average_multi_ext,inpfiles,outfile,median=median,flat=flat,$
    biasframe=biasframe,cutflat=cutflat,verbose=verbose,$
    saturation=saturation,correct_persistence=correct_persistence

if(n_elements(saturation) ne 1) then saturation=62000.0

n_ext=get_nreads_ramp(inpfiles[0],exptime=exptime,ramp=ramp,header=h_img,gain=gain,rdnoise=rdnoise)
h_dummy=headfits(inpfiles[0])
n_f=n_elements(inpfiles)

if(n_elements(cutflat) ne 1) then cutflat=2.3

writefits,outfile,0,h_dummy

bias = (n_elements(biasframe) eq 1)? get_bias(biasframe) : 0.0
if(n_elements(bias) gt 0) then begin
    bias_cube=fltarr(2048,2048,n_f)
    for i=0,n_f-1 do bias_cube[*,*,i]=bias
    bias=bias_cube
endif

for i=0,n_ext-1 do begin
    if(keyword_set(verbose)) then print,'Processing extension ',i+1,' out of ',n_ext
    cube_cur=fltarr(2048,2048,n_f)
    for f=0,n_f-1 do begin
        cube_cur[*,*,f]=float(mrdfits(inpfiles[f],i+1,h,/silent))+32768.0
    endfor
    sat_pix=where(cube_cur ge saturation,csat_pix)
    if(csat_pix gt 0) then cube_cur[sat_pix]=!values.f_nan
    if(keyword_set(verbose)) then print,'number of saturated pixels:',csat_pix
    if(keyword_set(flat)) then begin
        med_img=median(cube_cur-bias,dim=3)
        val_med=median(med_img)
        for f=0,n_f-1 do begin
            val_cur=median(cube_cur[*,*,f]-bias[*,*,f])
            cube_cur[*,*,f]=(cube_cur[*,*,f]-bias[*,*,f])*val_med/val_cur + bias[*,*,f]
        endfor
        if((csat_pix gt 0) and (n_f gt 2) and $
            keyword_set(correct_persistence)) then begin
            persist_pix=sat_pix + 2048L*2048L
            g_pers=where(persist_pix lt n_elements(cube_cur),n_g_pers)
            if(n_g_pers gt 0) then cube_cur[persist_pix[g_pers]]=!values.f_nan
            if(keyword_set(verbose)) then print,'number of pixels with persistence:',n_g_pers
        endif

        med_val=median(cube_cur-bias)
;;;        bval = where(((cube_cur-bias) lt med_val/cutflat) or (cube_cur-bias) gt med_val*cutflat, cbval)
        bval = where(((cube_cur-bias) lt med_val/cutflat), cbval)
        if(keyword_set(verbose)) then print,'number of rejected values: ',cbval
        if(cbval gt 0) then cube_cur[bval]=!values.f_nan
    endif
    out_img=(keyword_set(median))? median(cube_cur,dim=3)-32768.0 : float(total(cube_cur,3))/float(n_ext)-32768.0
    mwrfits,out_img,outfile,h
endfor

end
