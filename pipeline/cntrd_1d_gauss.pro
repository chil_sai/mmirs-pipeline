pro cntrd_1d_gauss, img_orig, x_ori, xcen, errxcen=errxcen, fwhm_ori, SILENT= silent, DEBUG=debug, oversample=oversample, fast=fast, quadpoly=quadpoly

 On_error,2                          ;Return to caller

 if N_params() LT 3 then begin
        print,'Syntax: CNTRD, img, x, xcen, [ fwhm, /SILENT, /DEBUG ]'
        PRINT,'img - Input image vector'
        PRINT,'x - Input scalars giving approximate X position'
        PRINT,'xcen - Output scalars giving centroided X position'
        return
 endif else if N_elements(fwhm_ori) NE 1 then $
      read,'Enter approximate FWHM of image in pixels: ',fwhm_ori

 if(n_elements(oversample) ne 1) then oversample=1
 img = (oversample ge 2)? rebin(img_orig,fix(oversample)*n_elements(img_orig)) : img_orig
 x = (oversample ge 2)? x_ori*double(fix(oversample)) : x_ori
 FWHM = (oversample ge 2)? FWHM_ORI*double(fix(oversample)) : FWHM_ORI
 sz_image = size(img)
 if sz_image[0] NE 1 then message, $
   'ERROR - Vector (first parameter) must be 1 dimensional'

 xsize = sz_image[1]
 dtype = sz_image[2]              ;Datatype

;   Compute size of box needed to compute centroid

 nhalf =  fix(0.637*fwhm) > 2  ;
 nbox = 2*nhalf+1             ;Width of box to be used to compute centroid
 nhalfbig = nhalf +3
 nbig = nbox + 6        ;Extend box 3 pixels on each side to search for max pixel value
 npts = N_elements(x) 
 xcentroid = fltarr(npts)
 xcen = float(x)
 errxcen = float(x)*0.0+2.0
 ix = fix( x + 0.5 )          ;Central X pixel        ;Added 3/93

 for i = 0,npts-1 do begin        ;Loop over X vector

 pos = strtrim(x[i],2)

 if ( (ix[i] LT nhalfbig) or ((ix[i] + nhalfbig) GT xsize-1)) then begin
     if not keyword_set(SILENT) then message,/INF, $
           'Position '+ pos + ' too near edge of image'
     xcen[i] = -1
     goto, DONE
 endif

 bigbox = img[ix[i]-nhalfbig : ix[i]+nhalfbig]

;  Locate maximum pixel in 'NBIG' sized subimage 

 mx = max( bigbox)     ;Maximum pixel value in BIGBOX
 mx_pos = where(bigbox EQ mx, Nmax) ;How many pixels have maximum value?
 idx = mx_pos mod nbig          ;X coordinate of Max pixel
 if NMax GT 1 then begin        ;More than 1 pixel at maximum?
     idx = round(total(idx)/Nmax)
 endif else begin
     idx = idx[0]
 endelse

 xmax = ix[i] - (nhalf+3) + idx  ;X coordinate in original image array

; ---------------------------------------------------------------------
; check *new* center location for range
; added by Hogg

 if ( (xmax LT nhalf) or ((xmax + nhalf) GT xsize-1) ) then begin
     if not keyword_set(SILENT) then message,/INF, $
           'Position '+ pos + ' moved too near edge of image'
     xcen[i] = -1
     goto, DONE
 endif
; ---------------------------------------------------------------------

;  Extract smaller 'STRBOX' sized subimage centered on maximum pixel 

 strbox = img[xmax-nhalf : xmax+nhalf]
 if (dtype NE 4) and (dtype NE 5) then strbox = float(strbox)

 if keyword_set(DEBUG) then begin
       message,'Subarray used to compute centroid:',/inf
       print,strbox
 endif  

 ir = (nhalf-1) > 1 
 dd = indgen(nbox-1) + 0.5 - nhalf
; Weighting factor W unity in center, 0.5 at end, and linear in between 
 w = dd*0+0.5
 w[1:n_elements(w)-2]=1.0 
 sumc   = total(w)

; Find X centroid

 deriv = shift(strbox,-1) - strbox    ;Shift in X & subtract to get derivative
 deriv = deriv[0:nbox-2] ;Don't want edges of the array
 sumd   = total( w*deriv )
 sumxd  = total( w*dd*deriv )
 sumxsq = total( w*dd^2 )

 if sumxd GT 0 then begin  ;Reject if X derivative not decreasing
   
   if not keyword_set(SILENT) then message,/INF, $
        'Unable to compute X centroid around position '+ pos
   xcen[i]=-1
   goto,DONE
 endif 
 dx = sumxsq*sumd/(sumc*sumxd)
 status=1

 if(~keyword_set(fast)) then begin
     if(keyword_set(quadpoly)) then begin 
         pp=robust_poly_fit(dindgen(nbox),strbox,2)
         min_pos=-0.5*pp[1]/pp[2]
         if((pp[2] lt 0) and (min_pos gt 0) and (min_pos lt nbox)) then begin
             dx=float(nhalf)-min_pos
             errxcen[i]=0.1
         endif
     endif else begin
         kkk=mpfitpeak(/gauss,/positive,dindgen(nbox),strbox,resfit,estim=[max(strbox)-min(strbox),nbox/2.0,FWHM/2.35,0.0],nterms=4,sig=sigfit,status=status)

         if ((status eq 0) and (sigfit[1] gt 0) and (sigfit[1] le 0.2) and (resfit[1] gt 0) and (resfit[1] lt nbox) and (resfit[2] gt 0) and (resfit[0] gt 0)) then begin
             ;print,resfit,sigfit,dx,dx-(float(nhalf)-resfit[1]),format='(2(f7.0,f7.2,f8.2,f7.0),2f8.2)'
             errxcen[i] = sigfit[1]
             dx=float(nhalf)-resfit[1]
         endif
     endelse
 endif
 if ( abs(dx) GT nhalf ) then begin    ;Reject if centroid outside box  
   if not keyword_set(SILENT) then message,/INF, $
       'Computed X centroid for position '+ pos + ' out of range'
   xcen[i]=-1
   goto, DONE
 endif

 xcen[i] = xmax - dx    ;X centroid in original array

 DONE: 

 endfor

 if(oversample ge 2) then begin
   xcen=xcen/double(fix(oversample))
 endif
 return



end
