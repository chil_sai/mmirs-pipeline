function poly3d,x,y,z,c0,deg1=deg1,deg2=deg2,deg3=deg3,irregular=irregular,legendre=legendre,crd_map=crd_map,cbadpoints=cb_crd

if(n_elements(crd_map) ne 6) then crd_map=[[0d,0d,0d],[1d,1d,1d]]

c=c0
s=size(c)
if(s[0] eq 3) then begin
    deg1a=s[1]-1L
    deg2a=s[2]-1L
    deg3a=s[3]-1L
    c1=c ;;;;transpose(c)
endif else message,'Only 3D coefficient arrays are supported for a moment'

nx=n_elements(x)
ny=n_elements(y)
nz=n_elements(z)
if(keyword_set(irregular) and ((nx ne ny) or (nx ne nz))) then message,'X and Y should have equal number of elements when IRREGULAR is set'

if(keyword_set(irregular)) then begin
    x_crd=x
    y_crd=y
    z_crd=z
endif else begin
    x_crd=reform(transpose(congrid(transpose([x]),ny,nx,nz)),nx*ny*nz)
    y_crd=reform(congrid(transpose(y),nx,ny,nz),nx*ny*nz)
    z_crd=reform(congrid(transpose(z),nx,ny,nz),nx*ny*nz)
endelse

if(crd_map[0,0] ne 0d and crd_map[0,1] ne 1d) then x_crd=(x_crd-crd_map[0,0])/crd_map[0,1]
if(crd_map[1,0] ne 0d and crd_map[1,1] ne 1d) then y_crd=(y_crd-crd_map[1,0])/crd_map[1,1]
if(crd_map[2,0] ne 0d and crd_map[2,1] ne 1d) then z_crd=(z_crd-crd_map[2,0])/crd_map[2,1]

if(keyword_set(legendre)) then begin
    g_crd=where((x_crd ge -1d and x_crd le 1d) and $
                (y_crd ge -1d and y_crd le 1d) and $
                (z_crd ge -1d and z_crd le 1d),cg_crd,compl=b_crd,ncompl=cb_crd)
    if(cb_crd gt 0) then message,/inf,string(cb_crd,format='(i)')+' points have coordinates outside the [-1,1] range. Returning NaNs for them.'
endif else begin
    cb_crd=0l
    b_crd=[-1l]
endelse

res = (keyword_set(irregular))? dblarr(nx) : dblarr(nx*ny*nz)

for i=0, deg1a do begin
    for j=0, deg2a do begin
        for k=0, deg3a do begin
            if(keyword_set(legendre)) then $
                res[g_crd]+=legendre(x_crd[g_crd],i)*legendre(y_crd[g_crd],j)*legendre(z_crd[g_crd],k)*c1[i,j,k] $
            else $
                res+= x_crd^i*y_crd^j*z_crd^k*c1[i,j,k]
        endfor
    endfor
endfor

if(cb_crd gt 0) then res[b_crd]=!values.d_nan

if(not keyword_set(irregular)) then res=reform(res,nx,ny,nz)
return,res
end
