;-------------------------------------
; parsing spectral WCS
;-------------------------------------

pro parse_spechdr,spheader,axiswl=axiswl,$
    wl=wl,nlam=nlam,islog=islog,velscale=velscale,unitwl=unitwl,waven=waven ;;; output keywords

axwls = strtrim((n_elements(axiswl) eq 1)? axiswl : 1, 2)
naxwl = string(axwls, format='(i1)')
islog=0
velScale=-1d
nlam=sxpar(spheader,'NAXIS'+naxwl)
bintype=strcompress(sxpar(spheader,'BINTYPE'),/remove_all)
crpix=sxpar(spheader,'CRPIX'+axwls,count=cntcrpix)
if(cntcrpix ne 1) then crpix=1.0
w0=double(sxpar(spheader,'CRVAL'+axwls))
disp=double(sxpar(spheader,'CDELT'+axwls))
unitwl=sxpar(spheader,'CUNIT'+axwls,count=cntunit)
if(cntunit eq 0) then unitwl='A' else unitwl=strcompress(unitwl,/remove_all)
if(disp eq 0.0) then disp=sxpar(spheader,'CD'+axwls+'_'+axwls)
ctype1=strcompress(string(sxpar(spheader,'CTYPE'+axwls)),/remove_all)
log10wl=0
if(string(sxpar(spheader,'WFITTYPE')) eq 'LOG-LINEAR') then begin ;; for SDSS
    log10wl=1
    ctype1='WAVE-LOG'
endif

waven=(ctype1 eq 'WAVN')? 1 : 0
if((ctype1 eq 'AWAV-LOG') or (ctype1 eq 'WAVE-LOG') or (ctype1 eq 'WAVE-ALOG')) then begin
    wl=(log10wl eq 1)? 10d^(w0+disp*(dindgen(nlam)-crpix+1d)) : exp(w0+disp*(dindgen(nlam)-crpix+1d))
    velScale=disp*299792.458d
    if(log10wl eq 1) then velScale=velScale*alog(10d)
    islog=1
endif else begin
    if(bintype ne 'log') then begin
        wl=w0+disp*(dindgen(nlam)-crpix+1d)
    endif else begin
        wl=w0*exp(disp*(dindgen(nlam)-crpix+1d)/299792.458d)
        velScale=disp
        islog=1
    endelse
endelse
end

