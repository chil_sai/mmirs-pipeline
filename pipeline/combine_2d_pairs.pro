;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; usage example:
;
;    loglist=file_search('lei1/lei1*.txt')
;    combine_2d_pairs,loglist,'/db1/Data/raw/MOS/MMIRS/Leiton/lei1/sum_pairs/',box=1,dith0=7
;    combine_2d_pairs,loglist,'/db1/Data/raw/MOS/MMIRS/Leiton/lei1/sum_pairs/',tell=5,box=1,/corr,dith0=7
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

function mask_oh_lines,wl,w
    if(n_params() eq 1) then w=4.0 ;; 4A=0.4nm
    flag=byte(wl*0)
    lines_data=read_asc(getenv('MMIRS_PIPELINE_PATH')+'calib_MMIRS/linelists/linesOH_R2k.tab')
    lines_wl=transpose(lines_data[0,*])
    for i=0,n_elements(lines_wl)-1 do begin
        b_l=where(abs(wl-lines_wl[i]) lt w, cb_l)
        if(cb_l gt 0) then flag[b_l]=1
    endfor
    return,flag
end

pro combine_2d_pairs,logfilelist,wdir,$
    dith0=dith0,box_idx=box_idx,target=target,anyslit=anyslit,nokelson=nokelson,$
    clean=clean,weight_by_seeing=weight_by_seeing,fitprofile=fitprofile,$
    corr_tell=corr_tell,num_tel=num_tel,telluric_dir_idx=telluric_dir_idx,gain=gain,verbose=verbose

weight_by_seeing=1
clean=1
if(n_elements(num_tel) eq 0) then num_tel=[1]
if(n_elements(telluric_dir_idx) ne 1) then telluric_dir_idx=0
if(n_elements(box_idx) eq 0) then box_idx=0 ;;; use offset from the 1st align box
if(n_elements(gain) ne 1) then gain=2.1 ;;; 5.0

n_exp=n_elements(logfilelist)

log_first=readlog(logfilelist[0])
dithpos=sxpar(log_first,'DITHPOS')
dithpos2=sxpar(log_first,'DITHPOS2')
if(n_elements(dith0) ne 1) then dith0=(dithpos-dithpos2)/0.2/2.0 ;;; where to shift the "+" spectrum.
dithdist=round(abs(dithpos-dithpos2)/0.2) ;;; distance between two dithering positions in pixels

log_telluric=readlog(logfilelist[telluric_dir_idx])
rdir_telluric=def_wdir(logfilelist[telluric_dir_idx])

mkdir,wdir

if(keyword_set(corr_tell)) then begin
    n_tel=0
    ttcnt=1
    while(ttcnt gt 0) do begin
        stmp=sxpar(log_telluric,'STAR'+string(n_tel+1,format='(i2.2)'),count=ttcnt)
        if(ttcnt eq 1) then n_tel=n_tel+1        
    endwhile

    tel_corr_files=file_search(rdir_telluric+'/correction_tel_*.fits',count=cnt_tel)
    if(cnt_tel eq 0) then begin
        print,'Telluric correction files not found in '+rdir_telluric+'. Cannot continue combine_2d_pairs.'
        status=8
        return
    endif

    if(max(num_tel) gt n_tel or min(num_tel) lt 1) then begin
        print,'No telluric star with the specified number exists. Cannot continute combine_2d_pairs.'
        status=16
        return
    endif

    n_good_tel=0
    for i=0,n_elements(num_tel)-1 do begin
        fname_telcorr = rdir_telluric+'/correction_tel_'+string(num_tel[i],format='(i2.2)')+'.fits'
        if(file_test(fname_telcorr)) then begin
            n_good_tel++
            file_copy,fname_telcorr,wdir,/overwrite
    ;        print,'Reading '+wdir+'/correction_tel_'+string(i,format='(i2.2)')+'.fits'
            tell_cur=mrdfits(fname_telcorr,1,h_tell)
            parse_spechdr,h_tell,wl=wl_tell
            atm_model_tab=mrdfits(fname_telcorr,2)
            if(n_elements(tell_arr) eq 0) then begin
                tell_arr=dblarr(sxpar(h_tell,'NAXIS1'),n_elements(num_tel))
                airmass_arr=dblarr(n_tel)
                water_vapor_arr=dblarr(n_tel)
            endif
            tell_arr[*,i]=tell_cur
            airmass_arr[i]=sxpar(h_tell,'AIRMASS')
            water_vapor_arr[i]=sxpar(h_tell,'WVAPOR_M')
            suffix='_corr'
        endif
    endfor
    if(n_good_tel eq 0) then begin
        print,'No suitable telluric files found in '+rdir_telluric+'. Cannot continue combine_2d_pairs.'
        return
    endif
endif else suffix=''


file_delete,wdir+'/mask_mos.txt',/allow_nonexistent
file_copy,rdir_telluric+'/mask_mos.txt',wdir,/overwrite
mask=read_mask_mmirs_ms(wdir+'/mask_mos.txt')
boxstr = (keyword_set(target))? 'TARGET' : 'BOX'
box0=(box_idx[0] ge 0)? (where(mask.type eq boxstr))[box_idx] : -1
if(keyword_set(anyslit) and box_idx[0] ge 0) then box0=box_idx-1
n_box=n_elements(box_idx)

sxaddpar,log_telluric,'DITHPOS',dith0*0.2,' current dithering position in arcsec'
sxaddpar,log_telluric,'DITHPOS2',-dith0*0.2,' dithering position for the second frame'
fxaddpar,log_telluric,'W_DIR',wdir
sxaddpar,log_telluric,'EXTDFBOX',0,' use alignbox star positions to get empirical dithering offset?'
for i=1,8 do sxaddpar,log_telluric,'S'+string(i,format='(i2.2)')+'PROC',0

openw,u,wdir+'comb_2d_log.txt',/get_lun
printf,u,log_telluric
close,u
free_lun,u

if(keyword_set(corr_tell)) then begin
    tell_corr_arr=dblarr(n_elements(wl_tell),n_exp)
endif

exptime=0.0
exposure_table = replicate({n_pair:-1L,n_exp:-1L,filename:'',date_obs:'',exptime:!values.f_nan,dithpos:!values.f_nan,seeing:!values.f_nan,weight:!values.f_nan},n_exp*2)
weights=dblarr(n_exp)+1d
n_slit=n_elements(mask)
slit_yoff=fltarr(n_slit,n_exp)  ;; differential slit offsets with respect to the reference align box

for x=0,n_slit-1 do begin
    print,'Processing Slit #'+string(x+1,format='(i2)')
    for i=0,n_exp-1 do begin
        rdir_cur=def_wdir(logfilelist[i])

        if(x eq 0 and keyword_set(weight_by_seeing)) then begin
            h_sci=headfits(rdir_cur+'/obj_dark.fits',ext=1)
            seeing_cur=(float(sxpar(h_sci,'SEEING'))) > 0.2
            weights[i]=1.0/seeing_cur^2
        endif

        fname=(keyword_set(nokelson))? rdir_cur+'/obj_diff_slits_lin.fits' : rdir_cur+'/obj_diff-sky_slits_lin.fits'
        print,'FILE: ',fname+'    ',format='(2a,%"\r",$)'
        flin=rdir_cur+$
                 '/err_obj_diff_slits_lin.fits'
                ;'/obj_slits_lin.fits'
        fssky=rdir_cur+$
                '/obj-sky_slits_lin.fits'
        log_cur=readlog(logfilelist[i])

        h_img_cur=headfits(fname,exten=x+1)

        if(box0[0] ne -1) then begin
            hbox0=headfits(fssky,exten=box0[0]+1)
            box0_yoff=sxpar(hbox0,'YOFFSET')

            ;;; computing a relative offset with respect to the alignment box and correcting for possible differences
            slit_yoff[x,i]=sxpar(h_img_cur,'YOFFSET')-box0_yoff
            dy_slitoff=(slit_yoff[x,i]-slit_yoff[x,0])

            if(x eq 0) then begin
                for b=0,n_box-1 do begin
                    rbox0=mrdfits(fssky,box0[b]+1,hbox0,/silent)
                    rdiff=mrdfits(fname,box0[b]+1,/silent)
                    nx_box=sxpar(hbox0,'NAXIS1')
                    ny_box=sxpar(hbox0,'NAXIS2')
                    if(b eq 0) then begin
                        parse_spechdr,hbox0,wl=wl_box
                        flag_oh=mask_oh_lines(wl_box,5.0)
                    endif
                    flag_arr=double(rebin(flag_oh,nx_box,ny_box))
                    rbox0/=(1d -flag_arr)
                    rdiff/=(1d -flag_arr)
                    slit_yoff[box0[b],i]=sxpar(hbox0,'YOFFSET')-box0_yoff
                    dy_slitoff_box=(slit_yoff[box0[b],i]-slit_yoff[box0[b],0])
                    dithcomp = sxpar(hbox0,'DITHCOMP')/0.2
                    diffprof=shift(median(rdiff[0.25*nx_box:0.75*nx_box,*],dim=1),-dy_slitoff_box)
                    bdp=where(finite(diffprof) ne 1,cbdp,compl=gdp,ncompl=cgdp)

                    if(cbdp gt 0) then diffprof[bdp]=0.0
                    if(cgdp gt 6) then begin
                        if(abs(diffprof[gdp[0]]) gt 0.2*max(abs(diffprof[gdp]))) then diffprof[gdp[0]]=0.0
                        if(abs(diffprof[gdp[1]]) gt 0.3*max(abs(diffprof[gdp]))) then diffprof[gdp[1]]=0.0
                        if(abs(diffprof[gdp[cgdp-2]]) gt 0.3*max(abs(diffprof[gdp]))) then diffprof[gdp[cgdp-2]]=0.0
                        if(abs(diffprof[gdp[cgdp-1]]) gt 0.2*max(abs(diffprof[gdp]))) then diffprof[gdp[cgdp-1]]=0.0
                    endif
                    diffprof=smooth(diffprof,3,/nan)
                    if(i eq 0) then begin
                        if(b eq 0) then begin
                            dithcomp_arr=fltarr(n_exp,n_box)
                            dithcomp_slits=fltarr(n_exp,n_slit)
                            diffprof_ref=dblarr(n_elements(diffprof),n_box)
                            dithcomp_g1=fltarr(n_exp,n_box)
                            dithcomp_g2=fltarr(n_exp,n_box)
                            dithcomp_g1_cur=fltarr(n_exp,n_slit)
                            dithcomp_g2_cur=fltarr(n_exp,n_slit)
                        endif
                        dpr_len = ((n_elements(diffprof_ref[*,b])) < (n_elements(diffprof))) -1
                        dpr_shift = ((n_elements(diffprof_ref[*,b]) - n_elements(diffprof))-1)/2
;print,'DPR_SHIFT=',dpr_shift
                        diffprof_ref[0:dpr_len,b]=(shift(diffprof,dpr_shift))[0:dpr_len]
                        dithcomp_arr[i,b]=sxpar(log_first,'DITHPOS')/0.2
                    endif else begin
                        x_xc=-5+findgen(11)
                        dp_len = ((n_elements(diffprof_ref[*,b])) < (n_elements(diffprof))) -1
                        dp_shift = ((n_elements(diffprof_ref[*,b]) - n_elements(diffprof))-1)/2
;print,'DP_SHIFT=',dp_shift
                        c_xc=c_correlate(diffprof_ref[0:dp_len,b],(shift(diffprof,dp_shift))[0:dp_len],x_xc)
                        maxxc=max(c_xc,x_maxxc)
                        dx_maxxc=x_xc[x_maxxc]
                        g_fit=gaussfit(x_xc,c_xc,g_coeff,nterms=4)
                        ;;;print,'i=',i,dx_maxxc,g_coeff[1]
                        ;;;print,''
                        dx_maxxc = -5 > dx_maxxc < 5
;                        dithcomp_arr[i]=round(sxpar(log_first,'DITHPOS')/0.2 +dx_maxxc)
                        dithcomp_arr[i,b]=sxpar(log_first,'DITHPOS')/0.2+dx_maxxc
                    endelse
                    
                    w_prof=dblarr(ny_box)
                    w_prof[0:ny_box*0.6]=1
;                    g1=gaussfit(findgen(ny_box),diffprof*w_prof,ag1,nterms=3)
                    g1=gaussfit(findgen(ny_box),((diffprof*w_prof)<0),ag1,nterms=3)
                    dithcomp_g1[i,b]=ag1[1]
;                    g2=gaussfit(findgen(ny_box),diffprof*(1d -w_prof),ag2,nterms=3)
                    g2=gaussfit(findgen(ny_box),((diffprof*reverse(w_prof))>0),ag2,nterms=3)
                    dithcomp_g2[i,b]=ag2[1]
    ;                dithcomp_arr[i]=round(sxpar(log_first,'DITHPOS')/0.2 + (dithcomp_g2[i]-dithcomp_g2[0]))
                    dithcomp_arr[i,b]=(sxpar(log_first,'DITHPOS')/0.2+ (dithcomp_g2[i,b]-dithcomp_g2[0,b]))
                endfor
                if(n_box gt 0 and n_box lt 3) then begin
                    dithcomp_g1_cur=rebin(dithcomp_g1[*,0],n_exp,n_slit)
                    dithcomp_g2_cur=rebin(dithcomp_g2[*,0],n_exp,n_slit)
                endif else begin
                    inp_mtx=dblarr((4 < (n_box-1)),n_box)+1d
                    inp_mtx[1,*]=reform(mask[box0].x,1,n_box) ;; rotation
                    if(n_box gt 3) then inp_mtx[2,*]=reform(mask[box0].y,1,n_box) ;; stretch
                    if(n_box gt 4) then inp_mtx[3,*]=reform(mask[box0].x*mask[box0].y,1,n_box) ;; cross-term

;                    sol_dithcomp=la_least_squares(inp_mtx,(transpose(dithcomp_g2[i,*]-dithcomp_g2[0,*])),/double,resid=aaa)

                    sol_dithcomp=la_least_squares(inp_mtx,(transpose(dithcomp_arr[i,*]-dithcomp_arr[0,*])),/double,resid=aaa)
                    dithcomp_slits[i,*]=dithcomp_arr[0,0]+transpose(sol_dithcomp[0]+mask.x*sol_dithcomp[1])
                    if(n_box gt 3) then dithcomp_slits[i,*]+=transpose(mask.y*sol_dithcomp[2])
                    if(n_box gt 4) then dithcomp_slits[i,*]+=transpose(mask.x*mask.y*sol_dithcomp[3])

                    sol_dithcomp_g1=la_least_squares(inp_mtx,(transpose(dithcomp_g1[i,*]-dithcomp_g1[0,*])),/double)
                    dithcomp_g1_cur[i,*]=dithcomp_g1[0,0]+transpose(sol_dithcomp_g1[0]+mask.x*sol_dithcomp_g1[1])
                    if(n_box gt 3) then dithcomp_g1_cur[i,*]+=transpose(mask.y*sol_dithcomp_g1[2])
                    if(n_box gt 4) then dithcomp_g1_cur[i,*]+=transpose(mask.x*mask.y*sol_dithcomp_g1[3])

                    sol_dithcomp_g2=la_least_squares(inp_mtx,(transpose(dithcomp_g2[i,*]-dithcomp_g2[0,*])),/double)
                    dithcomp_g2_cur[i,*]=dithcomp_g2[0,0]+transpose(sol_dithcomp_g2[0]+mask.x*sol_dithcomp_g2[1])
                    if(n_box gt 3) then dithcomp_g2_cur[i,*]+=transpose(mask.y*sol_dithcomp_g2[2])
                    if(n_box gt 4) then dithcomp_g2_cur[i,*]+=transpose(mask.x*mask.y*sol_dithcomp_g2[3])

                    if(keyword_set(verbose)) then begin
                        print,''
                        print,'i,x,Dithcomp:',i,x,dithcomp,sol_dithcomp ;;dithcomp_g1_cur[i,x],dithcomp_g2_cur[i,x]
                        print,'Measured:',transpose(dithcomp_arr[i,*])
                        resid=transpose(dithcomp_arr[i,*])-dithcomp_arr[0,0]-transpose(sol_dithcomp[0]+mask[box0].x*sol_dithcomp[1])
                        if(n_box gt 3) then resid-=transpose(mask[box0].y*sol_dithcomp[2])
                        if(n_box gt 4) then resid-=transpose(mask[box0].x*mask[box0].y*sol_dithcomp[3])
                        print,'Resid:   ',resid,aaa
;                        print,transpose(dithcomp_arr[i,*])-dithcomp_arr[0,0]-transpose(sol_dithcomp[0]+mask[box0].x*sol_dithcomp[1]+mask[box0].y*sol_dithcomp[2]),aaa
    ;                    print,'i,x,Dithcomp1:',i,x,dithcomp,sol_dithcomp_g1
    ;                    print,'i,x,Dithcomp2:',i,x,dithcomp,sol_dithcomp_g2
                    endif
                endelse
            endif
            if(n_box eq 1) then dithcomp=dithcomp_arr[i,0] else dithcomp=dithcomp_slits[i,x]
        endif else begin
            box0_yoff = 0

            ;;; computing a relative offset with respect to the alignment box and correcting for possible differences
            slit_yoff[x,i]=sxpar(h_img_cur,'YOFFSET')-box0_yoff
            dy_slitoff=(slit_yoff[x,i]-slit_yoff[x,0])

            dithcomp = sxpar(log_cur,'DITHPOS')/0.2+dy_slitoff
            dithcomp_g1_cur=fltarr(n_exp,n_slit)+sxpar(log_cur,'DITHPOS')/0.2+dy_slitoff
            dithcomp_g2_cur=fltarr(n_exp,n_slit)+sxpar(log_cur,'DITHPOS2')/0.2+dy_slitoff
        endelse
;        if(keyword_set(verbose)) then print,'n_exp=',i,' dithpos(pix)=',dithcomp
    
        h_pri_cur=headfits(fname)
        exptime_cur=sxpar(h_pri_cur,'EXPTIME') ;*2.0 SM remove this factor of 2?
        airmass_cur=sxpar(h_pri_cur,'AIRMASS')

;        im_tmp = shift(mrdfits(fname,x+1,h_img_cur,/silent)*exptime_cur,0,round(-dithcomp+dith0))*weights[i]
;        imlin_tmp = shift(mrdfits(flin,x+1,/silent)*exptime_cur,0,round(-dithcomp+dith0))*weights[i]
        im_tmp = shift_image(mrdfits(fname,x+1,h_img_cur,/silent)*exptime_cur,0,(-dithcomp-dy_slitoff+dith0))*weights[i]
        imlin_tmp = shift_image(mrdfits(flin,x+1,/silent)*exptime_cur,0,(-dithcomp-dy_slitoff+dith0))*weights[i]

;        ;;; computing a relative offset with respect to the alignment box and correcting for possible differences
;        slit_yoff[x,i]=sxpar(h_img_cur,'YOFFSET')-box0_yoff
;        dy_slitoff=(slit_yoff[x,i]-slit_yoff[x,0])
;        im_tmp = shift(im_tmp,0,-dy_slitoff)
;        imlin_tmp = shift(im_tmp,0,-dy_slitoff)
        if(keyword_set(verbose)) then print,'slit,exp,total_shift=',x,i,(-dithcomp+dith0)-dy_slitoff

        if(keyword_set(corr_tell)) then begin
            water_vapor_cur=water_vapor_arr[0]
            if(x eq 0) then begin
                tell_corr=interp_telluric_correction(tell_arr,airmass_arr,airmass_cur,water_vapor_arr,water_vapor_cur,atm_model_tab=atm_model_tab)
                tell_corr_arr[*,i]=tell_corr
            endif else tell_corr=tell_corr_arr[*,i]
            for y=0,sxpar(h_img_cur,'NAXIS2')-1 do begin
                im_tmp[*,y]=im_tmp[*,y]/tell_corr
                imlin_tmp[*,y]=imlin_tmp[*,y]/tell_corr
            endfor
        endif
        if(i eq 0) then begin
            h_out_pri=h_pri_cur
            h_out_ext=h_img_cur
            h_out_ext_cube=h_img_cur
            sxaddpar,h_out_pri,'NCOMEXP',n_exp*2,'number of combined exposures'
            sxaddpar,h_out_pri,'NCOMPAIR',n_exp,'number of combined dithered pairs'
            sxdelpar,h_out_pri,'BUNIT'
            sxaddpar,h_out_pri,'BUNIT','counts'
            sxaddpar,h_out_ext_cube,'NAXIS',3
            sxaddpar,h_out_ext_cube,'NAXIS3',n_exp,after='NAXIS2'
            im=im_tmp
            im_cube=fltarr(sxpar(h_img_cur,'NAXIS1'),sxpar(h_img_cur,'NAXIS2'),n_exp)
            imlin_cube=fltarr(sxpar(h_img_cur,'NAXIS1'),sxpar(h_img_cur,'NAXIS2'),n_exp)
            imlin=imlin_tmp^2.0 ;sum the variances, not the errors!
        endif else begin
           ;im=im+im_tmp ;;new IDL truncates *im* if im_tmp is smaller!!!
            ymin=0
            ymax=(n_elements(im[0,*]) < n_elements(im_tmp[0,*]))-1    
            im[*,ymin:ymax]=im[*,ymin:ymax]+im_tmp[*,ymin:ymax]
            imlin[*,ymin:ymax]=imlin[*,ymin:ymax]+(imlin_tmp[*,ymin:ymax])^2.0 ;sum the variances, not the errors!
        endelse
        ymin=0
        ymax=(n_elements(im_cube[0,*,i]) < n_elements(im_tmp[0,*]))-1 ;;; truncating the larger image in the Y direction
        im_cube[*,ymin:ymax,i]=im_tmp[*,ymin:ymax]
        imlin_cube[*,ymin:ymax,i]=imlin_tmp[*,ymin:ymax]
        if(x eq 0) then begin
            exptime+=exptime_cur
            exposure_table[i*2].n_pair=i+1
            exposure_table[i*2+1].n_pair=i+1
            exposure_table[i*2].n_exp=2*i+1
            exposure_table[i*2+1].n_exp=2*i+2
            exposure_table[i*2].filename=sxpar(log_cur,'SCI')
            exposure_table[i*2+1].filename=sxpar(log_cur,'SCI2')
            h_sci=headfits(rdir_cur+'/obj_dark.fits',ext=1)
            h_sci2=headfits(rdir_cur+'/'+sxpar(log_cur,'SCI2')+'_dark.fits',ext=1)
            exposure_table[i*2].exptime=sxpar(h_sci,'EXPTIME')
            exposure_table[i*2+1].exptime=sxpar(h_sci2,'EXPTIME')
            exposure_table[i*2].date_obs=sxpar(h_sci,'DATE-OBS')
            exposure_table[i*2+1].date_obs=sxpar(h_sci2,'DATE-OBS')
            exposure_table[i*2].dithpos=sxpar(log_cur,'DITHPOS')
            exposure_table[i*2+1].dithpos=sxpar(log_cur,'DITHPOS2')
            exposure_table[i*2].seeing=sxpar(h_sci,'SEEING')
            exposure_table[i*2+1].seeing=sxpar(h_sci,'SEEING')
            exposure_table[i*2].weight=weights[i]
            exposure_table[i*2+1].weight=weights[i]
        endif
     endfor
     ;SM add normalize the weights
    im=im/total(weights)*n_elements(weights) ;finish the weighted sum in cnts
    for kk=0,n_exp-1 do begin
       im_cube[*,*,kk]/=weights[kk] ;back to raw exposures in cnts
       imlin_cube[*,*,kk]/=weights[kk] ;back to raw errors in cnts
    endfor
    ;im_cube=im_cube/total(weights)*n_elements(weights)
    imlin=imlin/total(weights)^2.0*n_elements(weights)^2.0 ;weighted sum of variances
    ;imlin_cube=imlin_cube/total(weights)*n_elements(weights)
    ;weights=weights/total(weights)*n_elements(weights)
    print,''
    if(keyword_set(clean) and (n_exp ge 3)) then begin
        nsig=5.0
        im_cur=im
        s_im=size(im)

        im_med=median(im_cube,dim=3)*n_elements(weights) ;median*n_exposures for total counts
        im_bad=where(finite(im_cur) ne 1,cim_bad)
        if(cim_bad gt 0) then im_cur[im_bad]=im_med[im_bad]
        ;for kk=0,n_exp-1 do im_cube[*,*,kk]/=weights[kk]
        ;im_cube=im_cube*total(weights)/n_elements(weights)
        im_cur_cube=congrid(reform(im_cur/n_elements(weights),s_im[1],s_im[2],1),s_im[1],s_im[2],n_exp)
        ;im_stdev=sqrt(total((im_cube-im_cur_cube)^2,3,/nan)/(n_exp-1.0))
        im_stdev=stddev(im_cube,dim=3,/nan)
        im_stdev_cube=congrid(reform(im_stdev,s_im[1],s_im[2],1),s_im[1],s_im[2],n_exp)

        badpix_cube=where(abs(im_cube-im_cur_cube) gt nsig*im_stdev_cube,cbadpix_cube)
        if(cbadpix_cube gt 0) then begin
            print,'Slit #',string(x+1,format='(i3)'),', the number of rejected pixels:',string(cbadpix_cube,format='(i)')
            im_cube[badpix_cube]=!values.f_nan
            im_stdev=stddev(im_cube,dim=3,/nan)*sqrt(n_elements(weights)) ;stdev of final sum
            im_cube[badpix_cube]=im_cur_cube[badpix_cube]
            for kk=0,n_exp-1 do im_cube[*,*,kk]*=weights[kk]
            im_cube=im_cube/total(weights)*n_elements(weights)
            im=total(im_cube,3) ;new weighted sum with rejection
            for kk=0,n_exp-1 do im_cube[*,*,kk]/=weights[kk]
            im_cube=im_cube*total(weights)/n_elements(weights) ;restore orig im_cube
        endif else begin
            ;for kk=0,n_exp-1 do im_cube[*,*,kk]*=weights[kk] SM
            im_stdev=stddev(im_cube,dim=3,/nan)*sqrt(n_elements(weights)) ;sttdev of final sum
        endelse
    endif

    yc_cur=(n_elements(im[0,*])-1)/2.0
    yim_max=(yc_cur+dithdist-1) < (n_elements(im[0,*])-1)
;;;;; old working version
;    im_all=im[*,yc_cur:yim_max]-(shift(im,0,dithdist))[*,yc_cur:yim_max] ;;;;im[*,yc_cur-dithdist:yc_cur-1]
;    imlin_all=imlin[*,yc_cur:yim_max]-(shift(imlin,0,dithdist))[*,yc_cur:yim_max] ;;;;imlin[*,yc_cur-dithdist:yc_cur-1]
;;;;;

;;;;;;  - + - version ;;;;;;;
    im_cube_all=fltarr(n_elements(im_cube[*,0,0]),yim_max+1+dithdist,n_exp)
    im_cube_all[*,0:((n_elements(im_cube[0,*,0])-1)<(yim_max+dithdist)),*]=im_cube[*,0:((n_elements(im_cube[0,*,0])-1)<(yim_max+dithdist)),*]
    nancube=where(finite(im_cube_all) ne 1,cnancube)
    if(cnancube gt 0) then im_cube_all[nancube]=0.0
;    for i=0,n_exp-1 do $
;        im_cube_all[*,*,i]=shift(im_cube_all[*,*,i]-(shift(im_cube_all[*,*,i],0,round(dithcomp_g2[i]-dithcomp_g1[i]))),0,-(yim_max-yc_cur)/2+1)
    for i=0,n_exp-1 do $
        im_cube_all[*,*,i]=shift(im_cube_all[*,*,i]-(shift_image(im_cube_all[*,*,i],0,(dithcomp_g2_cur[i,x]-dithcomp_g1_cur[i,x]))),0,-(yim_max-yc_cur)/2+1)
    
    for kk=0,n_exp-1 do im_cube_all[*,*,kk]*=weights[kk]
    im_cube_all=im_cube_all/total(weights)*n_elements(weights)
    im_all=(n_exp eq 1)? im_cube_all : total(im_cube_all,3)
    for kk=0,n_exp-1 do im_cube_all[*,*,kk]/=weights[kk]
    im_cube_all=im_cube_all*total(weights)/n_elements(weights) ;back to raw shift+sum exposures

    if(n_exp ge 3) and (dithdist gt 0.0) then begin
       s_ca=size(im_cube_all)
       ;im_all_stdev=sqrt(total((im_cube_all-congrid(reform(im_all,s_ca[1],s_ca[2],1)/s_ca[3],s_ca[1],s_ca[2],s_ca[3]))^2,3)/(n_exp-1))
       im_all_stdev=stddev(im_cube_all,dimen=3,/nan)*sqrt(n_elements(weights))
    endif

;    im_all=fltarr(n_elements(im[*,0]),yim_max+1+dithdist)
;    im_all[*,0:n_elements(im[0,*])-1]=im
;    nanim=where(finite(im_all) ne 1,cnanim)
;    if(cnanim gt 0) then im_all[nanim]=0.0
;    im_all=shift(im_all-(shift(im_all,0,dithdist)),0,-(yim_max-yc_cur)/2+1)

    imlin_all=fltarr(n_elements(imlin[*,0]),yim_max+1+dithdist)
    imlin_all[*,0:((n_elements(imlin[0,*])-1)<(yim_max+dithdist))]=imlin[*,0:((n_elements(imlin[0,*])-1)<(yim_max+dithdist))]
    nanimlin=where(finite(imlin_all) ne 1,cnanimlin)
    if(cnanimlin gt 0) then imlin_all[nanimlin]=0.0
    imlin_all=shift(imlin_all+(shift(imlin_all,0,dithdist)),0,-(yim_max-yc_cur)/2+1)
;;;;;;;

    if(dithpos lt dithpos2) then begin
        im_all=-im_all
        ;imlin_all=-imlin_all
    endif

    ;im=im/total(weights)*n_elements(weights) ;SM moved up
    imlin=sqrt(abs(imlin)) ;/total(weights)*n_elements(weights) ;do the sqrt here to preserve S/N with weighting
    ;im_all=im_all/total(weights)*n_elements(weights)
    imlin_all=sqrt(abs(imlin_all));/total(weights)*n_elements(weights)

    if(x eq 0) then begin
        sxaddpar,h_out_pri,'EXPTIME',exptime
        sxaddpar,h_out_pri,'DATAPROD',((suffix eq '_corr')? 'Telluric corrected' : 'Uncorrected')+' flux'
        writefits,wdir+'/sum_obj-sky_slits'+suffix+'.fits',0,h_out_pri
        writefits,wdir+'/all_obj-sky_slits'+suffix+'.fits',0,h_out_pri
        sxaddpar,h_out_pri,'EXPTIME',exptime*2.0
        writefits,wdir+'/sum_all_obj-sky_slits'+suffix+'.fits',0,h_out_pri
        sxaddpar,h_out_pri,'DATAPROD',((suffix eq '_corr')? 'Telluric corrected' : 'Uncorrected')+' flux uncertainty'
        writefits,wdir+'/err_sum_all_obj-sky_slits'+suffix+'.fits',0,h_out_pri
        if keyword_set(verbose) then writefits,wdir+'/err_alt_sum_all_obj-sky_slits'+suffix+'.fits',0,h_out_pri
        sxaddpar,h_out_pri,'EXPTIME',exptime
        writefits,wdir+'/err_sum_obj-sky_slits'+suffix+'.fits',0,h_out_pri
        if keyword_set(verbose) then writefits,wdir+'/err_alt_sum_obj-sky_slits'+suffix+'.fits',0,h_out_pri
        writefits,wdir+'/err_all_obj-sky_slits'+suffix+'.fits',0,h_out_pri
    endif
    mwrfits,im,wdir+'/sum_obj-sky_slits'+suffix+'.fits',h_out_ext,/silent
    ;im_stdev = (n_elements(im_stdev) gt 1)? im_stdev : float(sqrt(abs(imlin)*gain+sxpar(h_out_pri,'RDNOISE')^2)/gain)
    im_stdev = ((n_elements(im_stdev) gt 1) and (n_exp ge 5))? im_stdev : float(imlin)
    ;im_stdev=float(imlin)
    mwrfits,im_stdev,$
        wdir+'/err_sum_obj-sky_slits'+suffix+'.fits',h_out_ext,/silent
    if keyword_set(verbose) then mwrfits,imlin,$
        wdir+'/err_alt_sum_obj-sky_slits'+suffix+'.fits',h_out_ext,/silent

    mwrfits,im_cube,wdir+'/all_obj-sky_slits'+suffix+'.fits',h_out_ext_cube,/silent
    ;mwrfits,float(float(sqrt(abs(imlin_cube)*gain+sxpar(h_out_pri,'RDNOISE')^2)/gain)),$
    mwrfits,float(imlin_cube),$
        wdir+'/err_all_obj-sky_slits'+suffix+'.fits',h_out_ext_cube,/silent

    mwrfits,im_all,wdir+'/sum_all_obj-sky_slits'+suffix+'.fits',h_out_ext,/silent
    ;im_all_stdev = (n_elements(im_all_stdev) gt 1)? im_all_stdev : float(sqrt(abs(imlin_all)*gain+sxpar(h_out_pri,'RDNOISE')^2)/gain)
    im_all_stdev = ((n_elements(im_all_stdev) gt 1) and (n_exp ge 5))? im_all_stdev : float(imlin_all)
    mwrfits,im_all_stdev,$
        wdir+'/err_sum_all_obj-sky_slits'+suffix+'.fits',h_out_ext,/silent
    if keyword_set(verbose) then mwrfits,imlin_all,$
        wdir+'/err_alt_sum_all_obj-sky_slits'+suffix+'.fits',h_out_ext,/silent
endfor

writefits,wdir+'/exposure_table.fits',0
mwrfits,exposure_table,wdir+'/exposure_table.fits',/silent
log = readlog(wdir+'comb_2d_log.txt')
estimate=sxpar(log, 'EXTR_EST')
n_apwmax=sxpar(log, 'N_APWMAX')
;if (estimate and not n_apwmax) then n_apwmax=4.5
mmirs_extract_1d_slits,wdir+'comb_2d_log.txt','sum_obj-sky',suffix=suffix,writesuff=suffix,/detect,estimate=estimate,n_apwmax=n_apwmax,fitprofile=fitprofile,/diffmode, /split1d

;;; Euro3D output
mmirs_slits_to_e3d,wdir+'/sum_all_obj-sky_slits'+suffix+'.fits',$
        wdir+'/sum_all_obj-sky_slits'+suffix+'_e3d.fits',$
        err=wdir+'/err_sum_all_obj-sky_slits'+suffix+'.fits',$
        mask=wdir+'/mask_mos.txt'

mmirs_extracted_to_e3d,wdir+'/sum_obj-sky_slits_extr'+suffix+'.fits',$
        wdir+'/sum_obj-sky_slits_extr'+suffix+'_e3d.fits',$
        mask=wdir+'/mask_mos.txt'


end
