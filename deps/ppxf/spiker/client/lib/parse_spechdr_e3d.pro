;-------------------------------------
; parsing spectral WCS from Euro3D
;-------------------------------------

pro parse_spechdr_e3d,spheader,wl=wl,nlam=nlam,islog=islog,velscale=velscale
islog=0
velScale=-1.0
nlam=sxpar(spheader,'TFORM10')
bintype=strcompress(sxpar(spheader,'BINTYPE'),/remove_all)
w0=sxpar(spheader,'CRVALS')
disp=sxpar(spheader,'CDELTS')
if(disp eq 0.0) then disp=sxpar(spheader,'CD1_1')
ctype1=strcompress(string(sxpar(spheader,'CTYPE1')),/remove_all)
if((ctype1 eq 'WAVE-LOG') or (ctype1 eq 'WAVE-ALOG') or (ctype1 eq 'LINEAR')) then begin
    wl=10^(w0+disp*dindgen(nlam))
    islog=1
    velScale=disp*299792.458d*alog(10)
endif else begin
    if(bintype ne 'log') then begin
        wl=w0+disp*dindgen(nlam)
    endif else begin
        wl=w0*exp(disp*findgen(nlam)/299792.458d)
        velScale=disp
        islog=1
    endelse
endelse
end

