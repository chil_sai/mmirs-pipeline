function shift_s,vector,dx0

dx=dx0
ax=0
in=shift(vector,rfix(dx))
dx=dx-rfix(dx)
if dx lt 0 then ax=in -shift(in,-1)
if dx gt 0 then ax=shift(in,1)-in
in=in+ax*dx
return,in

end
