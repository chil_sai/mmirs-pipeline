pro mmirs_create_sky_ms,logfile,image_type,clipwl=clipwl,$
    objpos=objpos,objbox=objbox,nstages=nstages,$
    gain=gain,rdnoise=rdnoise,$
    sky_reg_slit=sky_reg_slit,$
    per_slit=per_slit,adj=adj,$
    mask_slit_individual=mask_slit_individual,$
    dimensions=dimensions,targbksp=targbksp,boxbksp=boxbksp,$
    skydegx=skydegx_inp,skydegy=skydegy_inp,$
    skylinecorr=skylinecorr,$
    nomask=nomask,debug=debug,status=status,_Extra=extra_kw

if(n_elements(gain) ne 1) then gain=1.0
if(n_elements(rdnoise) ne 1) then rdnoise=3.0
if(n_elements(clipwl) ne 1) then clipwl=0.0
if(n_params() eq 1) then image_type='obj'
if(n_elements(skydegx_inp) ne 1) then skydegx_inp=4
skydegx=skydegx_inp
if(n_elements(skydegy_inp) ne 1) then skydegy_inp=4
skydegy=skydegy_inp
if(n_elements(dimensions) ne 1) then dimensions=3
if(skydegx eq 0) then dimensions=2
if(n_elements(nstages) ne 1) then nstages=1

status=0

wdir=def_wdir(logfile)
instrument=def_inst(logfile)
f_obj=wdir+image_type+'_slits.fits'
f_aobj=(image_type eq 'obj_diff')? wdir+'obj_slits.fits' : f_obj
f_flat=wdir+'flatn_slits.fits'
if(keyword_set(skylinecorr)) then f_flat_line_corr=wdir+'flat_emline_slits.fits'
h_pri=headfits(f_obj)

dist_map=mrdfits(wdir+'dist_map.fits',1,/silent)
mask=read_mask_mmirs_ms(wdir+'mask_mos.txt',logfile=logfile)

log=readlog(logfile)
bright=sxpar(log,'BRIGHT')
slit_id = def_slit(logfile)

h_a=headfits(wdir+'obj_dark.fits',ext=1)
grism=def_grism(logfile,filter=filter)

if(grism eq 'HK') then begin
;    wlmin=12440.0
;    wlmax=24000.0
    if(n_elements(targbksp) ne 1) then targbksp=2.0
    if(n_elements(boxbksp) ne 1) then boxbksp=6.0
endif

if(grism eq 'J') then begin
;    wlmin=9600.0
;    wlmax=14300.0
    if(n_elements(targbksp) ne 1) then targbksp=0.8
    if(n_elements(boxbksp) ne 1) then boxbksp=2.0
endif

if(grism eq 'H') then begin
;    wlmin=14000.0
;    wlmax=18000.0
    if(n_elements(targbksp) ne 1) then targbksp=1.0 ;;; 0.3
    if(n_elements(boxbksp) ne 1) then boxbksp=3.0 ;;; 0.7
endif

if(grism eq 'H3000') then begin
    if(n_elements(targbksp) ne 1) then targbksp=1.0 ;;; 0.3
    if(n_elements(boxbksp) ne 1) then boxbksp=3.0 ;;; 0.7
endif

if(grism eq 'K3000') then begin
    if(n_elements(targbksp) ne 1) then targbksp=1.0 ;;; 0.3
    if(n_elements(boxbksp) ne 1) then boxbksp=3.0 ;;; 0.7
endif

if(clipwl lt 0.01) then nstages=1
for stg=0,0+2*(nstages-1) do begin
    writesuff=(stg eq 0)? '_cnt' : (stg eq 1)? '_blue' : '_red'
    if(nstages eq 1) then writesuff=''
    file_out=wdir+'/sky_'+image_type+'_2d_bspl_slits'+writesuff+'.fits'
    writefits,file_out,0,h_pri

    if(stg gt 0) then begin
        ;;dimensions=2
        skydegx = 0 ;;-=1
        skydegx = (skydegx>0)
        if(skydegx lt 1) then dimensions=2
        ;if(skydegy gt 3) then skydegy-=1
        ;if(keyword_set(skylinecorr)) then skydegy=2
        skydegy = (skydegy>0)
;        clipwl=75.0
    endif

    mask_cur=mask
    ff_file=file_search(wdir+image_type+'_ff.fits')
    if ff_file[0] ne '' then $
       h_obj=headfits(wdir+image_type+'_ff.fits') else $
       h_obj=headfits(wdir+image_type+'_diff_ff.fits')          

        nx=long(sxpar(h_obj,'NAXIS1'))
        ny=long(sxpar(h_obj,'NAXIS2'))

        f_dt=(keyword_set(adj))? 'disper_table_adj.fits' : 'disper_table.fits'
        disp_rel=mrdfits(wdir+f_dt,1,/silent)
        disp_rel_out=disp_rel
        skyline_files=file_search(wdir+'skyline_flux_data_*.fits')
        if(skyline_files[0] ne '') then begin
            fit_skyline=0
            sl_flux=0d
            for f_s=0,n_elements(skyline_files)-1 do begin
                skyline_flux_data_tmp=mrdfits(skyline_files[f_s],1,/silent)
                sl_flux_cur=median(skyline_flux_data_tmp.flux)
                if(sl_flux_cur gt sl_flux) then begin
                    skyline_flux_data=skyline_flux_data_tmp
                    g_skyline=where((finite(skyline_flux_data.wl) eq 1) and (finite(skyline_flux_data.x_mask) eq 1),cg_skyline)
                    if((cg_skyline gt 10) and keyword_set(fit_skyline)) then begin
                        t_wl=robust_poly_fit(double(g_skyline),skyline_flux_data[g_skyline].wl,5)
                        skyline_flux_data[g_skyline].wl=poly(double(g_skyline),t_wl)
                    endif
                    mean_wl_offset=median(skyline_flux_data.wl-skyline_flux_data.wl_line)*10d
                    sl_flux=sl_flux_cur
                endif
            endfor
            for s=0,cg_skyline-1 do begin
                disp_rel[g_skyline[s]].wl_sol[0]-=(skyline_flux_data[g_skyline[s]].wl-skyline_flux_data[g_skyline[s]].wl_line)*10d -mean_wl_offset
                disp_rel_out[g_skyline[s]].wl_sol[0]-=(skyline_flux_data[g_skyline[s]].wl-skyline_flux_data[g_skyline[s]].wl_line)*10d -mean_wl_offset
            endfor
        endif

        l_par = linearisation_params(grism,filter)
        wlclip = (clipwl gt 0 and clipwl lt 0.45 and n_elements(mask_cur) gt 1)? l_par.npix*clipwl*l_par.dwl : 0.0
        wlmin=l_par.wl_min + wlclip
        wlmax=l_par.wl_max - wlclip
        if(clipwl ge 1 and n_elements(mask_cur) gt 1) then begin ;; clipwl in percent (%)
            wllim=dblarr(2,ny)
            for y=0,ny-1 do wllim[*,y]=poly([0,4095],disp_rel[y].wl_sol)
            hist_minwl=histogram(wllim[0,where(finite(wllim[0,*]) eq 1)],nbin=10,locations=xhist_minwl)
            chist_minwl=total(hist_minwl,/cumulative)
            minwl_cut=interpol(xhist_minwl,chist_minwl,chist_minwl[9]*(100.0-clipwl)/100.0)
            hist_maxwl=histogram(wllim[1,where(finite(wllim[0,*]) eq 1)],nbin=10,locations=xhist_maxwl)
            chist_maxwl=total(hist_maxwl,/cumulative)
            maxwl_cut=interpol(xhist_maxwl,chist_maxwl,chist_maxwl[9]*(clipwl)/100.0)
            ; print,100*(maxwl_cut-minwl_cut)/(wlmax-wlmin),'% of the total wavelength range is retained'
            if(stg eq 0) then begin
                wlmin=minwl_cut>(l_par.wl_min)
                wlmax=maxwl_cut<(l_par.wl_max)
            endif
        endif

        wlmin=(wlmin>9500.0)  ;; hardcoded blue limit 
        wlmax=(wlmax<25000.0) ;; hardcoded red limit
        if(stg eq 0) then begin
            wlmin_stg0=wlmin>(l_par.wl_min)
            wlmax_stg0=wlmax<(l_par.wl_max)
        endif

        if(stg eq 1) then wlmax=wlmin_stg0+200.0*targbksp
        if(stg eq 2) then wlmin=wlmax_stg0-200.0*targbksp
;if stg eq 2 then stop

        print,'Generating sky model at wavelengths: '+string(wlmin,format='(i5)')+' and '+string(wlmax,format='(i5)')+' A'

        nslit=n_elements(mask)
        nslitoff=0

        comm_targ_sky=0
        comm_box_sky=0

        targ=where(strcompress(mask.type,/remove_all) eq 'TARGET',ctarg,compl=box,ncompl=cbox)

        s_skyreg=size(sky_reg_slit)
        if(s_skyreg[2] ne 8) then begin
            sky_reg_slit=replicate({slit:0,ss_tech:1,$
                                    sky_slits:bytarr(nslit),sky_reg:bytarr(ny)},nslit)
            targflag=bytarr(Ny)
            boxflag=bytarr(Ny)

            sky_reg_slit[*].slit=mask[*].slit
            for i=0,ctarg-1 do begin
                targflag[where(disp_rel.slit eq mask[targ[i]].slit)]=1
            endfor
            for i=0,ctarg-1 do begin
                sky_reg_slit[targ[i]].sky_reg[where((finite(disp_rel.wl_s_err) eq 1) and (targflag eq 1))]=1
                sky_reg_slit[targ[i]].sky_slits[targ]=1
            endfor

            for i=0,cbox-1 do begin
                sky_reg_slit[box[i]].ss_tech=0
                boxflag[where(disp_rel.slit eq mask[box[i]].slit)]=1
            endfor
            for i=0,cbox-1 do begin
                sky_reg_slit[box[i]].sky_reg[where((finite(disp_rel.y_mask) eq 1) and (boxflag eq 1))]=1
                sky_reg_slit[box[i]].sky_slits[box]=1
            endfor

            comm_targ_sky=1
            comm_box_sky=1
        endif

        sky_model_slit = {slit:0, sky_model:ptr_new()}
        sky_all = replicate(sky_model_slit,nslit)
        for i=0,nslit-1 do sky_all[i].sky_model = ptr_new(/allocate_heap)

        targ_sky_created=0
        box_sky_created=0

        ;;read,aaa
        flag_im=bytarr(Nx,Ny)

        y_scl=30.1101
        ;;if(tag_exist(dist_map,'mask_y_scl')) then y_scl=dist_map.mask_y_scl
        fl_thr=0.1

        xx=findgen(nx)

        slit_reg=get_slit_region(mask,nx=nx,ny=ny,dist_map=dist_map,slit_trace=slit_trace)

        for k=0,nslit-1 do begin
        ;    print,'k, tg_sky_c, comm_tg_sky, bx_sky_c, comm_bx_sk=',k,targ_sky_created,comm_targ_sky,box_sky_created,comm_box_sky
            if((targ_sky_created eq 1) and (comm_targ_sky eq 1) and (strcompress(mask[k].type,/remove_all) eq 'TARGET')) then begin
                *sky_all[k].sky_model=*sky_all[targ[0]].sky_model
                continue
            endif
            if((box_sky_created eq 1) and (comm_box_sky eq 1) and (strcompress(mask[k].type,/remove_all) eq 'BOX')) then begin
                *sky_all[k].sky_model=*sky_all[box[0]].sky_model
                continue
            endif

            if(sky_reg_slit[k].ss_tech eq 1) then begin ;;; Kelson-style sky subtraction
                print,'Creating an oversampled sky vector for slit=',k,' slit type=',mask[k].type
                sky_reg_cur=where(sky_reg_slit[k].sky_reg eq 1,n_sky)
                xp=dblarr(long(nx)*long(n_sky))
                yp=xp
                zp=xp
                sky_flux=xp*!values.d_nan
                asky_flux=xp*!values.d_nan

                for j=0,nslit-1 do begin
                    if(sky_reg_slit[k].sky_slits[j] eq 0) then continue

                    flat_line_slit=(keyword_set(skylinecorr))?mrdfits(f_flat_line_corr,j+1+nslitoff,/silent):1d
                    obj_slit=mrdfits(f_obj,j+1+nslitoff,hdr_slit_cur,/silent)/flat_line_slit
                    aobj_slit=(f_obj ne f_aobj)? mrdfits(f_aobj,j+1+nslitoff,/silent)/flat_line_slit : obj_slit
                    flat_slit=mrdfits(f_flat,j+1+nslitoff,/silent)
                    y_off=long(sxpar(hdr_slit_cur,'YOFFSET'))
                    ny_cur=long(sxpar(hdr_slit_cur,'NAXIS2'))
                    slit_tilt = (sxpar(hdr_slit_cur,'SLITTHET')-sxpar(hdr_slit_cur,'MASKPA')) mod 180.0

                    if(keyword_set(bright)) then begin
                        target_offset=(mask_cur[j].y-(mask_cur[j].bbox[5]+mask_cur[j].bbox[1])/2.0)*5.98802 ;; target offset in arcsec, 5.98802=scale in arcsec/mm
                        mask_targ=mask_target(logfile,slitnum=j+1+nslitoff,nmasked=nmasked,offset=target_offset,/diffmode)
                        if(nmasked gt 0) then begin
                            obj_slit[mask_targ]=!values.f_nan
                            aobj_slit[mask_targ]=!values.f_nan
                        endif
                    endif

                    if(not(keyword_set(nomask))) then begin ;;;;; masking the blinking amplifier
                        y_min_cur = y_off
                        y_max_cur = y_off + ny_cur -1
                        if(((y_min_cur ge 384) and (y_min_cur le 511)) or $
                           ((y_max_cur ge 384) and (y_max_cur le 511))) then begin
                            ymask_min = (384-y_off) > 0L
                            ymask_max = (511-y_off) < (ny_cur-1L)
                            print,'Masking the blinking channel affecting slit ',string(j+1,format='(i3)'),ymask_min,ymask_max
                            obj_slit[1024:*,ymask_min:ymask_max]=!values.f_nan
                            aobj_slit[1024:*,ymask_min:ymask_max]=!values.f_nan
                        endif
                    endif

                    wlmap_slit=obj_slit*!values.d_nan
                    xmaskmap_slit=obj_slit*!values.d_nan
                    ymaskmap_slit=obj_slit*!values.d_nan
                    for i=0L,ny_cur-1L do begin
                        if ((i+y_off) ge 0 and (i+y_off) lt Ny) then begin
                         if (total(disp_rel[i+y_off].wl_sol) eq 0.) then disp_rel[i+y_off].wl_sol=!values.d_nan
                         if(disp_rel[i+y_off].slit eq mask_cur[j].slit) then begin
                            wlmap_slit[*,i]=poly(xx,disp_rel[i+y_off].wl_sol)
                            xmaskmap_slit[*,i]=disp_rel[i+y_off].x_mask
                            ymaskmap_slit[*,i]=disp_rel[i+y_off].y_mask
                         endif
                        endif
                    endfor
                    wlmap_slit=mmirs_rectify_slit(wlmap_slit,dist_map,y_off,/edge,/inverse)
                    xmaskmap_slit=mmirs_rectify_slit(xmaskmap_slit,dist_map,y_off,/edge,/inverse)
                    ymaskmap_slit=mmirs_rectify_slit(ymaskmap_slit,dist_map,y_off,/edge,/inverse)

                    if(n_elements(mask_slit_individual) gt 0) then begin
                        mask_slit_id=where(mask_slit_individual.slit eq mask_cur[j].slit,cmask_slit_id) 
;;;                           mask_slit_individual.slit eq j,cmask_slit_id) 
                        if(cmask_slit_id eq 1) then begin
                            mask_targ=mask_target(logfile,slitnum=j+1+nslitoff,$
                                width=mask_slit_individual[mask_slit_id].width,$
                                offset=mask_slit_individual[mask_slit_id].offset,$
                                nmasked=nmasked,/diffmode)
                            if(nmasked gt 0) then begin
                                obj_slit[mask_targ]=!values.f_nan
                                aobj_slit[mask_targ]=!values.f_nan
                            endif
                        endif
                    endif

                    for i=0L,n_sky-1L do begin
;;                        xp[i*nx:(i+1)*nx-1]=poly(xx,disp_rel[sky_reg_cur[i]].wl_sol)
                        if(finite(disp_rel[sky_reg_cur[i]].x_mask) ne 1) then continue

                        cur_slit=disp_rel[sky_reg_cur[i]].slit-1l

;;                        dy = slit_trace[cur_slit].y_trace - (slit_reg[0,cur_slit]+slit_reg[1,cur_slit])/2d
;;                        dy_mask_pix=-(dy-fix(dy)*0)
;;                        yp[i*nx:(i+1)*nx-1]=disp_rel[sky_reg_cur[i]].x_mask+dy_mask_pix*tan(slit_tilt/!radeg)/y_scl
;;                        zp[i*nx:(i+1)*nx-1]=disp_rel[sky_reg_cur[i]].y_mask+dy_mask_pix/y_scl

;;                        idx=lindgen(Nx)+long(dy)*Nx+long(sky_reg_cur[i]-y_off)*Nx

                        idx=lindgen(Nx)+long(sky_reg_cur[i]-y_off)*Nx

                        good_idx=where(idx ge 0 and idx lt n_elements(obj_slit),cgood_idx)
                        if(cgood_idx gt 0) then begin
                            good_idx=where((idx ge 0) and (idx lt n_elements(obj_slit)) and (flat_slit[idx] gt fl_thr),cgood_idx)
                            if(cgood_idx gt 0) then begin
                              i_idx=(i*nx+lindgen(Nx))[good_idx]
                              sky_flux[i_idx]=obj_slit[idx[good_idx]]
                              asky_flux[i_idx]=aobj_slit[idx[good_idx]]

                              xp[i_idx]=wlmap_slit[idx[good_idx]]
                              yp[i_idx]=xmaskmap_slit[idx[good_idx]]
                              zp[i_idx]=ymaskmap_slit[idx[good_idx]]

                              flag_im[idx[good_idx]]=1
                              if(keyword_set(debug)) then begin
                                  oplot,xp[i_idx],sky_flux[i_idx],psym=-4,col=i,syms=0.3
                                  print,'slit,Y=',j,sky_reg_cur[i]
                                  ;aaa=''
                                  ;if(sky_reg_cur[i] gt 1720 and sky_reg_cur[i] lt 2650) then read,aaa
                                  ;if(aaa eq 's') then stop
                              endif
                            endif
                        endif
                    endfor
                endfor

                xs=sort(xp)
                x_sky=xp[xs]
                y_sky=yp[xs]
                z_sky=zp[xs]
                sky=sky_flux[xs]
                asky=asky_flux[xs]
                if(image_type eq 'obj_diff') then asky=asky*sqrt(2)
                isky= gain^2 / (gain * abs(asky) + rdnoise^2)

                kk=where((finite(sky+asky+isky) eq 1) and (x_sky gt wlmin) and (x_sky lt wlmax),ckk)
                if(ckk gt 512) then begin
                    x_sky=x_sky[kk]
                    y_sky=y_sky[kk]
                    z_sky=z_sky[kk]
                    sky=sky[kk]
                    isky=isky[kk]
                    n_allsky=long(n_elements(x_sky))
                    bkspace = (strcompress(mask[k].type,/remove_all) eq 'TARGET')? targbksp : boxbksp
    
                    if(keyword_set(debug)) then begin
                        aaa=''
                        read,aaa
                        plot,x_sky,sky,psym=3,xs=1,xtitle='wavelength',ytitle='sky flux',title='Slit #'+string(k,format='(i3)')
                        aaa=''
                        read,aaa
                        if(aaa eq 's') then stop
                    endif
                    npoly=skydegx ;;;;; (slit_id eq 'mos')? 2 : 3
                    n2poly=skydegy
                    if(strcompress(mask[k].type,/remove_all) eq 'TARGET') then begin
                        sset = (dimensions eq 3)? $
                            bspline_iterfit_3d(x_sky, sky, x2=y_sky, x3=z_sky, invvar=isky, npoly=npoly, n2poly=n2poly, bkspace=targbksp, _Extra=extra_kw) : $
                            bspline_iterfit(x_sky, sky, x2=z_sky, invvar=isky, npoly=npoly, bkspace=targbksp, _Extra=extra_kw)
                    endif else sset = bspline_iterfit(x_sky, sky, x2=z_sky, invvar=isky, bkspace=targbksp, _Extra=extra_kw)
    
                    if(total(sset.coeff) eq 0.0) then begin
                        message,/inf,'SKY MODEL FAILED !!!'
                        if(dimensions eq 3)  then begin
                            message,/inf,'Re-doing the sky model with dimensions=2'
                            dimensions=2
                            sset = bspline_iterfit(x_sky, sky, x2=z_sky, invvar=isky, npoly=npoly, bkspace=targbksp, _Extra=extra_kw)
                            if(total(sset.coeff) eq 0.0) then begin
                                message,/inf,'SKY MODEL FAILED WITH dimensions=2 !!!'
                                status=1
                            endif
                        endif else status=1
                    endif
                    *sky_all[k].sky_model=sset
                endif else *sky_all[k].sky_model=0.0
            endif else *sky_all[k].sky_model=0.0

            if((comm_targ_sky eq 1) and (strcompress(mask[k].type,/remove_all) eq 'TARGET')) then targ_sky_created=1
            if((comm_box_sky eq 1) and (strcompress(mask[k].type,/remove_all) eq 'BOX')) then box_sky_created=1
        endfor

        if(keyword_set(debug)) then writefits,wdir+'flag_im.fits',flag_im

        if(status eq 0) then $
            print,'Sky successfully created for all slits' else begin
            print,'Sky model failed at least in one slit'
        endelse

        sxaddpar,h_pri,'SKYSTAT',status,' status of the sky model 0=ok'

        ;;;n_sky=long(n_elements(skyreg))

        for k=0,nslit-1 do begin
            flat_line_slit=(keyword_set(skylinecorr))?mrdfits(f_flat_line_corr,k+1+nslitoff,/silent):1d
            obj_slit=mrdfits(f_obj,k+1+nslitoff,hdr_slit_cur,/silent)/flat_line_slit
            skymodel=obj_slit*0.0+!values.f_nan
            y_off=long(sxpar(hdr_slit_cur,'YOFFSET'))
            ny_cur=long(sxpar(hdr_slit_cur,'NAXIS2'))
            slit_tilt = (sxpar(hdr_slit_cur,'SLITTHET')-sxpar(hdr_slit_cur,'MASKPA')) mod 180.0
            wlmap_slit=obj_slit*!values.d_nan
            xmaskmap_slit=obj_slit*!values.d_nan
            ymaskmap_slit=obj_slit*!values.d_nan
            for i=0L,ny_cur-1L do begin
                if ((i+y_off) ge 0 and (i+y_off) lt Ny) then begin
                 if(total(disp_rel[i+y_off].wl_sol) eq 0.) then disp_rel[i+y_off].wl_sol=!values.d_nan
                 if (disp_rel[i+y_off].slit eq mask_cur[k].slit) then begin
                   wlmap_slit[*,i]=poly(xx,disp_rel[i+y_off].wl_sol)
                   xmaskmap_slit[*,i]=disp_rel[i+y_off].x_mask
                   ymaskmap_slit[*,i]=disp_rel[i+y_off].y_mask
                endif
               endif
            endfor
            wlmap_slit=mmirs_rectify_slit(wlmap_slit,dist_map,y_off,/edge,/inverse)
            xmaskmap_slit=mmirs_rectify_slit(xmaskmap_slit,dist_map,y_off,/edge,/inverse)
            ymaskmap_slit=mmirs_rectify_slit(ymaskmap_slit,dist_map,y_off,/edge,/inverse)

            print,'Slit #',k+1+nslitoff,'/',nslit+nslitoff
            if(sky_reg_slit[k].ss_tech eq 1) then begin ;;; Kelson-style sky subtraction
                print,'Creating 2D sky model... ',format='(a,$)'
                if((size(*sky_all[k].sky_model))[2] eq 8) then begin
                    if((strcompress(mask[k].type,/remove_all) eq 'TARGET') and (slit_id eq 'mos')) then begin
                        skymodel = (dimensions eq 3)? $
                                            bspline_valu_3d(wlmap_slit, x2=xmaskmap_slit, $
                                                            x3=ymaskmap_slit, *sky_all[k].sky_model) : $
                                            bspline_valu(wlmap_slit, x2=xmaskmap_slit, *sky_all[k].sky_model)
                    endif else begin
                        skymodel = bspline_valu(wlmap_slit, x2=ymaskmap_slit, *sky_all[k].sky_model)
                    endelse
                    badwl=where((wlmap_slit lt wlmin) or (wlmap_slit gt wlmax) or (finite(wlmap_slit) ne 1), cbadwl)
                    if(cbadwl gt 0) then skymodel[badwl]=!values.d_nan
                endif else skymodel=!values.d_nan
                print,'done'
            endif else begin
                skymodel = obj_slit*0.0
            endelse
            mwrfits,skymodel,file_out,hdr_slit_cur,/silent
        endfor
        for i=0,nslit-1 do ptr_free,sky_all[i].sky_model
endfor

if(nstages gt 1) then begin
    file_out=wdir+'/sky_'+image_type+'_2d_bspl_slits.fits'
    file_cnt=wdir+'/sky_'+image_type+'_2d_bspl_slits_cnt.fits'
    file_blue=wdir+'/sky_'+image_type+'_2d_bspl_slits_blue.fits'
    file_red=wdir+'/sky_'+image_type+'_2d_bspl_slits_red.fits'
    h0=headfits(file_cnt)
    writefits,file_out,0,h0
    for i=0,n_elements(mask)-1 do begin
        print,'Combining sky models for slit #'+string(i+1,format='(i4)')+'/'+string(n_elements(mask),format='(i4)')
        s_c=mrdfits(file_cnt,i+1,h_c,/silent)
        s_b=mrdfits(file_blue,i+1,/silent)
        s_r=mrdfits(file_red,i+1,/silent)

        s_out=s_c
        pix_blue=where(finite(s_b) eq 1 and finite(s_c) ne 1, cpix_blue)
        if(cpix_blue gt 0) then s_out[pix_blue]=s_b[pix_blue]
        pix_red=where(finite(s_r) eq 1 and finite(s_c) ne 1, cpix_red)
        if(cpix_red gt 0) then s_out[pix_red]=s_r[pix_red]
        mwrfits,s_out,file_out,h_c,/silent
    endfor
endif

end
