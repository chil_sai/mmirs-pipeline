function def_grism,logfile,filter=filter
;+
; NAME:
;	def_grism
; PURPOSE:
;	Getting grism ID from logfile
; DESCRIPTION: 
;	
; CALLING SEQUENCE:
;	Result=def_grism(logfile)
;
; CATEGORY:
;	MMIRS pipeline
;
; INPUTS:
;	LOGFILE = file name of the pipeline task file (FITS-header)
;	
;
; OUTPUTS:
;	Result = string scalar giving the grism ID
;		
; OPTIONAL OUTPUT:
;	no
;
; OPTIONAL INPUT KEYWORDS:
;	no	
;
; RESTRICTIONS:
;	no
;
; NOTES:
;	no
;
; PROCEDURES USED:
;	SXPAR,READLOG
;
; MODIFICATION HISTORY:
;       Written by Igor Chilingarian, CfA, Jan/2012
;-

on_error,2

log=readlog(logfile)

result=strcompress(sxpar(log,'GRISM',count=nmatches),/remove_all)
if(nmatches ne 1) then result='HK' ;;;; defaults to HK
filter=strcompress(sxpar(log,'FILTER',count=nmatches),/remove_all)
if(nmatches ne 1) then filter='HK' ;;;; defaults to HK
if(filter eq 'Kspec') then filter='K'

return,result

end
