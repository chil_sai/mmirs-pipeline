Mask1

Giude	Ok
start	Jan 01 00:00:00 2012
minutes	404
utc	Dec 01 00:00:00 2012
ra	0:00:00.0000
dec	+00:00:00.000
ha	0:00:00.0000
airmass	    1.000
parang	    0.000
usepar	1
pa	   0.000
rotator	   0.000
scale	 1.000219
wavelen	 5000.000
arc2mm	    0.165
label	3_pixel
inst	mmirs.mdf
rank	1
filter	
disperser	
dangle	
telepoly	0	0	0	0	0	0
telerevs	0	0	0	0	0	0


GuideStars	ra	dec	x	y	mag	wfs
GuideStars	--	---	-	-	---	---



corners	-19.300	-33.500	 19.300	 33.300
axis	 26.162	 46.990

slit	ra          	dec          	x      	y      	target	object	type  	wstart   	wend     	height	width	offset	theta	bbox                          	polygon                                                       
----	------------	-------------	-------	-------	------	------	------	---------	---------	------	-----	------	-----	------------------------------	--------------------------------------------------------------
   1	0:00:00.0000	 00:00:00.000	  0.110	  0.000	     1	3_pix	TARGET	14000.193	27121.548	66.800	0.0662	 0.000	0.000	26.165   0.000   0.132  93.980	16.287  14.425  16.287  15.584  16.419  15.584  16.419  14.425
