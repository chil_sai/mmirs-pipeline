function linearisation_params,grism,filter,wl0=wl0,dwl=dwl,wl_min=wl_min,wl_max=wl_max

    if(grism eq 'HK') then begin
        wl0=12400.0
        dwl=6.5
        npix=1831
        if(filter eq 'K') then begin
            wl_min=20500.0
            wl_max=24300.0
        endif else begin ;;;; 'HK' assumed
            wl_min=12400.0
            wl_max=24300.0
        endelse
    endif

    if(grism eq 'J') then begin
        wl0=9500.0
        dwl=2.4
        npix=1600
        if(filter eq 'J') then begin
            wl_min=11720.0
            wl_max=13300.0
        endif else begin ;;;; 'zJ' assumed
            npix=2300
            wl_min=9500.0
            wl_max=15100.0 ;;; 14300
        endelse
    endif

    if(grism eq 'H') then begin
        wl0=14500.0
        dwl=3.2
        npix=1200
        if(filter eq 'H') then begin
            wl_min=14500.0
            wl_max=18340.0 ;;; 17910.0
        endif else begin ;;;; 'HK' assumed
            npix=2400
            wl_min=14500.0
            wl_max=24000.0
        endelse
    endif

    if(grism eq 'H3000') then begin
        wl0=14420.0
        dwl=2.00
        npix=2040
        wl_min=14420.0
        wl_max=17900.0
    endif

    if(grism eq 'K3000') then begin
        wl0=18900.0
        dwl=2.95
        npix=2100
;        if(filter eq 'K') then begin
            wl_min=18900.0
            wl_max=24700.0
;        endif
    endif

    return,{grism:grism, filter:filter, wl0:wl0, dwl:dwl, npix:npix, wl_min:wl_min, wl_max:wl_max}

end
