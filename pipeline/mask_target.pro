function mask_target,logfile,$
    slitnum=slitnum,width=width,$
    offset=offset,$
    diffmode=diffmode,linearised=linearised,$
    nmasked=nmasked,maskarray=maskarray

log=readlog(logfile)
if(n_elements(slitnum) ne 1) then slitnum=1
if(n_elements(offset) ne 1) then offset=0.0
wdir=def_wdir(logfile)

if(n_elements(width) ne 1) then width=sxpar(log,'EXTAPW') ; defaults to the extraction aperture

mask=read_mask_mmirs_ms(wdir+'mask_mos.txt',logfile=logfile)

val=sxpar(log,'DITHPOS',count=cntval)
dithpos=(cntval eq 1)? double(strsplit(val,',',/extract)) : 0.0

if(keyword_set(diffmode)) then begin
    val2=sxpar(log,'DITHPOS2',count=cntval2)
    dithpos2=(cntval2 eq 1)? double(strsplit(val2,',',/extract)) : 0.0
    dithpos=[dithpos,dithpos2]
endif

ccdscl=0.2 ;;; 0.2 arcsec per pix -- hardcoded CCD scale

dithpos=dithpos+offset

if(keyword_set(linearised)) then begin
    im_cur=mrdfits(wdir+'obj_slits_lin.fits',slitnum,h_cur,/silent)
    maskarray=byte(im_cur*0)
endif else begin
    im_cur=mrdfits(wdir+'obj_slits.fits',slitnum,h_cur,/silent)
    maskarray=byte(im_cur*0)

    dist_map=mrdfits(wdir+'dist_map.fits',1,/silent)
    slit_reg=get_slit_region(mask,nx=nx,ny=ny,dist_map=dist_map,slit_trace=slit_trace)

    cur_y_tr= 0 > (slit_trace[slitnum-1].y_trace - sxpar(h_cur,'YOFFSET') + dithpos[0]/ccdscl) < (sxpar(h_cur,'NAXIS2')-1)
    maskarray_tmp=maskarray
    for i=0,sxpar(h_cur,'NAXIS1')-1 do maskarray_tmp[i,round(cur_y_tr[i])]=1

    if(keyword_set(diffmode)) then begin
        cur_y_tr2= 0 > (slit_trace[slitnum-1].y_trace - sxpar(h_cur,'YOFFSET') + dithpos[1]/ccdscl) < (sxpar(h_cur,'NAXIS2')-1)
        for i=0,sxpar(h_cur,'NAXIS1')-1 do maskarray_tmp[i,round(cur_y_tr2[i])]=1
    endif
    
    maskarray=maskarray_tmp+shift(maskarray_tmp,0,-1)+maskarray_tmp(0,+1)

    ;;; get the trace from get_slit_regions and shift it up and down
endelse

return,where(maskarray gt 0,nmasked) ;;; subscripts of pixels to mask

end
