pro mmirs_adjust_wavesol_ms_simple,logfile,stages=stages,crosscorr=crosscorr,debug=debug,improve=improve

if(n_elements(stages) ne 2) then stages=[1,1]
if(keyword_set(crosscorr)) then stages[1]=1
grism=def_grism(logfile,filter=filter)
wdir=def_wdir(logfile)
wl_inp_file=(keyword_set(improve))? 'disper_table_adj.fits' : 'disper_table.fits'
wl_data=mrdfits(wdir+wl_inp_file,1,/silent)
log=readlog(logfile)

bright=sxpar(log,'BRIGHT')

wl_new = wl_data
if(stages[0] eq 1) then begin
    Ny=n_elements(wl_data)
    
    ndeg=n_elements(wl_data[0].wl_sol)-1
    
    for j=0,n_elements(wl_data)-1 do $
        wl_new[j].wl_sol=transform_coeff(wl_data[j].wl_sol,1d,1024d)
    
    for i=0,ndeg do begin
        deg_fit1=(i le 1)? sxpar(log,'WLNDEG') : 1
        deg_fit2=(i le 1)? sxpar(log,'WLYNDEG') : 1
    
        f_arr=dblarr(3,Ny)
        f_arr[0,*]=wl_data.x_mask
        f_arr[1,*]=wl_data.y_mask
    ;;    f_arr[2,*]=wl_data.wl_sol[i]
        f_arr[2,*]=wl_new.wl_sol[i]
        f_fit=(i le 2)? sfit_2deg(f_arr,/irr,deg_fit1,deg_fit2,kx=kx) : replicate(median(f_arr[2,*]),Ny)
        if(i gt 2) then kx=[[f_fit[0],0d],[0d,0d]]
    ;stop
        good_pnts=where(finite(f_arr[2,*]) eq 1,cgood)
        if(keyword_set(debug)) then print,'i=',i,' total   cgood=',cgood,'/',Ny
        dsurf=robust_sigma((f_arr[2,*]-f_fit)[good_pnts])
        bsurf=where(abs(f_arr[2,*]-f_fit) gt 3*dsurf,cbsurf,compl=gsurf)
        if(keyword_set(debug)) then print,'i=',i,' iter 1, cbad =',cbsurf,'/',Ny,' sig=',dsurf
        if(cbsurf gt 0) then begin
            f_arr_tmp=f_arr
            f_arr_tmp[*,bsurf]=!values.f_nan
            f_fit=(i le 2)? sfit_2deg(f_arr_tmp,deg_fit1,deg_fit2,kx=kx,/irreg) : replicate(median(f_arr_tmp[2,gsurf]),Ny)
            if(i gt 2) then kx=[[f_fit[0],0d],[0d,0d]]
        
            dsurf=robust_sigma((f_arr_tmp[2,*]-f_fit)[good_pnts])
            bsurf=where(abs(f_arr_tmp[2,*]-f_fit) gt 3*dsurf,cbsurf,compl=gsurf)
            if(keyword_set(debug)) then print,'i=',ndeg,' iter 2, cbad =',cbsurf,'/',Ny,' sig=',dsurf
            if(cbsurf gt 0) then begin
    ;            f_arr_tmp=f_arr
                f_arr_tmp[*,bsurf]=!values.f_nan
                f_fit=(i le 2)? sfit_2deg(f_arr_tmp,deg_fit1,deg_fit2,kx=kx,/irreg) : replicate(median(f_arr_tmp[2,gsurf]),Ny)
                if(i gt 2) then kx=[[f_fit[0],0d],[0d,0d]]
            endif
        endif
        wl_new.wl_sol[i]=poly2d(wl_new.x_mask,wl_new.y_mask,kx,/irreg,deg1=deg_fit1,deg2=deg_fit2)
    endfor
    
    for j=0,n_elements(wl_new)-1 do $
        wl_new[j].wl_sol=transform_coeff(wl_new[j].wl_sol,1d,-1024d)
endif

mask=read_mask_mmirs_ms(wdir+'mask_mos.txt',logfile=logfile)
inp_file = (bright eq 1)? wdir+'arc_ff.fits' : wdir+'obj_ff.fits'
obj=readfits(inp_file,h,/silent)
dist_map=mrdfits(wdir+'dist_map.fits',1,/silent)
obj_r=rotate(poly_2d(rotate(obj,5),dist_map.kxwrap,dist_map.kywrap,2,cubic=-0.5),5)
obj=obj_r
Nx=sxpar(h,'NAXIS1')
Ny=sxpar(h,'NAXIS2')

l_par = linearisation_params(grism,filter)
wl0=l_par.wl0
dwl=l_par.dwl
npix=l_par.npix
wl_min=l_par.wl_min
wl_max=l_par.wl_max

obj_lin=fltarr(npix,Ny)
wl_sc_lin=wl0+findgen(npix)*dwl
flag_arr=bytarr(Ny)

if(stages[1] eq 1) then begin
    dwl_arr=dblarr(Ny)+!values.f_nan
    nmag=10L
    dpix=5
    x_cross=findgen(2*nmag*dpix+1)/double(nmag)-dpix
    for i=0,Ny-1 do begin
        wl_sc_cur=poly(findgen(Nx),wl_new[i].wl_sol)
        obj_lin[*,i]=interpol(obj[*,i],poly(findgen(Nx),wl_new[i].wl_sol),wl_sc_lin)
 ;       print,'i=',i,' slit=',wl_new[i].slit,where(mask.slit eq wl_new[i].slit)
        if((finite(wl_new[i].x_mask) eq 1) and (wl_new[i].slit ge 0) and $
           (finite(wl_new[i].wl_sol[0]) eq 1)) then $
             if (mask[where(mask.slit eq wl_new[i].slit)].type eq 'TARGET') then begin
                 flag_arr[i]=1
                 wl_min = (wl_min > wl_sc_cur[0])
                 wl_max = (wl_max < wl_sc_cur[Nx-1])
             endif
    endfor
    pix_good=where((wl_sc_lin gt wl_min+100.0) and (wl_sc_lin lt wl_max-100.0),cgoodpix)

    gflag=where(flag_arr eq 1, cgflag)
    med_obj=median(obj_lin[*,gflag],dim=2) 
    med_obj=med_obj[pix_good]
    med_vec=congrid(med_obj,cgoodpix*nmag,cubic=-0.5)
    if(keyword_set(debug)) then $
        plot,/nodata,[0],xs=1,ys=1,xr=[0,Ny],yr=[-3.,3],xtitle='Y coord',ytitle='d(wl)'
;;        plot,x_cross,x_cross,/nodata,xs=1,ys=1,yr=[-0.2,1.2]
    for j=0,cgflag-1 do begin
        vec_cur=obj_lin[pix_good,gflag[j]]
        n_coeff=median(vec_cur/med_obj)
        bvec=where((abs(vec_cur/n_coeff/med_obj - 1.0) gt 0.9) or (finite(vec_cur) ne 1),cbvec)
        if(cbvec gt 0) then vec_cur[bvec]=med_obj[bvec]*n_coeff
        obj_vec=congrid(vec_cur,cgoodpix*nmag,cubic=-0.5)
        c_cross=c_correlate(med_vec,obj_vec,x_cross*double(nmag))
        if((max(c_cross,/nan) gt 0.3)) then begin
            gau=gaussfit(x_cross,c_cross,G,nterms=3)
            if (abs(G[1]) lt dwl*3.0) then dwl_arr[gflag[j]]=G[1]
            if(keyword_set(debug)) then begin
                ;;oplot,x_cross,c_cross,col=(j > 20)
                ;;oplot,x_cross,gau,col=(j > 20),psym=7
                oplot,[gflag[j]],[G[1]],psym=7,syms=0.4
                print,'Y=',gflag[j],' cbvec=',cbvec,' dl=',G[1]
            endif
        endif
    endfor
    dwl_arr=dwl_arr-median(dwl_arr)
    miss_dwl=where(finite(dwl_arr) ne 1, cmiss_dwl)
    dwl_arr[miss_dwl]=0.0
    for i=0,Ny-1 do begin
        wl_new[i].wl_sol[0]=wl_new[i].wl_sol[0]-dwl_arr[i]*dwl
        obj_lin[*,i]=interpol(obj[*,i],poly(findgen(Nx),wl_new[i].wl_sol),wl_sc_lin)
    endfor
endif
writefits,wdir+'obj_lin.fits',obj_lin,h
;writefits,wdir+'obj_ttt.fits',obj,h

writefits,wdir+'disper_table_adj.fits',0
mwrfits,wl_new,wdir+'disper_table_adj.fits'

end
