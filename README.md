# README #

This repository contains a data reduction pipeline for the MMT and Magellan Infrared Spectrograph.
Details can be found in the project [wiki pages](https://bitbucket.org/chil_sai/mmirs-pipeline/wiki/Home)

