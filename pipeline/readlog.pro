function readlog,logfile

nl=long(file_lines(LOGFILE))
tab=strarr(nl)
openr,u,logfile,/get_lun
readf,u,tab
close,u
free_lun,u

return,tab
end
