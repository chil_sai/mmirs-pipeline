;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;  INPUTS
;      ra -- vector of R.A. values, deg
;      dec -- vector of Dec values, deg
;      racen -- scalar, R.A. center, deg
;      deccen -- scalar, Dec center, deg
;      posa -- position angle, deg
;
;  OPTIONAL INPUTS
;      mjd -- MJD for the time of observation
;      lat -- latitude of the observing site, deg, defaults to 31.6888d (MMT)
;      altitude -- altitude of the observing site in m, defaults to 2600
;      telepoly -- polynomical coefficients of the projection (deg->mm)
;                  defaults to: [0,599.508,7.25994,0,73.5279,0,-40.5352]
;
;  OUTPUTS
;      x -- vector of X, mm
;      y -- vector of Y, mm
;
;  OPTIONAL INPUTS/OUTPUTS
;      lst -- local siderial time, hours, computed from MJD if given, defaults to racen/15d (i.e. transit)
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

pro mmirs_radec2xy,ra,dec,racen,deccen,posa,$
    lat=lat,mjd=mjd,lst=lst,altitude=altitude,norefract=norefract,nomask=nomask,$
    telepoly=telepoly,x,y

    if(n_elements(telepoly) lt 1) then $
       ;telepoly=[0d,(0.163518127280455d*3600d),0d,(3600d^3.0*1.28763014592289e-8),0d,0d] ;from mmirs.tab
       telepoly=[0d,588.950355963739d,-0.491895696870835d,492.445872987495d,1310.59666090758d,-326.514829993992d] ;from guistars-mmirs.conf and fitmask-opt.c

      ;telepoly=[0d,599.508197d,7.259937d,0d,73.5279d,0d,-40.5352d]

    ra_=ra
    dec_=dec
    racen_=racen
    deccen_=deccen

    if(not keyword_set(norefract)) then begin
        ;;; various corrections (precession, nutation, aberration, refraction)
        ra_=ra
        dec_=dec
        if(n_elements(lat) ne 1) then lat=31.688778d
        if(n_elements(altitude) ne 1) then altitude=2600.0

        if(n_elements(mjd) eq 1) then begin
            if(n_elements(lon) ne 1) then lon=-110.884556d
            j_now=(mjd-51544.4d)/365.25d + 2000d
            precess, ra_, dec_, 2000d, j_now
            precess, racen_, deccen_, 2000d, j_now
            co_nutate, mjd+2400000.5d, [racen_,ra_], [deccen_,dec_], dra1, ddec1, eps=eps, d_psi=d_psi
            co_aberration, mjd+2400000.5d, [racen_,ra_], [deccen_,dec_], dra2, ddec2, eps=eps
            racen_ = racen_ + (dra1[0] + dra2[0])/3600d
            deccen_ = deccen_ + (ddec1[0] + ddec2[0])/3600d
            ra_ = ra_ + (dra1[1:*] + dra2[1:*])/3600d
            dec_ = dec_ + (ddec1[1:*] + ddec2[1:*])/3600d
            ct2lst, lmst, lon, 0, mjd+2400000.5d
            lst = lmst + d_psi*cos(eps)/3600d/15d
        endif

        if(n_elements(lst) ne 1) then lst=racen_/15d
        ha = lst*15d - [racen_,ra_]
        ha = ha mod 360d
        neg_ha=where(ha lt 0, cneg_ha)
        if(cneg_ha gt 0) then ha[neg_ha]+=360d
        hadec2altaz,ha,[deccen_,dec_],lat,alt,az
        alt=co_refract(alt, altitude=altitude, /to_observed, epsilon=0.1d)
        altaz2hadec, alt, az, lat, ha_, dec_
        ra_ = lst*15d - ha_
        ra_ = ra_ mod 360d
        neg_ra=where(ra_ lt 0, cneg_ra)
        if(cneg_ra gt 0) then ra_[neg_ra]+=360d
        racen_=ra_[0]
        ra_=ra_[1:*]
        deccen_=dec_[0]
        dec_=dec_[1:*]
    endif

    ;; first doing the tangential projection at (racen, deccen)
    beta      = (ra_ - racen_)/!radeg
    cosbeta   = cos(beta)
    tandec    = tan(dec_/!radeg)
    tandeccen = tan(deccen_/!radeg)
    s   = cosbeta + tandec*tandeccen
    xi  = sin(beta)/cos(deccen_/!radeg)/s
    eta = (tandec - tandeccen*cosbeta)/s

    ;; computing coordinates in mm using the telepoly projection
    r_xieta = sqrt(xi^2+eta^2)
    r_deg=atan(r_xieta)*!radeg
    ;polynomial is not valid beyond ~1.2 degrees, and in fact turns
    ;around at +X, so for targets beyond that radius, set = to 1.2
    bad=where(r_deg gt 1.2,/null, bcnt)
    if bcnt ge 1 then r_deg[bad]=1.2
    bad=where(r_deg lt -1.2,/null, bcnt)
    if bcnt ge 1 then r_deg[bad]=-1.2
    r_mm_new = poly(r_deg, telepoly) ;*1.000219d
    ;;r_xieta = sphdist(ra_/!radeg,dec_/!radeg,racen_/!radeg,deccen_/!radeg)
    ;;r_mm_new = poly(r_xieta*!RADEG, telepoly) *1.000219d

    ang_xieta = atan(eta, xi)
    x = r_mm_new*cos(ang_xieta+posa/!RADEG)
    y = r_mm_new*sin(ang_xieta+posa/!RADEG)

    if(not keyword_set(nomask)) then begin
        ;new steel mask thermal factor is actually 1.002718, but the conversion
        ;factor is applied in mask2dxf so we don't have to remake old msk files
        x=x*1.00376 ;cold slit x to warm slit x
        y=y*1.00376 ;cold slit y to warm slit y
    endif
    ;x*=-1d
    y*=-1d
 end
