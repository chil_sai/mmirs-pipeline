function get_telluric_list,logfile,n_telluric=n_telluric

log=readlog(logfile)  
if(n_elements(n_telluric) ne 1) then n_telluric=1

t_all=sxpar(log,'STAR'+string(n_telluric,format='(i2.2)'),count=cnt_tell)

if(cnt_tell eq 0) then return,-1
return,[strsplit(t_all,',',/extract)]

end
