function read_lines_id,filename
    n_l=file_lines(filename)-2
    data=dblarr(5,n_l)
    openr,u,filename,/get_lun
    hdr0=''
    readf,u,hdr0
    hdr1=''
    readf,u,hdr1
    for i=0,n_l-1 do begin
        dataline=''
        readf,u,dataline
        data_extr=strsplit(dataline,/extr)
        data[*,i]=double(data_extr[0:4])
        if(data_extr[3] eq 'NaN') then data[*,i]=!values.d_nan
    endfor
    close,u
    free_lun,u
    data=data[*,where(finite(data[0,*]) eq 1)]

    return,data
end

pro get_wl_ini,logfile,dir=dir,n_deg=n_deg,ndegY=ndegY,exclude_slits=exclude_slits,grism=grism

    if(n_elements(n_deg) ne 1) then n_deg=3
    if(n_elements(ndegY) ne 1) then ndegY=2
    if(n_elements(grism) ne 1) then grism='HK'

    case grism of
        'HK': grism_factor=-4.0
        'J': grism_factor=-8.0
        'H3000': grism_factor=-6.0
        'K3000': grism_factor=-10.0
        ELSE: grism_factor=-4.5
    endcase
    print,'Grism_factor=',grism,grism_factor
 
    if(keyword_set(dir)) then begin
        dir=logfile
    endif else begin
        dir=def_wdir(logfile)
    endelse

    mask_orig=read_mask_mmirs_ms(dir+'/mask_mos.txt')

    if(n_elements(exclude_slits) gt 0) then mask_orig[exclude_slits-1].type='EXCL'

    targ_slits=where(mask_orig.type eq 'TARGET',ctarg)


    if(ctarg eq 0) then begin
        print,'Not enough TARGET slits'
        return
    endif

    mask=mask_orig[targ_slits]
    coeff_arr=dblarr(n_deg+1,ctarg)

    for i=0,ctarg-1 do begin
;;;        dxpix=poly(mask[i].y,[-0.399428,0.0614533,-0.00330884])
        dxpix=poly(mask[i].y,[0.,0.,1/33.5^2*grism_factor])
        print,'slit_y,dxpix=',mask[i].y,dxpix
        lines_id=read_lines_id(dir+'/lines_id_slit'+string(targ_slits[i]+1,format='(i4.4)')+'.txt')
        c_line=robust_poly_fit(transpose(lines_id[3,*])-dxpix,transpose(lines_id[0,*]),n_deg)
        coeff_arr[*,i]=c_line[*]
    endfor
    
    wl0coeff=robust_poly_fit(mask.x,transpose(coeff_arr[0,*]),ndegY,yff,sff)
    dwlcoeff=robust_poly_fit(mask.x-0*1.5*sqrt(mask.x^2+mask.y^2),transpose(coeff_arr[1,*]),2,ydd,sdd)

    print,[coeff_arr[0:1,0],median(coeff_arr[2,*]),median(coeff_arr[3,*])] ;,format='(4f8)'
    print,wl0coeff
;    print,sff
    print,dwlcoeff
;    print,sdd
end
