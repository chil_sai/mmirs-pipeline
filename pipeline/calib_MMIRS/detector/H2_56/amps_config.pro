amps_conf=replicate({xmin:0,xmax:0,ymin:0,ymax:0,direction:1},32)
amps_conf[0:7].direction=2
amps_conf[16:23].direction=2

amps_conf[16:23].xmin=(7-findgen(8))*128
amps_conf[16:23].xmax=(7-findgen(8))*128+127
amps_conf[16:23].ymin=0
amps_conf[16:23].ymax=1023

amps_conf[8:15].xmin=1024
amps_conf[8:15].xmax=2047
amps_conf[8:15].ymin=(7-findgen(8))*128
amps_conf[8:15].ymax=(7-findgen(8))*128+127

amps_conf[24:31].xmin=0
amps_conf[24:31].xmax=1023
amps_conf[24:31].ymin=findgen(8)*128+1024
amps_conf[24:31].ymax=findgen(8)*128+127+1024

amps_conf[0:7].xmin=findgen(8)*128+1024
amps_conf[0:7].xmax=findgen(8)*128+127+1024
amps_conf[0:7].ymin=1024
amps_conf[0:7].ymax=2047

writefits,'amps32_layout.fits',0
mwrfits,amps_conf,'amps32_layout.fits'
