pro adjust_flatfield_ms,logfile,debug=debug

wdir=def_wdir(logfile)
wl_data=mrdfits(wdir+'disper_table_adj.fits',1,/silent)

Ny=n_elements(wl_data)
gg=where(finite(wl_data.y_mask) eq 1)

ndeg=n_elements(wl_data[0].wl_sol)-1

mask=read_mask_mmirs_ms(wdir+'mask_mos.txt',logfile=logfile)
flat=readfits(wdir+'flat_norm.fits',h,/silent)
flat=readfits(wdir+'obj_ff.fits',h,/silent)
dist_map=mrdfits(wdir+'dist_map.fits',1,/silent)
flat_r=rotate(poly_2d(rotate(flat,5),dist_map.kxwrap,dist_map.kywrap,2,cubic=-0.5),5)
flat=flat_r
Nx=sxpar(h,'NAXIS1')
Ny=sxpar(h,'NAXIS2')

;;;;;;;;;;;;;;;;;;;;for HK;;;;;;;;;;;;;;;;;
wl0=12500.0
dwl=6.5
npix=1700
wl_min=10000.0
wl_max=23400.0

flat_lin=fltarr(npix,Ny)
wl_sc_lin=wl0+findgen(npix)*dwl
flag_arr=bytarr(Ny)
dwl_arr=dblarr(Ny)+!values.f_nan

nmag=10L
dpix=5
for i=0,Ny-1 do begin
    wl_sc_cur=poly(findgen(Nx),wl_data[i].wl_sol)
    flat_lin[*,i]=interpol(flat[*,i],poly(findgen(Nx),wl_data[i].wl_sol),wl_sc_lin)
 ;       print,'i=',i,' slit=',wl_data[i].slit,where(mask.slit eq wl_data[i].slit)
    if((finite(wl_data[i].x_mask) eq 1) and (wl_data[i].slit ge 0)) then $
         if (mask[where(mask.slit eq wl_data[i].slit)].type eq 'TARGET') then begin
             flag_arr[i]=1
             wl_min = (wl_min > wl_sc_cur[0])
             wl_max = (wl_max < wl_sc_cur[Nx-1])
         endif
endfor
pix_good=where((wl_sc_lin gt wl_min+100.0) and (wl_sc_lin lt wl_max-100.0),cgoodpix,compl=pix_bad)
gflag=where(flag_arr eq 1, cgflag)
med_flat=median(flat_lin[*,gflag],dim=2) 
;;;med_flat=med_flat[pix_good]
;;;med_vec=congrid(med_flat,cgoodpix*nmag,/interp)
med_flat_z=med_flat
med_flat_z[pix_bad]=!values.f_nan

flat_lin_corr=flat_lin*0.0
flat_corr=flat*0.0
for i=0,Ny-1 do begin
    flat_lin_corr[*,i]=flat_lin[*,i]/med_flat_z
    flat_corr[*,i]=interpol(flat_lin_corr[*,i],wl_sc_lin,poly(findgen(Nx),wl_data[i].wl_sol))
endfor
flat_corr=rotate(poly_2d(rotate(flat_corr,5),dist_map.kxwrap_inv,dist_map.kywrap_inv,1),5)

writefits,wdir+'flat_corr.fits',flat_corr,h
writefits,wdir+'flat_lin.fits',flat_lin,h

end
