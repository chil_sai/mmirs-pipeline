maps=[$
 '/pool/tdc3/chil/mmirs/reduced/2011/2011.1203/lei1.a1_mask1/01/dist_map.fts',$
 '/pool/tdc3/chil/mmirs/reduced/2011/2011.1204/lei2.a1_mask2/01/dist_map.fts',$
 '/pool/tdc3/chil/mmirs/reduced/2011/2011.1204/lei3.a1_mask3/01/dist_map.fts',$
 '/pool/tdc3/chil/mmirs/reduced/2011/2011.1211/twang4_twang4/01/dist_map.fts',$
 '/pool/tdc3/chil/mmirs/reduced/2011/2011.1216/jsams11_jsams11_1/01/dist_map.fts',$
 '/pool/tdc3/chil/mmirs/reduced/2012/2012.0107/franz11_any/01/dist_map.fts',$
 '/pool/tdc3/chil/mmirs/reduced/2012/2012.0411/walth11_walth11/01/dist_map.fts']

m=mrdfits(maps[0],1,h)

g_m=[0,2,3,5,6]
n_g_m=n_elements(g_m)

n_m=n_elements(maps)
m_arr=replicate(m,n_m)
dd=dblarr(4,4,n_m)
for i=1,n_m-1 do m_arr[i]=mrdfits(maps[i],1)

m_mean=m_arr[g_m[0]]
for i=1,n_g_m-1 do begin
    m_mean.kxwrap=m_mean.kxwrap+m_arr[g_m[i]].kxwrap
    m_mean.kywrap=m_mean.kywrap+m_arr[g_m[i]].kywrap
    m_mean.kxwrap_inv=m_mean.kxwrap_inv+m_arr[g_m[i]].kxwrap_inv
    m_mean.kywrap_inv=m_mean.kywrap_inv+m_arr[g_m[i]].kywrap_inv
    m_mean.dyarr=m_mean.dyarr+m_arr[g_m[i]].dyarr
    m_mean.corrarr=m_mean.corrarr+m_arr[g_m[i]].corrarr
    m_mean.kx_2dfit=m_mean.kx_2dfit+m_arr[g_m[i]].kx_2dfit
    m_mean.mask_dy0=m_mean.mask_dy0+m_arr[g_m[i]].mask_dy0
    m_mean.mask_y_scl=m_mean.mask_y_scl+m_arr[g_m[i]].mask_y_scl
endfor
m_mean.kxwrap=m_mean.kxwrap/n_g_m
m_mean.kywrap=m_mean.kywrap/n_g_m
m_mean.kxwrap_inv=m_mean.kxwrap_inv/n_g_m
m_mean.kywrap_inv=m_mean.kywrap_inv/n_g_m
m_mean.dyarr=m_mean.dyarr/n_g_m
m_mean.corrarr=m_mean.corrarr/n_g_m
m_mean.kx_2dfit=m_mean.kx_2dfit/n_g_m
m_mean.mask_dy0=m_mean.mask_dy0/n_g_m
m_mean.mask_y_scl=m_mean.mask_y_scl/n_g_m

mwrfits,m_mean,'dist_map_HK_HK_mos.fts',h

u=[0,1,4,5,7,8,9,10,11,12,13,14,15]

for i=0,n_m-1 do dd[*,*,i]=m_arr[i].kywrap_inv
for i=0,n_m-1 do begin 
    print,maps[i] 
    print,m_arr[i].kywrap_inv/median(dd,dim=3)
    print,'Total:',total(((m_arr[i].kywrap_inv/m_mean.kywrap_inv)[u]-1.0)^2)
endfor

end

