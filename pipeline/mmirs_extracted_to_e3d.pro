; procedure to convert MMIRS processed data into the Euro3D format
;
;  inp_file -- filename of MMIRS extracted file (stacked spectra)
;  out_file -- filename of output E3D file
;
;  KEYWORDS
;  err_file -- error spectra (stacked spectra)
;              by default is set to NaN
;

pro mmirs_extracted_to_e3d, inp_file, out_file, err_file=err_file, maskfile=maskfile, fwhm=fwhm

if(n_elements(fwhm) ne 1) then fwhm=0.8 ;;;; fwhm seeing, default is 0.8 arcsec
if(n_elements(maskfile) ne 1) then maskfile='mask_mos.txt'

mask=read_mask_mmirs_ms(maskfile)
n_slits=n_elements(mask)

inp_hdr=headfits(inp_file)
inp_data=mrdfits(inp_file,1,inp_ext_hdr,/silent)
err_data=(n_elements(err_file) eq 1)? mrdfits(err_file,1,/silent) : mrdfits(inp_file,2,/silent) ;inp_data*!values.f_nan

out_hdr_pri=inp_hdr
sxaddpar,out_hdr_pri,'EURO3D','T','All mandatory EuroE3D extensions present'
sxaddpar,out_hdr_pri,'E3D_ADC','F','data not corrected for atm dispersion'
sxaddpar,out_hdr_pri,'E3D_VERS',1.1,'version number of the EuroE3D format'
sxaddpar,out_hdr_pri,'NAXIS',0
sxdelpar,out_hdr_pri,'NAXIS1'
sxdelpar,out_hdr_pri,'NAXIS2'
sxdelpar,out_hdr_pri,'NAXIS3'
out_hdr_data=["EXT00 = 'E3D_DATA'           / name of this binary table extension"]
sxaddpar,out_hdr_data,'EXTNAME','E3D_DATA','name of this binary table extension'
sxdelpar,out_hdr_data,'EXT00'
out_hdr_group=["EXT00 = 'E3D_GRP'            / name of this binary table extension"]
sxaddpar,out_hdr_group,'EXTNAME','E3D_GRP','name of this binary table extension'
sxdelpar,out_hdr_group,'EXT00'

n_sp=n_slits

arcsecpix=0.2012
arcsecmm=30.2101*0.2012

pa = atan(sxpar(inp_hdr,'CD1_1'),sxpar(inp_hdr,'CD1_2'))*!radeg

cospa=cos(pa*!dpi/180.0)
sinpa=sin(pa*!dpi/180.0)
get_coords,instr=sxpar(inp_hdr,'CAT-RA')+' '+sxpar(inp_hdr,'CAT-DEC'),coords
coords[0]=coords[0]*15d
;;;coords = [sxpar(inp_hdr,'CRVAL1'),sxpar(inp_hdr,'CRVAL2')]
xcent=(sxpar(inp_hdr,'CRPIX1')-1024.0)*arcsecpix
ycent=(sxpar(inp_hdr,'CRPIX2')-1024.0)*arcsecpix

epoch = 2000.0
wavunit=sxpar(inp_ext_hdr,'CUNIT1',count=cwavunit)
if(cwavunit eq 0) then wavunit='nm'
ctypes = sxpar(inp_ext_hdr,'CTYPE1')
crvals = sxpar(inp_ext_hdr,'CRVAL1')
cdelts = sxpar(inp_ext_hdr,'CDELT1')

sec = arcsecpix/3600d
nx=sxpar(inp_ext_hdr,'NAXIS1')

z_vec=dindgen(nx)*0d
z_vfl=float(z_vec)
z_vul=ulong(z_vec*0)

c=299792.458d   ;; speed of light

wl=crvals+double(cdelts*findgen(nx))


;sxaddpar,oud_hdr_pri,'OEPOCH',epoch,'Epoch of observations'

sxaddpar,out_hdr_pri,'EPOCH',2000.0,'Epoch of the coordinates for WCS'

;sxaddpar,out_hdr_pri,'APPRA',sxpar(inp_hdr,'RA'),'Apparent position'
;sxaddpar,out_hdr_pri,'APPDEC',sxpar(inp_hdr,'DEC'),'Apparent position'

sxaddpar,out_hdr_pri,'RA',coords[0],'Right Ascension J2000'
sxaddpar,out_hdr_pri,'DEC',coords[1],'Declination J2000'
sxaddpar,out_hdr_pri,'PA',pa,'positional angle'

data_tbl=replicate({  $
    SPEC_ID:[0l],        $
    SELECTED:'T',       $
    NSPAX:[1l],          $
    SPEC_LEN:[nx],       $
    SPEC_STA:[1l],       $
    XPOS:[0d],          $  ; should be array
    YPOS:[0d],          $  ; should be array
    GROUP_N:[1l],        $  ; should be array
    SPAX_ID:'spectrum',      $  ; should be array
    DATA_SPE:z_vfl,   $
    QUAL_SPE:z_vul,   $
    STAT_SPE:z_vfl $  ;,   $
;    WL_SPE:wl,        $  ; my addition: lambda axis
;    SPR_DWL:z_vfl,    $  ; my addition: lambda shift of the LSF, km/s
;    SPR_SIG:z_vfl,    $  ; my addition: SIGMA of the LSF, km/s
;    SPR_H3:z_vfl,     $  ; my addition: H3 of the LSF
;    SPR_H4:z_vfl      $  ; my addition: H4 of the LSF
},n_sp)

;;; setting up spectral WCS
sxaddpar,out_hdr_data,'CTYPES',ctypes,'wavelength axis'
sxaddpar,out_hdr_data,'CRVALS',crvals,'reference wavelength'
sxaddpar,out_hdr_data,'CDELTS',cdelts,'wavelength increment per vector element'
sxaddpar,out_hdr_data,'CUNITS','nm','wavelength unit'
sxaddpar,out_hdr_data,'CRPIXS',1.,'reference pixel'    
bunit = sxpar(inp_hdr,"BUNIT", count=cbunit)
if(cbunit gt 0) then sxaddpar,out_hdr_data,'BUNITS', bunit, 'flux unit'


;;;;;;;;; filling xpos_arr and ypos_arr
xpos_arr=dblarr(n_sp)
ypos_arr=dblarr(n_sp)

;sxaddpar,out_hdr_data,'DATAMIN',0.0
;sxaddpar,out_hdr_data,'DATAMAX',0.0

sxaddpar,out_hdr_data,'TCTYP6','RA---TAN','Right ascension'
sxaddpar,out_hdr_data,'TCUNI6','deg','units'
sxaddpar,out_hdr_data,'TCRVL6',coords[0],'reference value'
;sxaddpar,out_hdr_data,'TCDLT6',-sec
sxaddpar,out_hdr_data,'TCRPX6',xcent,'reference point'
sxaddpar,out_hdr_data,'TWCS6','Rest Frame'

sxaddpar,out_hdr_data,'TCTYP7','DEC--TAN','Declination'
sxaddpar,out_hdr_data,'TCUNI7','deg','units'
sxaddpar,out_hdr_data,'TCRVL7',coords[1],'reference value'
;sxaddpar,out_hdr_data,'TCDLT7',sec
sxaddpar,out_hdr_data,'TCRPX7',ycent,'reference point'
sxaddpar,out_hdr_data,'TWCS7','Rest Frame'

sxaddpar,out_hdr_data,'TCD6_6',-sec*cospa
sxaddpar,out_hdr_data,'TCD7_6', sec*sinpa
sxaddpar,out_hdr_data,'TCD6_7', sec*sinpa
sxaddpar,out_hdr_data,'TCD7_7', sec*cospa

sxaddpar,out_hdr_data,'TPA6_7',pa,'positional angle'

sp_idx = 0L
for s=0L,n_slits-1 do begin
        xpos_arr[s]=(mask[s].x)*(arcsecmm/arcsecpix)
        ypos_arr[s]=(mask[s].y)*(arcsecmm/arcsecpix)

        data_tbl[s].SPAX_ID[0]=mask[s].OBJECT
        data_tbl[s].GROUP_N=s+1L

        data_tbl[s].SPEC_ID[0]=s+1L
        data_tbl[s].XPOS[0]=xpos_arr[s]
        data_tbl[s].YPOS[0]=-ypos_arr[s]
        data_tbl[s].DATA_SPE=float(inp_data[*,s])
        data_tbl[s].STAT_SPE=float(err_data[*,s])
        bflag=where((finite(inp_data[*,s]) ne 1) or $
                    (finite(err_data[*,s]) ne 1), bcnt)
        if(bcnt gt 0) then data_tbl[s].QUAL_SPE[bflag]=2l^30
endfor

pressure=double(sxpar(inp_hdr,'PRESSURE'))
outtemp=double(sxpar(inp_hdr,'OUTTEMP'))
airmass=sxpar(inp_hdr,'AIRMASS')

group_tbl=replicate({$
    GROUP_N:1b,       $
    G_SHAPE:'RECTANGLE', $
    G_SIZE1:fwhm,       $
    G_ANGLE:pa,       $
    G_SIZE2:0.2d,$
    G_POSWAV:wl[nx/2],$
    G_AIRMAS:airmass, $
    G_PARANG:pa,$
    G_PRESSU:pressure,$
    G_TEMPER:outtemp, $
    G_HUMID:10d       $    ;;; no humidity, assume 10%
},n_slits)

for i=0,n_slits-1 do begin
    group_tbl[i].g_size2=mask[i].width*arcsecmm
    group_tbl[i].g_angle=group_tbl[i].g_angle+mask[i].theta
endfor

writefits,out_file,0,out_hdr_pri
mwrfits,data_tbl,out_file,out_hdr_data,logical_cols=[2]
mwrfits,group_tbl,out_file,out_hdr_group

end

