n_l_str=replicate({$
    n_amp: 0,$
    xmin:0,$
    xmax:0,$
    ymin:0,$
    ymax:2047,$
    c_poly:dblarr(10),$
    bias:0.0,$
    saturation:65000.0-9000.0,$
    std_fit:1.0},2048)

n_l_str[*].c_poly[1]=1.0
n_l_str[*].xmin=findgen(2048)
n_l_str[*].xmax=findgen(2048)

for i=0,31 do n_l_str[i*64:(i+1)*64-1].n_amp=i

writefits,'ramp1.475_nonlin.fits',0
mwrfits,n_l_str,'ramp1.475_nonlin.fits'
writefits,'ramp4.5_nonlin.fits',0
mwrfits,n_l_str,'ramp4.5_nonlin.fits'
writefits,'ramp1_nonlin.fits',0
mwrfits,n_l_str,'ramp1_nonlin.fits'
writefits,'ramp4_nonlin.fits',0
mwrfits,n_l_str,'ramp4_nonlin.fits'

end
