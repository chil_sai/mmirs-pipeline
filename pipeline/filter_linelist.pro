;    line_data: array (3,N_lines): wavelength  flag  flux
;        flag values of 1, 2, and 3 denote lines being used 
;        in the wavelength solution compuation
;
;    fwhm: filter width in A


function filter_linelist,line_data,fwhm

if(n_params() eq 1) then fwhm=5.0 ;;; 5.0A
s_line=size(line_data)
if(s_line[0] ne 2 or s_line[1] ne 3) then begin
    print,'line_data has invalid format. Returning it.'
    return,line_data
endif

n_lines=s_line[2]
line_filt=fltarr(4,n_lines)*!values.f_nan

n_acc=0
s_flux=sort(line_data[2,*])
line_sort_flux=line_data[*,s_flux]
flag_line=bytarr(n_lines)+1

for i=0,n_lines-1 do begin
    if(flag_line[i] eq 1) then begin
        wl_cur=line_sort_flux[0,i]
        l_fwhm=where(abs(line_sort_flux[0,*]-wl_cur) le fwhm and abs(line_sort_flux[2,*]/line_sort_flux[2,i]-1.0) lt 0.25) ;; must be at least one line
        ;flag_line[l_fwhm]=0
        line_filt[0,n_acc]=total(line_sort_flux[0,l_fwhm]*line_sort_flux[2,l_fwhm])/total(line_sort_flux[2,l_fwhm])
        line_filt[1,n_acc]=(line_sort_flux[2,i] ge 0.99*max(line_sort_flux[2,l_fwhm]))? line_sort_flux[1,i] : 0
        line_filt[2,n_acc]=total(line_sort_flux[2,l_fwhm])
        line_filt[3,n_acc]=line_filt[0,n_acc]-line_sort_flux[0,i]
        n_acc++
    endif
endfor

line_filt=line_filt[*,0:n_acc-1]
line_filt=line_filt[*,sort(line_filt[0,*])]

return,line_filt

end
