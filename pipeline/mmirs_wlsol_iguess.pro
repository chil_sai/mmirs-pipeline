;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;  This function returns the initial guess for the MMIRS wavelength solution
;  Input parameters are:
;           grism -- grism ID, one of 'H', 'K', or 'HK'
;           xpos  -- X position of the slit (in a mask or a longslit)
;  Optional input keywords:
;           order -- order of dispersion. Defaults to 1
;  Return value:
;           polynomial coefficients of the wavelength solution
;           if xpos contains more than one element then the output array
;           will be two-dimensional (N_deg+1) by n_elements(xpos)
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

function mmirs_wlsol_iguess,grism,xpos0,ypos=ypos,order=order,period=period

if(n_elements(order) ne 1) then order=1
if(n_params() eq 1) then xpos0=[0.0]
if(n_elements(ypos) ne n_elements(xpos0)) then ypos=xpos0*0.0
if(n_elements(period) ne 1) then period='2011B'

xpos=xpos0
;;; slit curvature correction
;for i=0,n_elements(xpos)-1 do $
;    xpos[i]=xpos0[i]+poly(ypos[0],[-0.0130282,0.00200443,-0.000107925])

if(grism eq 'HK') then begin
    dis_coeff = [9595.8143,6.5797586,0.00014144125,-3.3057739e-08]
    wl0_coeff=[11702.5,-205.63,-0.007]
    dwl_coeff=[6.6139868,0.000322,-8.1e-5]

    if(period eq '2014B' or float(strmid(period,0,4)) gt 2014) then begin
        dis_coeff = [9595.8143,6.5797586,9.96065e-5,-2.38626e-8]
        wl0_coeff=[11452.5,-205.63,-0.007]
        dwl_coeff=[6.6130728,0.000322,-8.1e-5]
;        dis_coeff = [9595.8143,6.5797586,0.00014144125,-3.3057739e-08]
;        wl0_coeff=[11462.5,-205.63,-0.007]
;        dwl_coeff=[6.6139868/1.0073,0.000322,-8.1e-5]
    endif
    if(period eq '2016C' or float(strmid(period,0,4)) gt 2016) then begin
;        dis_coeff = [13295.343,6.5941120,9.8782661e-05,-2.5284456e-08]
;        wl0_coeff = [11496.245,-207.15677,-0.073285678,0.0061432073]
;        dwl_coeff = [6.6155533,   0.00032816266,  -6.2248268e-06]
        dis_coeff = [11141.582, 6.5973949, 9.9138640e-05, -2.5284456e-08]
        wl0_coeff = [11495.777, -207.18387, -0.057443659, 0.0074041495]
        dwl_coeff = [6.6123095 ,  0.00090555215 , -0.00011998737]
    endif
    slit_curv=-4.0 ;;; slit curvature in pixels mid-to-edge
endif

if(grism eq 'J') then begin
    ;;; old detector (up-to 2014A)
    dis_coeff = [9145.00,2.47490,6.41e-05,-6.64e-09]
    wl0_coeff=[10031.140-85,-73.944980,-0.014848005]
    dwl_coeff=[2.4086427,-0.00044018442,-7.6000814e-06]

    if(period eq '2014B' or float(strmid(period,0,4)) gt 2014) then begin
        ;;; new detector (2014B->)
        dis_coeff = [10815.420,2.4031563,6.4608235e-05,-6.9361336e-09]
        wl0_coeff = [9945.1256,      -73.748349,    -0.010474084]
        dwl_coeff = [2.4022695,  -0.00052479271,  -1.0260228e-05]
    endif

    slit_curv=-8.0
endif

if(grism eq 'H') then begin
    dis_coeff = [13565.019,       3.3732422,   5.0e-05]
    wl0_coeff=[13876.562,      -102.86578,    -0.067359223]
    dwl_coeff=[3.3322425,    0.0011861471, 5.4237967e-05]
    slit_curv=-4.0
endif

if(grism eq 'H3000') then begin
    dis_coeff = [15082.059,       1.9981119,   3.5524321e-05,  -5.8735888e-09]
    wl0_coeff = [14441.983,      -61.165411,     0.018237151]
    dwl_coeff = [1.9918299,  -0.00093527097,  -1.3795452e-05]
    if(period eq '2024A' or float(strmid(period,0,4)) gt 2023) then wl0_coeff[0]=14460.0
    slit_curv=-6.0
endif

if(grism eq 'K3000') then begin
;;;    dis_coeff = [18921.661,       2.9565306,   2.1352512e-05]
;;;    wl0_coeff=[18921.661,      -91.65,    -0.060]
;;;    dwl_coeff=[2.9565306,    0.0011861471, 5.4237967e-05]
;    dis_coeff = [18635.737,       2.9362999,   4.3966376e-05,  -7.9690282e-09]
;    wl0_coeff=[18998.448,      -90.426496,    0.0039595451]
;    dwl_coeff=[2.9396957,   -0.0011624027,  -1.4693169e-05]
;;; new determination made on 2016/04/27 from a 2015/09/27 dataset
    dis_coeff = [19908.873,       2.9488956,   3.9561132e-05,  -7.3657642e-09]
    wl0_coeff = [18997.906,      -90.708012,   -0.0077512554]
    dwl_coeff = [2.9410455,  -0.00064641492,   2.0744779e-05]

    if(period eq '2019C' or float(strmid(period,0,4)) gt 2019) then begin
        dis_coeff = [20198.237, 2.9470309, 3.8479197e-05, -6.4098828e-09]
        wl0_coeff = [18939.896, -90.587554, 0.0018181490]
        dwl_coeff = [2.9479323, -0.00035674183, -1.3129556e-05]
    endif

    if(period eq '2020C' or float(strmid(period,0,4)) gt 2020) then begin
        dis_coeff = [20236.018, 2.9437165, 3.7701765e-05, -6.3733179e-09]
        wl0_coeff = [18989.320, -90.245761, -0.027007799]
        dwl_coeff = [2.9459627, -0.00099419593, -3.8357354e-05]
    endif

    slit_curv=-10.0
endif

xpos=xpos0+poly(ypos,[0.,0.,-slit_curv/33.5^2*dwl_coeff[0]/wl0_coeff[1]])

if(n_elements(xpos) gt 1) then begin
    dis_arr = dblarr(n_elements(dis_coeff),n_elements(xpos))
    for i=0,n_elements(xpos)-1 do dis_arr[*,i]=dis_coeff[*]
    dis_arr[0,*]=transpose(poly(xpos,wl0_coeff))
    dis_arr[1,*]=transpose(poly(xpos,dwl_coeff))
    if(grism eq 'HK') then dis_arr[1,*]=transpose(poly(xpos-0*1.5*sqrt(xpos^2+ypos^2),dwl_coeff))
endif else begin
    dis_arr=dis_coeff
    dis_arr[0]=poly(xpos,wl0_coeff)
    dis_arr[1]=poly(xpos,dwl_coeff)
    if(grism eq 'HK') then dis_arr[1]=poly(xpos-0*1.5*sqrt(xpos^2+ypos^2),dwl_coeff)
endelse

dis_arr=dis_arr/double(order)
;print,'DIS_ARR',dis_arr
return, dis_arr

end
