pro mmirs_create_wavesol_ms,logfile,slit=slit,ndeg=ndeg,smy=smy,$
    debug=debug,plot=plot,y_ndeg=y_ndeg,oh=oh,no3d=no3d,quick=quick,adjusted=adjusted

aaa=''
if(n_elements(ndeg) ne 1) then ndeg=3
if(n_elements(y_ndeg) ne 1) then y_ndeg=3
wdir=def_wdir(logfile)
grism=def_grism(logfile,filter=filter)
period=get_mmirs_period(wdir+'obj_dark.fits', year=year, exten=1)

if(keyword_set(quick)) then begin
    Ndeg=(grism eq 'HK')? 3 : 3
endif

dist_map_a=mrdfits(wdir+'dist_map.fits',1)

if keyword_set(quick) then begin
  h1=headfits(wdir+'obj_dark.fits',ext=1)
  Nx=sxpar(h1,'NAXIS1')
  Ny_a=sxpar(h1,'NAXIS2')
  mask_a=read_mask_mmirs_ms(wdir+'mask_mos.txt',logfile=logfile)
endif else begin
  h1=headfits(wdir+'arc_dark.fits',ext=1)
  Nx=sxpar(h1,'NAXIS1')
  Ny_a=sxpar(h1,'NAXIS2')
  mask_a=read_mask_mmirs_ms(wdir+'mask_mos.txt',logfile=logfile)
endelse
n_slits_a=n_elements(mask_a)

if(n_elements(slit) eq 0) then slit=findgen(n_slits_a)
n_slits=n_slits_a

y_slits_a=get_slit_region(mask_a,Nx=Nx,Ny=Ny_a,ratio=1.0,dist_map=dist_map_a,slit_geom=slit_geom_a)

pix_mask_str={y_pix:0,x_mask:!values.f_nan,y_mask:!values.f_nan,$
    w_pix:!values.f_nan,h_pix:!values.d_nan,slit:-1,$
    wl_sol:dblarr(Ndeg+1)+!values.d_nan,wl_s_err:!values.d_nan}
pix_mask_data_a=replicate(pix_mask_str,Ny_a)

tags=tag_names(slit_geom_a[0])
for i=0,n_elements(tags)-1 do pix_mask_data_a.(i)=slit_geom_a.(i)

disper_all_a=dblarr(Ndeg+2,Ny_a)

if(~keyword_set(no3d) and (n_slits_a ge 8)) then begin
    data_3d_a=dblarr(4,500000l)
    errdata_a=dblarr(500000l)
    np_a=0l
    flag_3d_a=1
endif else begin
    data_3d_a=dblarr(4,500000l)
    errdata_a=dblarr(500000l)
    flag_3d_a=0
    np_a=0l
endelse

for j=0,n_slits-1 do begin
    i=slit[j]
    ymin_cur = y_slits_a[0,i]
    ymax_cur = y_slits_a[1,i]

    maskentry=mask_a[i]
    if(~keyword_set(quick) and (strcompress(maskentry.type,/remove_all) ne 'TARGET')) then continue

    print,'Processing slit: ',i+1,'/',n_slits,ymin_cur,ymax_cur
    y_scl=30.2101d
    if(tag_exist(dist_map_a,'mask_y_scl')) then y_scl=dist_map_a.mask_y_scl

    fwhm=(maskentry.width*y_scl) > 2.0
    
    if(keyword_set(quick)) then begin
        hdr_slit=headfits(wdir+'obj_slits.fits',ext=i+1)
        xpos=mask_a[i].x ;;sxpar(hdr_slit,'SLITX')
        ypos=mask_a[i].y ;;sxpar(hdr_slit,'SLITY')
        xpos_cur=xpos+dblarr(ymax_cur-ymin_cur+1)
        ypos_cur=ypos+(findgen(ymax_cur-ymin_cur+1)-(ymax_cur-ymin_cur)/2)/y_scl

        for y=ymin_cur,ymax_cur do begin
            wltmp=mmirs_wlsol_iguess(grism,xpos_cur[y-ymin_cur],ypos=ypos_cur[y-ymin_cur],order=order,period=period)
            disper_all_a[0:n_elements(wltmp)-1,y]=wltmp
            disper_all_a[n_elements(wltmp)-1:*,y]=0d
            pix_mask_data_a[y].wl_sol=wltmp
            pix_mask_data_a[y].wl_s_err=0d
        endfor
    endif else begin
        status_slit=crea_disper_mmirs_slit(logfile,i,fwhm,Ndeg,yfit='Yfit',plot=plot,NdegY=y_ndeg,$
            smy=smy,pssuff='_slit'+string(i+1,format='(i4.4)'),$
            ymin_out=ymin_cur,ymax_out=ymax_cur,oh=oh,disper_par=disper_par_slit,debug=debug,pos_lines_str=pos_lines_str)
        if(status_slit eq 0) then begin
            if(flag_3d_a eq 1 and j lt n_slits_a) then begin
                nl=n_elements(pos_lines_str)
                for k=0,nl-1 do begin
                    pos_lines_cur=pos_lines_str[k]
                    np_cur=n_elements(pos_lines_cur.xpix)
                    data_3d_a[0,np_a:np_a+np_cur-1]=pos_lines_cur.xpix
                    data_3d_a[1,np_a:np_a+np_cur-1]=pos_lines_cur.xmask
                    data_3d_a[2,np_a:np_a+np_cur-1]=pos_lines_cur.ypix
                    data_3d_a[3,np_a:np_a+np_cur-1]=pos_lines_cur.wl
                    errdata_a[np_a:np_a+np_cur-1]=1.0/((pos_lines_cur.flux)>0.01)^0.25
                    np_a+=np_cur
                endfor
            endif
            ;;dtmp=readfits(wdir+'disper.fits',/silent)
            dtmp=disper_par_slit
            ymax_cur=ymax_cur<(n_elements(disper_all_a[0,*])-1)
            disper_all_a[*,ymin_cur:ymax_cur]=dtmp[*,0:(ymax_cur-ymin_cur)]
            pix_mask_data_a[ymin_cur:ymax_cur].wl_sol=dtmp[0:Ndeg,0:(ymax_cur-ymin_cur)]
            pix_mask_data_a[ymin_cur:ymax_cur].wl_s_err=transpose(dtmp[Ndeg+1,0:(ymax_cur-ymin_cur)])
            if(keyword_set(debug)) then begin
                aaa=''
                npix_cur=n_elements(where(finite(pix_mask_data_a[ymin_cur:ymax_cur].y_mask) eq 1))
                print,'npix=',ymax_cur-ymin_cur+1,'/',npix_cur                    
                ;read,aaa
            endif
        endif
    endelse
endfor

writefits,wdir+'disper.fits',disper_all_a
mwrfits,disper_all_b,wdir+'disper.fits'
writefits,wdir+'disper_table.fits',0
mwrfits,pix_mask_data_a,wdir+'disper_table.fits'

if(flag_3d_a eq 1 and ~keyword_set(quick)) then begin
    writefits,wdir+'wl_data_3d.fits',0
    h_empty=headfits(wdir+'wl_data_3d.fits')
    sxaddpar,h_empty,'NP_A',np_a
    writefits,wdir+'wl_data_3d.fits',0,h_empty
    mwrfits,data_3d_a[*,0:np_a-1],wdir+'wl_data_3d.fits',/silent
    mwrfits,errdata_a[0:np_a-1],wdir+'wl_data_3d.fits',/silent

    mmirs_adjust_wavesol_ms,logfile,suff='_adj',wlndeg=ndeg,flag_3d_a=flag_3d_a,slit=slit
    adjusted=1
endif else adjusted=0

end
