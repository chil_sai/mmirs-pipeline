pro preproc_mmirs,conf_str,rawext=rawext,verbose=verbose,crosstalk=crosstalk,nobadamp=nobadamp

badamp=(keyword_set(nobadamp))? -1 : 20

suffix='.fix.fits'
if(rawext eq '.gz' or rawext eq '.bz2') then compress=rawext else rawext=''

if((strlen(conf_str.sci[0]) gt 0) and (strlen(conf_str.sci_dark[0]) gt 0)) then begin
    biasframe=conf_str.rawdir+conf_str.sci_dark[0]+'.fits'
    if(~file_test(conf_str.rdir+conf_str.sci+suffix+rawext)) then mmfixen_nonlin,badamp=badamp,conf_str.rawdir+conf_str.sci+'.fits',conf_str.rdir+conf_str.sci+suffix,biasframe=biasframe,compress=compress,verbose=verbose,crosstalk=crosstalk
    for i=0,n_elements(conf_str.sci_dark)-1 do begin
        if(~file_test(conf_str.rdir+conf_str.sci_dark[i]+suffix+rawext)) then mmfixen_nonlin,badamp=badamp,conf_str.rawdir+conf_str.sci_dark[i]+'.fits',conf_str.rdir+conf_str.sci_dark[i]+suffix,biasframe=conf_str.rawdir+conf_str.sci_dark[i]+'.fits',compress=compress,verbose=verbose,crosstalk=crosstalk
    endfor
endif

if((strlen(conf_str.arc[0]) gt 0) and (strlen(conf_str.arc_dark[0]) gt 0)) then begin
    biasframe=conf_str.rawdir+conf_str.arc_dark[0]+'.fits'
    for i=0,n_elements(conf_str.arc)-1 do begin
        if(~file_test(conf_str.rdir+conf_str.arc[i]+suffix+rawext)) then mmfixen_nonlin,badamp=badamp,conf_str.rawdir+conf_str.arc[i]+'.fits',conf_str.rdir+conf_str.arc[i]+suffix,biasframe=biasframe,compress=compress,verbose=verbose,crosstalk=crosstalk
    endfor
    for i=0,n_elements(conf_str.arc_dark)-1 do begin
        if(~file_test(conf_str.rdir+conf_str.arc_dark[i]+suffix+rawext)) then mmfixen_nonlin,badamp=badamp,conf_str.rawdir+conf_str.arc_dark[i]+'.fits',conf_str.rdir+conf_str.arc_dark[i]+suffix,biasframe=conf_str.rawdir+conf_str.arc_dark[i]+'.fits',compress=compress,verbose=verbose,crosstalk=crosstalk
    endfor
endif

if((strlen(conf_str.flat[0]) gt 0) and (strlen(conf_str.flat_dark[0]) gt 0)) then begin
    biasframe=conf_str.rawdir+conf_str.flat_dark[0]+'.fits'
    for i=0,n_elements(conf_str.flat)-1 do begin
        if(~file_test(conf_str.rdir+conf_str.flat[i]+suffix+rawext)) then mmfixen_nonlin,badamp=badamp,conf_str.rawdir+conf_str.flat[i]+'.fits',conf_str.rdir+conf_str.flat[i]+suffix,biasframe=biasframe,compress=compress,verbose=verbose,crosstalk=crosstalk
    endfor
    for i=0,n_elements(conf_str.flat_dark)-1 do begin
        if(~file_test(conf_str.rdir+conf_str.flat_dark[i]+suffix+rawext)) then mmfixen_nonlin,badamp=badamp,conf_str.rawdir+conf_str.flat_dark[i]+'.fits',conf_str.rdir+conf_str.flat_dark[i]+suffix,biasframe=conf_str.rawdir+conf_str.flat_dark[i]+'.fits',compress=compress,verbose=verbose,crosstalk=crosstalk
    endfor
endif

if((strlen(conf_str.misc[0]) gt 0) and (strlen(conf_str.misc_dark[0]) gt 0)) then begin
    biasframe=conf_str.rawdir+conf_str.misc_dark[0]+'.fits'
    for i=0,n_elements(conf_str.misc)-1 do begin
        if(~file_test(conf_str.rdir+conf_str.misc[i]+suffix+rawext)) then mmfixen_nonlin,badamp=badamp,conf_str.rawdir+conf_str.misc[i]+'.fits',conf_str.rdir+conf_str.misc[i]+suffix,biasframe=biasframe,compress=compress,verbose=verbose,crosstalk=crosstalk
    endfor
    for i=0,n_elements(conf_str.misc_dark)-1 do begin
        if(~file_test(conf_str.rdir+conf_str.misc_dark[i]+suffix+rawext)) then mmfixen_nonlin,badamp=badamp,conf_str.rawdir+conf_str.misc_dark[i]+'.fits',conf_str.rdir+conf_str.misc_dark[i]+suffix,biasframe=conf_str.rawdir+conf_str.misc_dark[i]+'.fits',compress=compress,verbose=verbose,crosstalk=crosstalk
    endfor
endif

end
