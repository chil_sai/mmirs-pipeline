pro flat_fielding_mmirs_ms,logfile,image_type,$
    norm_slit=norm_slit,dymask=dymask,$
    sub_sc_flat=sub_sc_flat,sub_sc_sci=sub_sc_sci,debug=debug

message,'multislit flat-field correction ',/cont

if(n_elements(dymask) ne 1) then dymask=2

wdir=def_wdir(logfile)
grism=def_grism(logfile,filter=filter)
slit=def_slit(logfile)
flat=transpose(mrdfits(wdir+'flat_dark.fits',1,h,/silent))
Nx=sxpar(h,'NAXIS2')
Ny=sxpar(h,'NAXIS1')

dist_map=mrdfits(wdir+'dist_map.fits',1)

flat_orig=flat
flat_r=poly_2d(flat,dist_map.kxwrap,dist_map.kywrap,1)
flat=flat_r
flat_r=0

if(keyword_set(sub_sc_flat)) then begin
    flat_sc=crea_sc_mmirs_ms(logfile,'flat',debug=debug)
    flat=flat-flat_sc
endif

flat_norm=flat+!values.f_nan
flat_sm=flat_norm

mask=read_mask_mmirs_ms(wdir+'mask_mos.txt',logfile=logfile)
dist_map=mrdfits(wdir+'dist_map.fits',1)

n_slits=n_elements(mask)

y_slits=get_slit_region(mask,Nx=Nx,Ny=Ny,ratio=1.2,dist_map=dist_map)

flat_ncoeff=flat_norm*0.0

for i=0,n_slits-1 do begin
    ymin_cur = y_slits[0,i]
    ymax_cur = y_slits[1,i]
    print,'Processing slit: ',i+1,'/',n_slits,ymin_cur,ymax_cur
    for y=ymin_cur,ymax_cur do begin
        line_cur=median(flat[*,y],15)
        line_cur[0:9]=0.0    ;;; cut the edges
        line_cur[Nx-10:Nx-1]=0.0
        flat_sm[*,y]=smooth(line_cur,31,/nan)
    endfor
    
    maxv=(n_elements(norm_slit) eq 1 or ymin_cur eq ymax_cur)? 1.0 : max(median(flat_sm[*,ymin_cur:ymax_cur],dim=2),/nan)
;    plot,median(flat_sm[*,ymin_cur:ymax_cur],dim=2),xs=1,title='Slit'+string(i)
;    oplot,median(flat[*,ymin_cur:ymax_cur],dim=2),col=254
;    tt=''
;    read,tt

;;;;;;;;;;;;;;;; ATTENTION -- No smoothing in flat_norm
    flat_norm[*,ymin_cur:ymax_cur]=flat[*,ymin_cur:ymax_cur]/maxv
    flat_ncoeff[*,ymin_cur:ymax_cur]=maxv
endfor

if(n_elements(norm_slit) eq 1) then begin
    if(norm_slit lt 0) then begin
        max_vals=dblarr(n_slits)+!values.d_nan
        for i=0,n_slits-1 do $
            if(mask[i].type eq 'TARGET') then $
                max_vals[i]=max(flat_sm[*,y_slits[0,i]:y_slits[1,i]],/nan)
        d_ncoeff=median(max_vals)
    endif else d_ncoeff=max(flat_sm[*,y_slits[0,norm_slit]:y_slits[1,norm_slit]],/nan)
    flat_ncoeff=flat_ncoeff*d_ncoeff
endif

flat_ncoeff_geom=poly_2d(flat_ncoeff,dist_map.kxwrap_inv,dist_map.kywrap_inv,1)
flat_norm=flat_orig/flat_ncoeff_geom

flat_thr=(grism eq 'J' and filter eq 'zJ')? 0.05 : 0.1 ;;;; 0.05
low_flat=where(median(flat_norm,5) lt flat_thr,clf)
mask_flat=byte(flat_norm*0.0)
if(clf gt 0) then begin
    mask_flat[low_flat]=1
    mask_flat=mask_flat+shift(mask_flat,0,dymask)+shift(mask_flat,0,-dymask)
    flat_norm[where(mask_flat gt 0)]=!values.f_nan
endif

clean_hdr,h
sxaddpar,h,'datamin',0.8
sxaddpar,h,'datamax',1.2
mkhdr,h_fl,float(flat_norm),/extend
h_fl=[h_fl[0:5],h]
writefits,wdir+'flat_norm.fits',rotate(float(flat_norm),5),h_fl
writefits,wdir+'flat_ncoeff.fits',rotate(float(flat_ncoeff_geom),5),h_fl

for n=0,n_elements(image_type)-1 do begin
    print,'reading  '+wdir+IMAGE_TYPE[n]+'_dark.fits'
    obj=transpose(mrdfits(wdir+IMAGE_TYPE[n]+'_dark.fits',1,h,/silent))

    if(keyword_set(sub_sc_sci) and (image_type[n] ne 'arc')) then begin
        obj_sc=crea_sc_mmirs_ms(logfile,image_type[n],debug=debug)
        obj=obj-poly_2d(obj_sc,dist_map.kxwrap_inv,dist_map.kywrap_inv,1)
    endif

    clean_hdr,h
    mkhdr,h_out,float(flat_norm),/extend
    h_out=[h_out[0:5],h]
    sxaddhist,'flat-field corrected',h_out
    writefits,wdir+IMAGE_TYPE[n]+'_ff.fits',rotate(float(obj/flat_norm),5),h_out
endfor

return

END

