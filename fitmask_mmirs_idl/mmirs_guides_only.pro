function mmirs_guides_only,objlist,mask_params,field_geometry,gs_geometry=gs_geometry,box_geometry=box_geometry,$
guidestars=guidestars,uselst=uselst,$
    wfs_mag_lim=wfs_mag_lim,gs_min=gs_min,verbose=verbose

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; objlist is an array of the object structure:
;     .target -- target ID (longint)
;     .object -- object names (string)
;     .ra -- J2000 R.A., degrees (double precision)
;     .dec -- J2000 Dec, degrees (double precision)
;     .pmra -- R.A. proper motion mas/year (double precision)
;     .pmdec -- Dec proper motion mas/year (double precision)
;     .mag -- magnitude (float)
;     .type -- 'TARGET', 'BOX', 'GUIDE' (both for guide and WFS stars)
;     .priority -- priority for targets, larger numbers higher priority (float)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; field_geometry is a structure:
;     .mask -- bounbox of the mask in the focal plane, mm (float[4])
;     .guide -- boundboxes of guide star areas, mm (float[4,N])
;     .wfs -- boundbox of the WFS region, mm (float)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; slit_geometry is a structure:
;     .height -- slit height in arcsec (float)
;     .width  -- slit height in arcsec (flaat)
;     .gap    -- minimal acceptable gap between slits in arcsec (float)
;
; box_geometry is a structure of the same type as slit_geometry, .gap from slit_geometry will be used
; gs_geometry is a structure of the same type as slit_geometry
; wfs_mag_lim is the faint mag limit for wfs stars (which is brighter
; than the general guide star limit)  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

if(n_elements(gs_geometry) ne 1) then gs_geometry=slit_geometry
if(n_elements(wfs_mag_lim) ne 1) then wfs_mag_lim=15.0
if(n_elements(gs_min) ne 1) then gs_min=1
guide_limits={wfs_mag_lim:wfs_mag_lim, wfs_bright_lim:11.0,guide_mag_lim:18.5,guide_bright_lim:14.0, ng_limit:10}


mmirs_expand_mask_params,mask_params
if(keyword_set(uselst)) then begin
    ; no proper motion correction possible
    ra_corr=objlist.ra
    dec_corr=objlist.dec
    lst=(mask_params.ha - mask_params.ra)/15d
    if(lst lt 0) then lst+=24d
    if(lst ge 24) then lst-=24d
    mmirs_radec2xy,$
        ra_corr,dec_corr,$
        mask_params.ra,mask_params.dec,mask_params.posa,lst=lst,objx,objy
 endif else begin
    ; proper motion correction
    mjd_in=mask_params.mjdstart+mask_params.duration/60d/24d/2d
    j_now=(mjd_in-51544.4d)/365.25d + 2000d
    ra_corr=objlist.ra+objlist.pmra*(j_now-objlist.epoch)/cos(objlist.dec/!radeg)/(1000d*3600d) ;milliarsec/yr
    dec_corr=objlist.dec+objlist.pmdec*(j_now-objlist.epoch)/(3600d*1000d)
    mmirs_radec2xy,$
        ra_corr,dec_corr,$
        mask_params.ra,mask_params.dec,mask_params.posa,$
        mjd=mjd_in,objx,objy
 endelse
 
gs_id = where(objlist.type eq 'GUIDE',n_guidestars)
guidestars=replicate(mmirs_guidestar_stub(),n_guidestars)

guidestars.ra=ra_corr[gs_id]
guidestars.dec=dec_corr[gs_id]
guidestars.x=objx[gs_id]
guidestars.xmask=guidestars.x
guidestars.y=objy[gs_id]
;for k=0, n_guidestars-1 do if objx[gs_id[k]] > 0 then guidestars[k].xmask= 1.000079d*(objx[gs_id[k]]-56.236)+56.2356 else guidestars[k].xmask = 1.000079d*(objx[gs_id[k]]+56.236)-56.2356  ;56.2356 factor, to be consistent with BinoMask treatment of slits 
guidestars.ymask=guidestars.y;4321.72d*asin(objy[gs_id]/4321.72d)
guidestars.mag=objlist[gs_id].mag
guidestars.pmra=objlist[gs_id].pmra
guidestars.pmdec=objlist[gs_id].pmdec
guidestars.epoch=objlist[gs_id].epoch
guidestars.object=objlist[gs_id].object



;new function to select the guide stars
guidestars=mmirs_filter_guides(guidestars, mask_params,gs_geometry,field_geometry, guide_limits, gs_min,valid=valid, verbose=verbose)
if (valid eq 0) then return, -1
return,1.0

end
