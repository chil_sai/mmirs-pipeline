function compute_bgr_amp,img,npixbg=npixbg,namp=namp,fullframe=fullframe,meanbgr=meanbgr
    if(n_elements(npixbg) ne 1) then npixbg=4
    s_img=size(img)

    if(n_elements(namp) ne 1) then namp=32
    bgr_val=dblarr(namp)

    npixamp=s_img[1]/namp

    if(keyword_set(fullframe)) then begin
        for i=0,namp-1 do begin
            h_v=histogram(img[i*npixamp:(i+1)*npixamp-1,*],min=0.,max=32000.,/nan)

            max_h_v=max(h_v,val)
            h_v_1=histogram(img[i*npixamp:(i+1)*npixamp-1,*],min=val-5.,max=val+5.,binsize=0.05,/nan)
            ;h_v_1=smooth(median(h_v_1,3),9,/nan)
            ;max_h_v_1=max(h_v_1,val_1)
            p=mpfitpeak(dindgen(n_elements(h_v_1)),h_v_1,p_peak,/gauss)
            val_1=p_peak[1]
            bgr_val[i]=val-5.0+val_1*0.05
;            print,'BGR_VAL:',i,bgr_val[i],val,val_1,i*npixamp,(i+1)*npixamp-1
;            oplot,p,col=20+235*randomu(seed,1)
        endfor
    endif else begin
        resistant_mean,img[*,[lindgen(npixbg),s_img[2]-npixbg+lindgen(npixbg)]],1.0,prof_bgr,dim=2
        for i=0,namp-1 do begin
            resistant_mean,prof_bgr[i*npixamp:(i+1)*npixamp-1],1.0,val
            bgr_val[i]=val
        endfor
    endelse

    meanbgr=total(bgr_val)/namp
    bgr_val_full=dblarr(s_img[1])
    for i=0,namp-1 do bgr_val_full[i*npixamp:(i+1)*npixamp-1]=bgr_val[i]
    bgr_img=rebin(bgr_val_full,s_img[1],s_img[2])

    return,bgr_img
end

pro mmirs_reduce_imaging,$
   scilist,darklist,skylist=skylist,nobgr=nobgr,nosecbgr=nosecbgr,subglobbgr=subglobbgr,nopixflat=nopixflat,$
   flatreg=flatreg,skyflatreg=skyflatreg,starxy=starxy,max_ell=max_ell,seeing_limits=seeing_limits,$
   nstdsky=nstdsky,skynblock=skynblock,smw_mask=smw_mask,skycombmin=skycombmin,$
   rawdir=rawdir,rdir=rdir,wdir=wdir,output_prefix=output_prefix,azref=azref,elref=elref

;+
;  NAME: 
;       MMIRS_REDUCE_IMAGING
;  PURPOSE:
;       Simple imaging pipeline for MMIRS data
;  EXPLANATION:
;       This routine performs simple stacking and offseting of MMIRS images
;       and runs the up-the-ramp fitting if needed.
;
;  CALLING SEQUENCE: 
;       mmirs_reduce_imaging, scilist, darklist, skylist=skylist,$
;           rawdir=rawdir, rdir=rdir, wdir=wdir, output_prefix=output_prefix,$
;           starxy=starxy, flatreg=flatreg, skyflatreg=skyflatreg,$
;           max_ell=max_ell,seeing_limits=seeing_limits,$
;           nobgr=nobgr, subglobbgr=subglobbgr, nopixflat=nopixflat
;  CATEGORY:
;       CfA MMIRS pipeline
;
;  INPUTS:     
;       SCILIST - a list of dithered science images, string array N>1
;       DARKLIST - a list of dark frames
;       STARXY=STARXY   - coordinates of a non-saturated star on the first image of a sequence.
;                         Will be automated later
;       RAWDIR=RAWDIR - a directory containing raw data (MMIRS archive)
;       RDIR=RDIR     - a directory containing up-the-ramp fitted data (preproc/).
;                       MMFIXEN_NONLIN will be run if the file is not present and 
;                       the output will be stored in this directory
;       WDIR=WDIR     - a directory where output data will be written
;       OUTPUT_PREFIX=OUTPUT_PREFIX - a prefix for the output image filename
;
;  OPTIONAL INPUT PARAMETERS:
;       SKYLIST - a list of sky background exposures. Can be the same as SCLIST.
;                 Defaults to SCILIST if not set
;       MAX_ELL - a maximal value of accepted ellipticity for point sources. Defaults to 0.3. 
;                 Images where the stellar ellipticity exceeds this value will be rejected
;       SEEING_LIMITS - limits for accepted seeing quality in arcsec. Defaults to [0.3, 1.2]
;       FLATREG - a 4-element array [xmin,xmax,ymin,ymax] to specify a region 
;                         on a science frame where the flux normalization should be performed.
;                         Defaults to [1100,1199,1100,1199]
;       SKYFLATREG - same as FLATREG but on a sky frame
;       /NOPIXFLAT - do not use pixel flat from MMIRS calibrations
;
;  OUTPUTS:   
;       none
;
;  PROCEDURE: 
;       Maximum pixel within distance from input pixel X  determined 
;       from FHWM is found and used as the center of a square, within 
;       which the centroid is computed as the Gaussian least-squares fit
;       to the  marginal sums in the X direction. 
;
;  EXAMPLE:
;
;  MODIFICATION HISTORY:
;       Written Jan-Apr 2019 by Igor Chilingarian (CfA)
;       Converted to 1D, I. Chilingarian, 2005
;-      


n_im=n_elements(scilist)
if(n_elements(skylist) lt 1) then skylist=scilist
n_bgr=n_elements(skylist)
n_dark=n_elements(darklist)
if(n_elements(flatreg) ne 4) then flatreg=[1100,1199,1100,1199]
if(n_elements(skyflatreg) ne 4) then skyflatreg=flatreg

if(n_elements(rawdir) ne 1) then rawdir='/home/mmirs/Archive/rawdata/data/MMIRS/2015.0329/'
if(n_elements(rdir) ne 1) then rdir='/d1/data1/mmirs/preproc/2015/2015.0329/'
if(n_elements(wdir) ne 1) then wdir='/d1/data1/mmirs/reduced/2015/2015.0329/xmm1653_K/'
if(n_elements(output_prefix) ne 1) then output_prefix='target'

if(n_elements(max_ell) ne 1) then max_ell=0.3d
ax_rat=0d > (1d -max_ell)< 1d

if(n_elements(seeing_limits) ne 2) then seeing_limits=[0.3d,1.2d] ;; FWHM seeing limits in arcsec

mkdir,wdir

conf_str={$
    rawdir:rawdir,rdir:rdir,wdir:wdir,$
    sci:['0000'],$
    sci_dark:darklist,$
    arc:[''],$
    arc_dark:[''],$
    flat:[''],$
    flat_dark:[''],$
    misc:[''],$
    misc_dark:['']}

conf_arr=replicate(conf_str,n_im)
s_mmirs=[1,2048,2048]
instaz_arr=fltarr(n_im)
instel_arr=fltarr(n_im)
for i=0,n_im-1 do begin
    conf_arr[i].sci[0]=scilist[i]
    print,'Processing and loading '+conf_arr[i].sci[0]
    preproc_mmirs,conf_arr[i],rawext='.gz',/ver
    h=headfits(conf_arr[i].rdir+conf_arr[i].sci+'.fix.fits.gz',ext=1)
    instaz_arr[i]=sxpar(h,'INSTAZ')
    instel_arr[i]=sxpar(h,'INSTEL')
endfor

azmin=min(instaz_arr,max=azmax)
elmin=min(instel_arr,max=elmax)

if(n_elements(azref) ne 1) then azref=azmin  ; (azmin+azmax)/2.
if(n_elements(elref) ne 1) then elref=elmax  ; (elmin+elmax)/2.
img_cube=fltarr(s_mmirs[1]+(elmax-elmin)/0.2,s_mmirs[2]+(azmax-azmin)/0.2,n_im)+!values.f_nan

instaz_arr-=azref
instel_arr-=elref

for i=0,n_im-1 do begin
    if(i eq 0) then begin
        dark=average_img(conf_arr[i].rdir+conf_arr[i].sci_dark+'.fix.fits.gz',/median)
        ;;if(~keyword_set(nosecbgr)) then dark-=compute_bgr_amp(dark)
        h_img=headfits(conf_arr[i].rdir+conf_arr[i].sci+'.fix.fits.gz',ext=1)
        detector=sxpar(h_img,'DETECTOR')
        filter=sxpar(h_img,'FILTER')
        pixflat_name=$
            getenv('MMIRS_PIPELINE_PATH')+'calib_MMIRS/detector/'+$
            ((detector eq 'mmirs-h2rg')? 'H2RG' : 'H2_56')+$
            '/pixflats/pixflat_'+filter+'.fits.gz'
        pixflat=(file_test(pixflat_name) eq 1 and ~keyword_set(nopixflat))? mrdfits(pixflat_name,1,/silent) : 1d
        if(n_elements(pixflat) gt 1) then pixflat[*,[0,1,2,3,2044,2045,2046,2047]]=1.0
    endif
    img_cur=(mrdfits(conf_arr[i].rdir+conf_arr[i].sci+'.fix.fits.gz',1,h,/silent)-dark)/pixflat
    if(~keyword_set(nosecbgr)) then img_cur-=compute_bgr_amp(img_cur)

    img_cur[0:3,*]=!values.f_nan
    img_cur[*,0:3]=!values.f_nan
    img_cur[2044:*,*]=!values.f_nan
    img_cur[*,2044:*]=!values.f_nan
    img_cube[0:s_mmirs[1]-1,0:s_mmirs[2]-1,i]=img_cur
endfor

n_x0=flatreg[0]
n_x1=flatreg[1]
n_y0=flatreg[2]
n_y1=flatreg[3]
;norm_prof=median(median(img_cube[n_x0:n_x1,n_y0:n_y1,*],dim=1),dim=1)
resistant_mean,img_cube[n_x0:n_x1,n_y0:n_y1,*],1.0,norm_prof_2d,dim=1
resistant_mean,norm_prof_2d,1.0,norm_prof,dim=1
norm_prof=norm_prof/median(norm_prof)
for i=0,n_im-1 do img_cube[*,*,i]=img_cube[*,*,i]/norm_prof[i]

if(~array_equal(scilist,skylist) and ~keyword_set(nobgr)) then begin
    if(n_elements(skynblock) ne 1) then skynblock=n_bgr
    bgr_cube=fltarr(s_mmirs[1],s_mmirs[2],n_bgr)+!values.f_nan
    conf_bgr=replicate(conf_str,n_bgr)
    for i=0,n_bgr-1 do begin
        conf_bgr[i].sci[0]=skylist[i]
        print,'Processing and loading background '+conf_bgr[i].sci[0]
        preproc_mmirs,conf_bgr[i],rawext='.gz',/ver
        if(i eq 0) then dark=average_img(conf_bgr[i].rdir+conf_bgr[i].sci_dark+'.fix.fits.gz',/median)
        bgr_cube[*,*,i]=(mrdfits(conf_bgr[i].rdir+conf_bgr[i].sci+'.fix.fits.gz',1,h,/silent)-dark)/pixflat
        if(~keyword_set(nosecbgr)) then bgr_cube[*,*,i]-=compute_bgr_amp(bgr_cube[*,*,i])
    endfor
    bgr_cube[0:3,*,*]=!values.f_nan
    bgr_cube[*,0:3,*]=!values.f_nan
    bgr_cube[2044:*,*,*]=!values.f_nan
    bgr_cube[*,2044:*,*]=!values.f_nan
;    norm_prof_bgr=median(median(bgr_cube[skyflatreg[0]:skyflatreg[1],skyflatreg[2]:skyflatreg[3],*],dim=1),dim=1)
    resistant_mean,bgr_cube[skyflatreg[0]:skyflatreg[1],skyflatreg[2]:skyflatreg[3],*],1.0,norm_prof_bgr_2d,dim=1
    resistant_mean,norm_prof_bgr_2d,1.0,norm_prof_bgr,dim=1
    norm_prof_bgr=norm_prof_bgr/median(norm_prof_bgr)
    for i=0,n_bgr-1 do bgr_cube[*,*,i]=bgr_cube[*,*,i]/norm_prof_bgr[i]

    if(skynblock gt n_bgr/2) then begin
        flat_raw=median(bgr_cube,dim=3)
        flat_cube=bgr_cube
    endif else begin
        n_bgrnew=fix(n_bgr/skynblock)
        flat_cube=fltarr(s_mmirs[1],s_mmirs[2],n_bgrnew)
        for z=0,n_bgrnew-1 do begin
            idxmin=z*skynblock
            idxmax=((z+1)*skynblock-1)
            if(z eq n_bgrnew-1) then idxmax=n_bgr-1
            print,'Processing background block #',strcompress(string(z,format='(i)'),/remove_all),' indices:',idxmin,idxmax
            if(keyword_set(skycombmin)) then begin
                bgrcur=min(bgr_cube[*,*,idxmin:idxmax],dim=3)
            endif else begin
                resistant_mean,bgr_cube[*,*,idxmin:idxmax],1.0,bgrcur,dim=3
            endelse
            flat_cube[*,*,z]=bgrcur
        endfor
        resistant_mean,flat_cube,1.0,flat_raw,dim=3
        bgr_cube=flat_cube
        n_bgr=n_bgrnew
    endelse
endif else begin
    flat_raw=(keyword_set(nobgr))? 1d : median(img_cube[0:s_mmirs[1]-1,0:s_mmirs[2]-1,*],dim=3)
    flat_cube=img_cube[0:s_mmirs[1]-1,0:s_mmirs[2]-1,*]
endelse

flat_n=(keyword_set(nobgr))? 1d : flat_raw/median(flat_raw)

if(n_elements(smw_mask) ne 1) then smw_mask=5

if(n_elements(nstdsky) ne 1) then nstdsky=3.0
if(~keyword_set(nobgr)) then begin
    print,'Computing flat...'
    for i=0,n_bgr-1 do begin
        flat_cur=flat_cube[*,*,i]
        flat_cur_n=flat_cur/flat_n
        f_x0=skyflatreg[0]
        f_x1=skyflatreg[1]
        f_y0=skyflatreg[2]
        f_y1=skyflatreg[3]
        val_med=median(flat_cur_n[f_x0:f_x1,f_y0:f_y1])
        val_std=robust_sigma(flat_cur_n[f_x0:f_x1,f_y0:f_y1])
        b_flat=where(abs(flat_cur_n-val_med) gt nstdsky*val_std, cb_flat)
        if(cb_flat gt 0 and cb_flat lt n_bgr) then begin
            mask_im=fix(flat_cur)*0
            mask_im[b_flat]=100
            mask_im=smooth(mask_im,smw_mask)
            b_mask=where(mask_im gt 0)
            flat_cur[b_mask]=!values.f_nan
            flat_cube[*,*,i]=flat_cur
        endif
    endfor

    ;flat=median(flat_cube,dim=3)
    resistant_mean,flat_cube,3.0,flat,dim=3
    print,'done'
    ;if(n_elements(pixflat) gt 1 and keyword_set(subglobbgr)) then flat=flat-median(flat,21)+median(flat)
endif else flat=flat_n

if(keyword_set(nobgr)) then begin
    flat=1d
    med_flat=1d
endif else begin
    med_flat=median(flat)
    flat/=med_flat
    med_flat=1d
endelse

flag_im=bytarr(n_im)+1
img_cube_flat=img_cube
meanbgr_arr=fltarr(n_im)
for i=0,n_im-1 do begin
    img_cube_flat[0:s_mmirs[1]-1,0:s_mmirs[2]-1,i]=img_cube[0:s_mmirs[1]-1,0:s_mmirs[2]-1,i]/flat
    if(keyword_set(subglobbgr)) then begin
        bgr=compute_bgr_amp(img_cube_flat[0:s_mmirs[1]-1,0:s_mmirs[2]-1,i],/fullframe,meanbgr=meanbgr)
        img_cube_flat[0:s_mmirs[1]-1,0:s_mmirs[2]-1,i]-=bgr
        meanbgr_arr[i]=meanbgr
    endif
endfor
for i=0,n_im-1 do img_cube_flat[*,*,i]=shift(img_cube_flat[*,*,i],-instel_arr[i]/0.2,instaz_arr[i]/0.2)

qc_struct=replicate({$
    filename:'',$
    dateobs:'',$ ;; TIMFIRST
    exptime:!values.f_nan,$
    rotangle:!values.f_nan,$
    ra_icrs:!values.f_nan,$
    dec_icrs:!values.f_nan,$
    instaz:!values.f_nan,$
    instel:!values.f_nan,$
    fwhmx:!values.f_nan,$
    fwhmy:!values.f_nan,$
    dx:!values.f_nan,$
    dy:!values.f_nan,$
    flag:-1$
},n_im)

if(n_elements(starxy) eq 2) then begin
    wsize=20
    xstar=starxy[0]-instel_arr[0]/0.2
    ystar=starxy[1]+instaz_arr[0]/0.2
    openw,u,wdir+output_prefix+'_stat.txt',/get_lun
    for i=0,n_im-1 do begin
        img_cur=img_cube_flat[*,*,i]*med_flat ; shift(img_cube[*,*,i]/flat*med_flat,-instel_arr[i]/0.2,instaz_arr[i]/0.2)
        if(instel_arr[i]/0.2 gt 0) then img_cur[s_mmirs[1]-ceil(instel_arr[i]/0.2)-1:*,*]=!values.f_nan
        if(instel_arr[i]/0.2 lt 0) then img_cur[0:ceil(-instel_arr[i]/0.2),*]=!values.f_nan
        if(instaz_arr[i]/0.2 gt 0) then img_cur[*,0:ceil(instaz_arr[i]/0.2)]=!values.f_nan
        if(instaz_arr[i]/0.2 lt 0) then img_cur[*,s_mmirs[2]-ceil(-instaz_arr[i]/0.2)-1:*]=!values.f_nan
        im_frag=img_cur[xstar-wsize:xstar+wsize,ystar-wsize:ystar+wsize] ;-med_flat
        bfrag=where(finite(im_frag) ne 1,cbfrag)
        if(cbfrag gt 0) then im_frag[bfrag]=0.0
        t=gauss2dfit(im_frag,c_g2d,/tilt)
        if(i eq 0) then begin
            im_ref=im_frag
            c_g2d_ref=c_g2d
        endif
        dx=c_g2d[4]-c_g2d_ref[4]
        dy=c_g2d[5]-c_g2d_ref[5]
        flag_im[i] = (c_g2d[2]/c_g2d[3] ge ax_rat and c_g2d[2]/c_g2d[3] le 1d/ax_rat and $ ;;; roundness constraint
                      c_g2d[2] ge seeing_limits[0]/2.355/0.2 and c_g2d[2] le seeing_limits[1]/2.355/0.2 and $ ;;; image quality constraint
;;;                      c_g2d[2] lt 2.2 and $ ;;  old version corresponds to 1.04 arcsec
                      c_g2d[1] gt 0)? 1 : 0 ;;; positivity constraint
        print,'i=',i,'  i0=',c_g2d[1],'  flux=',c_g2d[1]*c_g2d[2]*c_g2d[3],' fwhm=',c_g2d[2:3]*2.355*0.2,' dx,dy=',dx,dy,flag_im[i],$
            form='(a,i3,2(a,f8.2),a,2f7.3,a,2f7.2,i3)'
        printf,u,'i=',i,'  i0=',c_g2d[1],'  flux=',c_g2d[1]*c_g2d[2]*c_g2d[3],' fwhm=',c_g2d[2:3]*2.355*0.2,' dx,dy=',dx,dy,flag_im[i],$
            form='(a,i3,2(a,f8.2),a,2f7.3,a,2f7.2,i3)'
        if(abs(dx) gt wsize) then dx=0.0
        if(abs(dy) gt wsize) then dy=0.0
        if(dx gt 0) then img_cur[s_mmirs[1]-ceil(dx)-1:*,*]=!values.f_nan
        if(dx lt 0) then img_cur[0:ceil(-dx),*]=!values.f_nan
        if(dy gt 0) then img_cur[*,s_mmirs[2]-ceil(dy)-1:*]=!values.f_nan
        if(dy lt 0) then img_cur[*,0:ceil(-dy)]=!values.f_nan
        img_cube_flat[*,*,i]=shift_image(img_cur,-dx,-dy)
    endfor
    close,u
    free_lun,u
endif

good_flag=where(flag_im eq 1, cgood_flag)
if(cgood_flag le 1) then begin
    print,'Not enough images available for stacking. Aborting the data reduction pipeline'
    return
endif

ii1=median(img_cube_flat[*,*,where(flag_im eq 1)],dim=3)
h000=headfits(conf_arr[0].rdir+conf_arr[0].sci+'.fix.fits.gz',ext=0)
h001=headfits(conf_arr[0].rdir+conf_arr[0].sci+'.fix.fits.gz',ext=1)
sxdelpar,h001,'DETSIZE'
sxdelpar,h001,'CCDSEC'
sxdelpar,h001,'DATASEC'
sxdelpar,h001,'TRIMSEC'
sxdelpar,h001,'DETSEC'
crpix1=sxpar(h001,'CRPIX1')
crpix2=sxpar(h001,'CRPIX2')
crpix1=crpix1+instel_arr[0]/0.2
crpix2=crpix2-instaz_arr[0]/0.2
sxaddpar,h001,'CRPIX1',crpix1
sxaddpar,h001,'CRPIX2',crpix2
h001_c=h001
sxaddpar,h001,'BUNIT','e-/s'
sxaddpar,h001,'EXPTIME',sxpar(h001,'EXPTIME')*total(flag_im)

writefits,wdir+'flat_norm_'+filter+'.fits',0,h000
mwrfits,float(flat/median(flat)),wdir+'flat_norm_'+filter+'.fits',h001

ii1=ii1*sxpar(h001,'GAIN') ;; *total(flag_im)*sxpar(h001,'EXPTIME')
writefits,wdir+output_prefix+'_med.fits',0,h000
mwrfits,float(ii1),wdir+output_prefix+'_med.fits',h001

resistant_mean,img_cube_flat[*,*,where(flag_im eq 1)],3.0,tt,e_tt,dim=3
ii2=tt
e_ii2=e_tt
ii2=ii2*sxpar(h001,'GAIN') ;; *total(flag_im)*sxpar(h001,'EXPTIME')
e_ii2=e_ii2*sxpar(h001,'GAIN')  ;;*total(flag_im)*sxpar(h001,'EXPTIME')

writefits,wdir+output_prefix+'_mean.fits',0,h000
mwrfits,float(ii2),wdir+output_prefix+'_mean.fits',h001

writefits,wdir+output_prefix+'_mean_err.fits',0,h000
mwrfits,float(e_ii2),wdir+output_prefix+'_mean_err.fits',h001

writefits,wdir+output_prefix+'_stack.fits',0,h000
sxaddpar,h001_c,'NAXIS3',n_elements(where(flag_im eq 1)),after='NAXIS2'
mwrfits,float(img_cube_flat[*,*,where(flag_im eq 1)]*sxpar(h001,'GAIN')),wdir+output_prefix+'_stack.fits',h001_c

end
