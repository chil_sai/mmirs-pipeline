pro run_pipeline,dir,nmin=nmin,nmax=nmax,box=box,target=target,anyslit=anyslit,$
    noreduce=noreduce,fitprofile=fitprofle,$
    nokelson=nokelson,telluric_dir_idx=telluric_dir_idx,num_tel=num_tel,verbose=verbose

loglist=file_search(dir+'/*.txt')
n_l=n_elements(loglist)

if(n_elements(nmin) ne 1) then nmin=0
if(n_elements(nmax) ne 1) then nmax=n_l-1
if(n_elements(box) eq 0) then box=0

nmin_corr = (nmin>0)
nmax_corr = (nmax<(n_l-1))

log0=readlog(loglist[nmin_corr])
w_dir=sxpar(log0,'W_DIR')
w_dir=strmid(w_dir,0,strlen(w_dir)-4)
fitprofile=sxpar(log0,'FITPROF')

if(~keyword_set(noreduce)) then $
    for i=nmin_corr,nmax_corr do reduce_mmirs,loglist[i]

combine_2d_pairs,loglist[nmin_corr:nmax_corr],w_dir+'/sum/',box=box,target=target,verbose=verbose, nokelson=nokelson,$
    anyslit=anyslit,fitprofile=fitprofile
combine_2d_pairs,loglist[nmin_corr:nmax_corr],w_dir+'/sum/',box=box,/corr,fitprofile=fitprofile,nokelson=nokelson,$
    telluric_dir_idx=telluric_dir_idx,num_tel=num_tel,target=target,verbose=verbose,anyslit=anyslit


end
