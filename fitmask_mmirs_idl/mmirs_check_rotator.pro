function mmirs_check_rotator,rotator_range,rotator_limits
    if(n_params() eq 1) then rotator_limits=[-180.0,164.0] ;; rotator limits
    rot_ok = where(rotator_range gt rotator_limits[0] and rotator_range lt rotator_limits[1],crot_ok)
    return,(crot_ok eq n_elements(rotator_range))? 1 : 0
end
